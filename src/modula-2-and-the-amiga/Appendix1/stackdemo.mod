MODULE stackdemo;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn;
FROM System IMPORT argc,argv,WBenchMsg,StackSize;
FROM AmigaDOSProcess IMPORT Delay;


PROCEDURE doit();
BEGIN
 WriteString("sorry, I don't actually do anything. This is only a test");
 WriteLn;
 Delay(120);
END doit;

BEGIN (* of MODULE *)

IF StackSize<6000D THEN (* stack is not large enough *)

 WriteString("Stack size is too small. Increase to at least 6000");WriteLn;

 IF WBenchMsg=NIL THEN (* was run from CLI *)
  WriteString("by typing: stack 6000");WriteLn;
 ELSE (* was run from Workbench *)
  WriteString("by clicking on the icon and using the Info(rmation) item");
  WriteLn;
  WriteString("from the Workbench menu");WriteLn;
  Delay(120);
 END; (* IF *)
 
ELSE (* stack is big enough *)

 doit; (* call the main Procedure *)
END; (* IF StackSize *)

END stackdemo.

