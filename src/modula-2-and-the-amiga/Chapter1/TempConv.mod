IMPLEMENTATION MODULE TempConv;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

PROCEDURE CtoF(cent:REAL):REAL;
BEGIN

RETURN ((cent+40.0)*9.0/5.0)-40.0;

END CtoF;

PROCEDURE FtoC(faren:REAL):REAL;
BEGIN

RETURN ((faren+40.0)*5.0/9.0)-40.0;

END FtoC;

BEGIN

END TempConv.
