MODULE arthur;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM Strings IMPORT StringLength;

(* =========================================== *)

(* Don't worry about how this PROCEDURE works right now.
   just know that is shows us how the name "Arthur" is stored in memory *)

PROCEDURE ShowCodes(text:ARRAY OF CHAR);
VAR
 i,len:CARDINAL;
BEGIN

 len:=StringLength(text);
 
 WriteLn;
 WriteString("Position  Character   Decimal code");WriteLn;
 WriteString("--------  ---------   ------------");WriteLn; 
 FOR i:=0 TO len DO
  WriteCard(i,4);WriteString("          ");
  WriteString(text[i]);WriteString("          ");
  WriteCard(CARDINAL(text[i]),4);WriteLn;
 END; (* FOR i *) 

END ShowCodes;

(* =========================================== *)

PROCEDURE demo;
VAR
 name:ARRAY[0..6] OF CHAR;
 
BEGIN

(* we could set 'name' to "Arthur" like this *)

name[0]:='A';
name[1]:='r';
name[2]:='t';
name[3]:='h';
name[4]:='u';
name[5]:='r';
name[6]:=CHAR(0);

WriteString("by method 1: ");WriteString(name);WriteLn;

(* this shows us how the name is stored *)
ShowCodes(name);

(* but this does the same thing, and is much more convenient *)

name:="Arthur";

WriteLn;
WriteString("by method 2: ");WriteString(name);WriteLn;


(* this shows us how the name is stored *)

ShowCodes(name);

END demo;

(* =========================================== *)

BEGIN

demo;


END arthur.

