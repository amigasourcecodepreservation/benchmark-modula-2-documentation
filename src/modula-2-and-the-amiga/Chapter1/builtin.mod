MODULE builtin;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,WriteCard,WriteInt,Write;
FROM RealInOut IMPORT WriteReal;

VAR
 i:INTEGER;
 r:REAL;
 c:CHAR;
 x:ARRAY[0..10] OF CHAR;
 b:BITSET;
 
PROCEDURE Showset(b:BITSET);
VAR
 i:CARDINAL;
BEGIN
 WriteString("{");
 FOR i:=0 TO 15 DO
 
 (* IN is used to find out if an item is in a set: *)
 
  IF i IN b THEN WriteCard(i,2);Write(",") END;
 END; (* FOR i *) 
 WriteString("}");
END Showset;
   
     
BEGIN

WriteString("** The absolute procedure ABS() makes numbers positive");WriteLn;
WriteString("Notice that is works with both integer and real types");WriteLn;

i:=-4;
WriteString("i=");WriteInt(i,2);WriteLn;
i:=ABS(i);
WriteString("ABS(i)=");WriteInt(i,2);WriteLn;
WriteLn;

r:=-4.2;
WriteString("r=");WriteReal(r,2);WriteLn;
r:=ABS(r);
WriteString("ABS(r)=");WriteReal(r,2);WriteLn;
WriteLn;

WriteString("** The CAP procedure turns characters to upper case");WriteLn;


WriteString("CAP('a')=");Write(CAP('a'));WriteLn;
WriteLn;

WriteString("** CHR returns a character given its ascii code");WriteLn;

WriteString("CHR(34)=");Write(CHR(34));WriteLn;
WriteLn;

WriteString("** FLOAT turns an integer into a real");WriteLn;

r:=FLOAT(12);
WriteString("FLOAT(12)=");WriteReal(r,4);WriteLn;
WriteLn;

WriteString("** HIGH returns the maximum dimension of an array");WriteLn;

WriteString("HIGH(x)=");WriteCard(HIGH(x),4);WriteLn;
WriteLn;

WriteString("** MAX returns the maximum allowed value for simple numeric types");
WriteLn;

WriteString("MAX(INTEGER)=");WriteInt(MAX(INTEGER),6);WriteLn;
WriteLn;


WriteString("** MIN returns the minimum allowed value for simple numeric types");
WriteLn;

WriteString("MIN(INTEGER)=");WriteInt(MIN(INTEGER),6);WriteLn;
WriteLn;

WriteString("** ODD returns TRUE for odd numbers, and FALSE for even numbers");
WriteLn;

IF ODD(3) THEN 
 WriteString("ODD(3) is TRUE")
ELSE
 WriteString("ODD(3) is FALSE")
END; (* IF *)
WriteLn;

IF ODD(4) THEN 
 WriteString("ODD(4) is TRUE")
ELSE
 WriteString("ODD(4) is FALSE")
END; (* IF *)
WriteLn;
WriteLn;

WriteString("** TRUNC turns a real into an integer");WriteLn;
WriteString("TRUNC(4.3)=");WriteInt(TRUNC(4.3),4);WriteLn;
WriteLn;

WriteString("** VAL coerces one type into another");WriteLn;
i:=VAL(INTEGER,'a');
WriteString("VAL(INTEGER,'a')=");WriteInt(i,4);WriteLn;
WriteLn;

WriteString("** DEC decrements numbers");WriteLn;

i:=5;
WriteString("i=");WriteInt(i,2);WriteLn;
DEC(i);
WriteString("After DEC(i) , i=");WriteInt(i,2);WriteLn;
WriteLn;


i:=5;
WriteString("i=");WriteInt(i,2);WriteLn;
DEC(i,2);
WriteString("After DEC(i,2) , i=");WriteInt(i,2);WriteLn;
WriteLn;


WriteString("** INC increments numbers");WriteLn;

i:=5;
WriteString("i=");WriteInt(i,2);WriteLn;
INC(i);
WriteString("After INC(i) , i=");WriteInt(i,2);WriteLn;
WriteLn;


i:=5;
WriteString("i=");WriteInt(i,2);WriteLn;
INC(i,2);
WriteString("After INC(i,2) , i=");WriteInt(i,2);WriteLn;
WriteLn;

WriteString("** ORD returns the numeric representation for a variable");WriteLn;
WriteString("ORD('a')=");WriteCard(ORD('a'),3);WriteLn;
WriteLn;

	(* If you get an 'Error 255: too many strings' error *)
	(* here, increase the Const buffer to at least 1500 *)
WriteString("** EXCL is used to remove items from a set");WriteLn;
b:=BITSET{1,2,7};
WriteString("set b=");Showset(b);WriteLn;
EXCL(b,7);
WriteString("After EXCL(b,7) set b=");Showset(b);WriteLn;
WriteLn;


WriteString("** INCL is used to add items to a set");WriteLn;

WriteString("set b=");Showset(b);WriteLn;
INCL(b,9);
WriteString("After INCL(b,9) set b=");Showset(b);WriteLn;
WriteLn;

WriteString("** IN is used to see if an item is in a set");WriteLn;
WriteString("(IN is really an operator rather than a Procedure)");WriteLn;
WriteString("set b=");Showset(b);WriteLn;

WriteString(" 9 IN b is ");
IF 9 IN b THEN
 WriteString("TRUE");
ELSE
 WriteString("FALSE");
END; (* IF *)
WriteLn;


WriteString(" 10 IN b is ");
IF 10 IN b THEN
 WriteString("TRUE");
ELSE
 WriteString("FALSE");
END; (* IF *)
WriteLn;
WriteLn;

WriteString("** DIV performs integer division");WriteLn;
WriteString("(DIV is really an operator rather than a Procedure)");WriteLn;
WriteString("10 DIV 3=");WriteCard(10 DIV 3,4);WriteLn;
WriteLn;

END builtin.

