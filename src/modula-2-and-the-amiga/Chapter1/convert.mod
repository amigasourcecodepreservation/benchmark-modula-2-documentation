MODULE convert;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteCard,WriteLn;
FROM LongInOut IMPORT WriteLongInt;
FROM RealInOut IMPORT WriteReal;
FROM MathLib0 IMPORT real,entier;

PROCEDURE demo;
VAR
 i:LONGINT;
 r:REAL;
 
BEGIN

i:=100D;

WriteString("i=");WriteLongInt(i,4);WriteLn;

r:=REAL(i); (* This is type coercion using the type REAL *)

WriteString("REAL(i)=");WriteReal(r,8);WriteLn;

r:=real(i); (* This is type conversion, using the PROCEDURE real *)

WriteString("real(i)=");WriteReal(r,8);WriteLn;

END demo;

BEGIN

demo;

END convert.
