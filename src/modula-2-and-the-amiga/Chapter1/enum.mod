MODULE enum;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,WriteCard;

TYPE
 ComputerType=(amiga,pc,mac,other);
 

PROCEDURE Opinion(comp:ComputerType);

BEGIN

WriteCard(CARDINAL(comp),4);WriteLn;

IF comp=amiga THEN
 WriteString("Amiga is the best"); 
ELSIF comp=mac THEN
 WriteString("At least the mac has a 68000");
ELSIF comp=pc THEN
 WriteString("pc?  Please, no, not a segmented addressing scheme!");
ELSE
 WriteString("unknown type of computer");
END; (* IF computer *)

WriteLn;

END Opinion;


BEGIN

Opinion(amiga);
Opinion(pc);
Opinion(mac);
Opinion(other);


END enum.

