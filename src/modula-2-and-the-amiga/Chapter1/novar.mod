MODULE novar;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,WriteCard;

(* =========================== *)
PROCEDURE doit(x:CARDINAL);
BEGIN
 x:=x+10;
END doit;
(* =========================== *)
PROCEDURE doit2(VAR x:CARDINAL);
BEGIN
 x:=x+10;
END doit2;
(* =========================== *)
PROCEDURE demo;
VAR
 x:CARDINAL;
 
BEGIN
x:=2;
WriteString("before x=");WriteCard(x,3);WriteLn;
doit(x);
WriteString("after x=");WriteCard(x,3);WriteLn;
WriteLn;

WriteString("before x=");WriteCard(x,3);WriteLn;
doit2(x);
WriteString("after x=");WriteCard(x,3);WriteLn;

END demo;
(* =========================== *)

BEGIN

demo;

END novar.

