MODULE proc;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,WriteCard;

(* ================================ *)
PROCEDURE hi;
BEGIN
 WriteString("hi");WriteLn;
END hi;
(* ================================ *)
PROCEDURE num;
BEGIN
 WriteCard(3,3);WriteLn;
END num;

(* ================================ *)

PROCEDURE demo(p:PROC);
BEGIN
 p;
END demo;
(* ================================ *)

BEGIN

demo(hi);
demo(num);

END proc.

