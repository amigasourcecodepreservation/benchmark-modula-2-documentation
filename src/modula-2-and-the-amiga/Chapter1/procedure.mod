MODULE procedure;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,WriteCard;

TYPE
 myproc=PROCEDURE(CARDINAL,CARDINAL):CARDINAL;

(* ================================ *)
PROCEDURE add(a,b:CARDINAL):CARDINAL;
BEGIN
 RETURN a+b;
END add;
(* ================================ *)
PROCEDURE sub(a,b:CARDINAL):CARDINAL;
BEGIN
 RETURN a-b;
END sub;

(* ================================ *)

PROCEDURE demo(p:myproc);
VAR
 ans:CARDINAL;
BEGIN
 ans:=p(5,3);
 WriteCard(ans,5);WriteLn;
END demo;
(* ================================ *)

BEGIN

demo(add);
demo(sub);

END procedure.

