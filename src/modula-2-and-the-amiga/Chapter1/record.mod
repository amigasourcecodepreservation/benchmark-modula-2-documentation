MODULE record;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,WriteCard;


TYPE

 sextype=(male,female,other);
 
 DataEntry=RECORD
 	    name:ARRAY[0..80] OF CHAR;
	    age:CARDINAL;
	    sex:sextype;
 	   END; (* RECORD *)

VAR
 MyFriends:ARRAY[1..10] OF DataEntry;
 
 
PROCEDURE ShowFriends(n:CARDINAL);
VAR
 i:CARDINAL;
 
BEGIN
 FOR i:=1 TO n DO
  WriteString(MyFriends[i].name);
  
  IF MyFriends[i].sex=male THEN
   WriteString(" is ");WriteCard(MyFriends[i].age,4);
   WriteString(" years old");WriteLn;
  ELSE
   WriteString(" : SORRY, AGE IS CLASSIFIED");WriteLn;
  END; (* IF *)
    
 END; (* FOR i *)
 
END ShowFriends;

 
BEGIN

 MyFriends[1].name:="Arthur";
 MyFriends[1].age:=35;
 MyFriends[1].sex:=male;

 MyFriends[2].name:="Zaphod";
 MyFriends[2].age:=145;
 MyFriends[2].sex:=male;
 

 MyFriends[3].name:="Ford";
 MyFriends[3].age:=155;
 MyFriends[3].sex:=male;
 
 
 MyFriends[4].name:="Trillian";
 MyFriends[4].age:=30;
 MyFriends[4].sex:=female;

 ShowFriends(4);
 
END record.

