MODULE return;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,WriteCard;


PROCEDURE demo(x:CARDINAL);
VAR
 y:CARDINAL;
 
BEGIN
 IF x=0 THEN 
  WriteString("aborting demo(), x=0");WriteLn;
  RETURN; (* this make us leave this procedure *)
 END; (* IF *)
 
 y:=10 DIV x;
 
 WriteString("y=");WriteCard(y,5);WriteLn; 

END demo;

BEGIN

demo(2);
demo(0);

END return.

