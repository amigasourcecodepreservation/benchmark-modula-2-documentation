MODULE set;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,WriteCard;

TYPE

 options=(Seats,Tires,AirConditioning,ElectricAshTrays);
 
 CarOptions=SET OF options;

 VAR
  YourOptions:CarOptions;
  x:CARDINAL;

    
  PROCEDURE FindCost(option:CarOptions):CARDINAL;
  VAR
   cost:CARDINAL;
  BEGIN
   cost:=0;
   
   IF Seats IN option THEN
    cost:=cost+100;
   END; (* IF *)
   
   IF Tires IN option THEN
    cost:=cost+200;
   END; (* IF *)
     
   IF AirConditioning IN option THEN
    cost:=cost+1000;
   END; (* IF *)

   IF ElectricAshTrays IN option THEN
    cost:=cost+10000;
   END; (* IF *)

   RETURN cost;

  END FindCost;
  
BEGIN

 YourOptions:=CarOptions{Seats,Tires};
 
 x:=FindCost(YourOptions);

 WriteString("your option cost is ");WriteCard(x,5);
 WriteString(" dollars");WriteLn;
 
END set.

