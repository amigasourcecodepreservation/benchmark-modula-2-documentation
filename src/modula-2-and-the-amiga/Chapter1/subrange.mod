MODULE subrange;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,WriteCard;

TYPE
 digit=[0..9];

VAR
 x,y:digit;
 
BEGIN
 x:=1;
 y:=2;
 WriteCard(x+y,4);WriteLn;
 
 (* y:=11; *)  (* this line give a compilation error:
 		 constant outside of subrange bounds *)

 x:=9;
 y:=9;
 
(*  y:=x+y; *) (* this gives a run-time error if range checking is on *)

 WriteCard(y,4);WriteLn;

END subrange.

