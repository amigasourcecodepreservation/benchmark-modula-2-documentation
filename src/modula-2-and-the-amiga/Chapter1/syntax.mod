MODULE syntax;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,WriteCard;

(* =============================== *)
PROCEDURE tryremark;
VAR x:CARDINAL;

BEGIN
 x:=1;
 
 (*
  x:=x+1; (* this will be ignored *)
 *)
 
 WriteString("This should still be 1. x=");WriteCard(x,4);WriteLn;
 
END tryremark;

(* =============================== *)
PROCEDURE AskUserIfReady():BOOLEAN;
BEGIN
 WriteString("see.. the inside of the REPEAT..UNTIL was executed");WriteLn;
 RETURN TRUE;

END AskUserIfReady;

(* =============================== *)
PROCEDURE tryrepeat;
VAR
 T:BOOLEAN;
 
BEGIN

 REPEAT
  T:=AskUserIfReady();
 UNTIL T=TRUE;
  

END tryrepeat;

(* =============================== *)
PROCEDURE tryif;
VAR
 a,b:INTEGER;
BEGIN
 a:=1;
 b:=2;
 
 IF a=b THEN
  WriteString("a=b");WriteLn;
 END;
 
 
 IF a=b THEN
  WriteString("a=b");WriteLn;
 ELSE
  WriteString("a<>b");WriteLn;
 END;

  
 IF a>b THEN
  WriteString("a>b");WriteLn;
 ELSIF a<b THEN
  WriteString("a<b");WriteLn;
 ELSE
  WriteString("a=b");WriteLn;
 END;

 WriteString("this will do the same thing as the last IF structure");WriteLn;
 
 IF a>b THEN
  WriteString("a>b");WriteLn;
 ELSE
  IF a<b THEN
   WriteString("a<b");WriteLn;
  ELSE
   WriteString("a=b");WriteLn;
  END; (* IF a<b *)
 END; (* IF a>b *)
 
 

END tryif;


(* =============================== *)
PROCEDURE tryfor;
 VAR
  i:CARDINAL;
  
BEGIN
 FOR i:=1 TO 10 DO
  WriteString("hi");WriteLn;
 END; (* FOR i *)
 
 FOR i:=2 TO 10 BY 2 DO
  WriteCard(i,4);WriteLn;
 END; (* FOR i *)
 
END tryfor;

(* =============================== *)

PROCEDURE trywhile;
VAR
 i,x:REAL;
 
BEGIN
 
 i:=0.0;
 x:=1.0;
 
 WHILE i>0.0 DO
  WriteString("you won't see this executed");WriteLn;
  x:=x/i;
  i:=i-1.0;
 END; (* WHILE *)
 

END trywhile;

(* =============================== *)
PROCEDURE tryloop;
VAR
 x,y:CARDINAL;
BEGIN

x:=5;
y:=1000;

LOOP
 x:=x-1;
 IF x=0 THEN EXIT; END;
 y:=y DIV x;
END; (* LOOP *)

WriteString("x=");WriteCard(x,5);WriteLn;
WriteString("y=");WriteCard(y,5);WriteLn;

END tryloop;

(* =============================== *)
PROCEDURE trycase;
VAR
 x:CARDINAL;

BEGIN

x:=1;

WriteString("That will be");

CASE x OF

0: WriteString(" free");WriteLn; |

1: WriteCard(x,3);WriteString(" dollar");WriteLn; |

ELSE
 
 WriteCard(x,3);WriteString(" dollars");WriteLn;

END; (* CASE *)


END trycase;

(* =============================== *)
PROCEDURE trytype;
TYPE
 mytype=ARRAY[0..40] OF CHAR;
VAR
 myname:mytype;
 yourname:mytype;
  
BEGIN
 myname:="Mike";
 yourname:="I DontKnow";
 
 WriteString(myname);WriteString(" says hi to ");
 WriteString(yourname);WriteLn;

END trytype;
(* =============================== *)

BEGIN

WriteString("tryremark");WriteLn;
tryremark;
WriteString("tryrepeat");WriteLn;
tryrepeat;
WriteString("tryif");WriteLn;
tryif;
WriteString("tryfor");WriteLn;
tryfor;
WriteString("trywhile");WriteLn;
trywhile;
WriteString("tryloop");WriteLn;
tryloop;
WriteString("trycase");WriteLn;
trycase;
WriteString("trytype");WriteLn;
trytype;

END syntax.

