MODULE testconv;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM TempConv IMPORT CtoF,FtoC;

FROM InOut IMPORT WriteString,WriteLn;
FROM RealInOut IMPORT WriteReal;

VAR
 celcius,farenheit:REAL;

BEGIN

celcius:=100.0;
farenheit:=CtoF(celcius);

WriteReal(celcius,4);
WriteString(" degrees C = ");WriteReal(farenheit,4);
WriteString(" degress F");WriteLn;


farenheit:=32.0;
celcius:=FtoC(farenheit);

WriteReal(farenheit,4);
WriteString(" degrees F = ");WriteReal(celcius,4);
WriteString(" degress C");WriteLn;

END testconv.

