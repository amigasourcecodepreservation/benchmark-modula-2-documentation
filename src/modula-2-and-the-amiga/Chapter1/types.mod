MODULE types;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,WriteCard,WriteInt;
FROM LongInOut IMPORT WriteLongCard,WriteLongInt;
FROM RealInOut IMPORT WriteReal;

FROM SYSTEM IMPORT TSIZE,BYTE,WORD,LONGWORD,ADDRESS;

TYPE 
 LONGBITSET =SET OF [0..31];

PROCEDURE getsizes;
 
BEGIN

WriteLn;
WriteString("TYPE        bytes          min               max");WriteLn;
WriteString("----        -----          ---               ---");WriteLn;

WriteString("CARDINAL   ");
WriteCard(TSIZE(CARDINAL),4);WriteString("  ");
WriteCard(MIN(CARDINAL),13);WriteString("       ");
WriteCard(MAX(CARDINAL),13);WriteLn;

WriteString("INTEGER    ");
WriteCard(TSIZE(INTEGER),4);WriteString("  ");
WriteInt(MIN(INTEGER),13);WriteString("       ");
WriteInt(MAX(INTEGER),13);WriteLn;

WriteString("LONGCARD   ");
WriteCard(TSIZE(LONGCARD),4);WriteString("  ");
WriteLongCard(MIN(LONGCARD),13);WriteString("       ");
WriteLongCard(MAX(LONGCARD),13);WriteLn;

WriteString("LONGINT    ");
WriteCard(TSIZE(LONGINT),4);WriteString("  ");
WriteLongInt(MIN(LONGINT),13);WriteString("       ");
WriteLongInt(MAX(LONGINT),13);WriteLn;

WriteString("REAL       ");
WriteCard(TSIZE(REAL),4);WriteString("  ");
WriteReal(MIN(REAL),13);WriteString("       ");
WriteReal(MAX(REAL),13);WriteLn;


WriteLn;


WriteString("BOOLEAN    ");
WriteCard(TSIZE(BOOLEAN),4);WriteString("          ");
WriteString("FALSE                TRUE");WriteLn;

(* WriteCard(CARDINAL(FALSE),4);WriteCard(CARDINAL(TRUE),4);WriteLn; *)

WriteString("CHAR       ");
WriteCard(TSIZE(CHAR),4);WriteString("             ");
WriteLn;

WriteString("BYTE       ");
WriteCard(TSIZE(BYTE),4);WriteString("             ");
WriteLn;

WriteString("WORD       ");
WriteCard(TSIZE(WORD),4);WriteString("             ");
WriteLn;

WriteString("LONGWORD   ");
WriteCard(TSIZE(LONGWORD),4);WriteString("             ");
WriteLn;

WriteString("BITSET     ");
WriteCard(TSIZE(BITSET),4);WriteString("             ");
WriteLn;

WriteString("LONGBITSET ");
WriteCard(TSIZE(LONGBITSET),4);WriteString("             ");
WriteLn;

WriteString("PROC       ");
WriteCard(TSIZE(PROC),4);WriteString("             ");
WriteLn;

WriteString("ADDRESS    ");
WriteCard(TSIZE(ADDRESS),4);WriteString("             ");
WriteLn;

END getsizes;

BEGIN

getsizes;

END types.

