MODULE var;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM MathLib0 IMPORT real;
FROM RealInOut IMPORT WriteReal;

(* ==================================== *)

PROCEDURE total(n:CARDINAL;VAR x:ARRAY OF REAL):REAL;
VAR
 t:REAL;
 i:CARDINAL;

BEGIN

 t:=0.0;
 FOR i:=0 TO n DO
  t:=t+x[i];
 END; (* FOR i *)
 RETURN t;
 
END total;

(* ==================================== *)

PROCEDURE demo;

VAR
 x:ARRAY[0..100] OF REAL;
 i:CARDINAL;
 t:REAL;
 
BEGIN

 FOR i:=0 TO 100 DO
  x[i]:=real(i);
 END; (* FOR i *)
 
 t:=total(100,x);
 
 WriteString("total=");WriteReal(t,8);WriteLn;
   
END demo;

(* ==================================== *)

BEGIN

demo;

END var.

