MODULE checkrevs;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM System IMPORT ExecBase,DOSBase,MathBase,IntuitionBase,GfxBase,LayersBase;
FROM Libraries IMPORT LibraryPtr;

PROCEDURE CheckLibs(Rev:CARDINAL):BOOLEAN;
 VAR
  cklib:LibraryPtr;
BEGIN
  cklib:=ExecBase;
  IF cklib^.libVersion<Rev THEN RETURN FALSE; END;
  cklib:=DOSBase;
  IF cklib^.libVersion<Rev THEN RETURN FALSE; END;
  cklib:=MathBase;
  IF cklib^.libVersion<Rev THEN RETURN FALSE; END;
  cklib:=IntuitionBase;
  IF cklib^.libVersion<Rev THEN RETURN FALSE; END;
  cklib:=GfxBase;
  IF cklib^.libVersion<Rev THEN RETURN FALSE; END;
  cklib:=LayersBase;
  IF cklib^.libVersion<Rev THEN RETURN FALSE; END;
  RETURN TRUE;
END CheckLibs;

BEGIN

IF CheckLibs(34) THEN
 WriteString("We are running under 1.3 or higher");
ELSE
 WriteString("We are running under 1.2 or earlier");
END;
WriteLn;

END checkrevs.

