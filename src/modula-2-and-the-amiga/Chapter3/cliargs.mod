MODULE cliargs;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)


(* Benchmark Modules *)

FROM System IMPORT argc,argv;

FROM InOut IMPORT WriteString,WriteLn,WriteCard;

   
PROCEDURE test;
VAR
 i:CARDINAL;
BEGIN


IF argc>=1 THEN
 FOR i:=0 TO argc-1 DO
  WriteString("argv[");WriteCard(i,3);WriteString("]=");
  WriteString(argv^[i]^);WriteLn;
 END; (* FOR i *)
END; (* IF *)


 

END test;
(* ============================= *)

BEGIN (* of MODULE *)

test;

END cliargs.

