MODULE copyit;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)


(* Benchmark Modules *)

FROM System IMPORT argc,argv;
FROM SYSTEM IMPORT ADR;
FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM AmigaDOS IMPORT Open,FileHandle,Close,Read,ModeOldFile,ModeNewFile,Write;

CONST
 maxchar=512;
 
VAR
 buf:ARRAY[0..maxchar] OF CHAR;
 
(* ======================================== *)
 
PROCEDURE CopyFile(name,newname:ARRAY OF CHAR);
VAR
fh,fh2:FileHandle;
NumRead,NumWritten:LONGINT;
done:BOOLEAN;

PROCEDURE cleanup;
BEGIN
 IF fh<>NIL THEN Close(fh); END;
 IF fh2<>NIL THEN Close(fh2); END;
END cleanup;

BEGIN
 fh:=NIL;
 fh2:=NIL;
 
 fh:=Open(ADR(name),ModeOldFile); (* open existing file *)
 
 IF fh=NIL THEN
  cleanup;
  WriteString("could not open file ");WriteString(name);WriteLn;
 ELSE
 
  fh2:=Open(ADR(newname),ModeNewFile); (* create new file *)
  
  IF fh2=NIL THEN
   cleanup;
   WriteString("could not create new file ");WriteString(newname);WriteLn;
  ELSE
   
  done:=FALSE;
  
  REPEAT
   
   NumRead:=Read(fh,ADR(buf),maxchar);
   
   IF NumRead<=0D THEN 
    done:=TRUE; 
   ELSE
   
    NumWritten:=Write(fh2,ADR(buf),NumRead);
    
    IF NumWritten<>NumRead THEN
     WriteString("disk full?");WriteLn;
     cleanup;
     RETURN; (* leave the CopyFile procedure *)
    END; (* IF NumWritten *)
    
   END; (* IF NumRead *)
   
  UNTIL done;
        
  cleanup;
  
  END; (* IF fh2 *)
 END; (* IF fh *)
 
END CopyFile;

(* ======================================== *)
      
PROCEDURE test;
VAR
 i:CARDINAL;
BEGIN


IF argc>=3 THEN
  CopyFile(argv^[1]^,argv^[2]^);
ELSE
 WriteString("Usage: copyit oldfile newfile");WriteLn;
END; (* IF *)


 

END test;
(* ============================= *)

BEGIN (* of MODULE *)

test;

END copyit.
