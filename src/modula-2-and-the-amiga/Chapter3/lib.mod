MODULE Lib;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM Libraries IMPORT OpenLibrary,CloseLibrary,LibraryPtr;
FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM SYSTEM IMPORT ADR;
FROM Translator IMPORT Translate,TranslatorName,TranslatorBase;
FROM Strings IMPORT StringLength;
FROM LongInOut IMPORT WriteLongInt;

VAR
 lib:LibraryPtr;
 in,out:ARRAY[0..80] OF CHAR;
 ok:LONGINT;
  
BEGIN
 WriteString("trying to open library:");WriteString(TranslatorName);WriteLn;
 
 lib:=OpenLibrary(ADR(TranslatorName),0);
 
 IF lib=NIL THEN
  WriteString("could not open library");WriteLn;
 ELSE
 
  TranslatorBase:=lib; (* copy to Global variable *)
  
  in:="hi there";
  
  ok:=Translate(ADR(in),LONGINT(StringLength(in)),ADR(out),80D);
  
  IF ok=0D THEN
   WriteString(in);WriteLn;
   WriteString("translates to");WriteLn;
   WriteString(out);WriteLn;
  ELSE
   WriteString("got error from translate=");WriteLongInt(ok,4);WriteLn;
  END; (* IF *)
    
  CloseLibrary(lib^);
 END; (* IF lib *)

END Lib.

