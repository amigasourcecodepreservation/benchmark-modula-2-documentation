MODULE typeit;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)


(* Benchmark Modules *)

FROM System IMPORT argc,argv;
FROM SYSTEM IMPORT ADR;
FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM AmigaDOS IMPORT Open,FileHandle,Close,Read,ModeOldFile,SigBreakCtrlC;
FROM Tasks IMPORT SignalSet,SetSignal;

CONST
 maxchar=80;
 
VAR
 buf:ARRAY[0..maxchar] OF CHAR;
 
(* ======================================== *)
 
PROCEDURE ShowFile(name:ARRAY OF CHAR);
VAR
fh:FileHandle;
NumRead:LONGINT;
done:BOOLEAN;

BEGIN
  
 fh:=Open(ADR(name),ModeOldFile);
 
 IF fh=NIL THEN
  WriteString("could not open file ");WriteString(name);WriteLn;
 ELSE
 
  done:=FALSE;
  
  REPEAT
   
   NumRead:=Read(fh,ADR(buf),maxchar);
   
   IF NumRead<=0D THEN 
    done:=TRUE; 
   ELSE
    buf[CARDINAL(NumRead)]:=CHAR(0); (* null terminate *)
    WriteString(buf);
    
      (* check for CTRL C *)
    IF SigBreakCtrlC IN SetSignal(SignalSet{},SignalSet{SigBreakCtrlC}) THEN
     WriteLn; WriteString("**BREAK");WriteLn;
     Close(fh); RETURN; 
    END; (* IF SigBreakCtrlC *)
    
   END; (* IF *)
   
  UNTIL done;
       
  Close(fh);
  
 END; (* IF fh *)
 
END ShowFile;

(* ======================================== *)
      
PROCEDURE test;
VAR
 i:CARDINAL;
BEGIN


IF argc>=2 THEN
 FOR i:=1 TO argc-1 DO
  ShowFile(argv^[i]^);
 END; (* FOR i *)
ELSE
 WriteString("Usage: typeit file1 file2 ..");WriteLn;
END; (* IF *)


 

END test;
(* ============================= *)

BEGIN (* of MODULE *)

test;

END typeit.
