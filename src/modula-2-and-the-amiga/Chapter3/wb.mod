MODULE wb;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)


(* Benchmark Modules *)

FROM System IMPORT argc,argv,WBenchMsg;

FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM Workbench IMPORT WBArgPtr,WBStartup,WBStartupPtr; 
FROM SYSTEM IMPORT ADDRESS,LONGWORD; 
FROM AmigaDOSProcess IMPORT Delay;
   
PROCEDURE test;
VAR
 j,NumProjects:LONGINT;
 i:CARDINAL;
 name:POINTER TO ARRAY[0..99] OF CHAR;
 args:WBArgPtr;
 startmsg:WBStartupPtr;
BEGIN



IF WBenchMsg=NIL THEN (* CLI *)

IF argc>=1 THEN
 FOR i:=0 TO argc-1 DO
  WriteString("argv[");WriteCard(i,3);WriteString("]=");
  WriteString(argv^[i]^);WriteLn;
 END; (* FOR i *)
END; (* IF *)

ELSE (* Workbench *)
    startmsg:=WBStartupPtr(WBenchMsg);
    args := startmsg^.smArgList;
    
    NumProjects:=startmsg^.smNumArgs-1D;
    FOR j := 0D TO NumProjects DO
      name := ADDRESS(args^.waName);
      WriteString(name^);WriteLn;
      
      INC(LONGCARD(args),SIZE(args^))
      
    END; (* FOR j *)
 
Delay(100); (* pause to view stuff in window *)

END; (* IF WBenchMsg *)
 

END test;
(* ============================= *)

BEGIN (* of MODULE *)

test;

END wb.

