MODULE adir;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,Write;
FROM System IMPORT argc,argv;
FROM SYSTEM IMPORT ADR,TSIZE;
FROM AmigaDOS IMPORT FileLock,SharedLock,FileInfoBlock,FileInfoBlockPtr,
		     Examine,ExNext,Lock,UnLock,SigBreakCtrlC;
FROM Memory IMPORT AllocMem,FreeMem,MemReqSet,MemClear,MemPublic,MemChip;
FROM Tasks IMPORT SignalSet,SetSignal;
FROM LongInOut IMPORT WriteLongInt;

(* ==================================================== *)

PROCEDURE Dir(VAR name:ARRAY OF CHAR):BOOLEAN; (* TRUE if found dir *)
VAR
  InputFIBPtr : FileInfoBlockPtr; (* ptr to the file info   *)
                                  (* block of the InFile    *)
  worked: BOOLEAN;
  InputLock:FileLock;
  ret:BOOLEAN;
  sum:LONGINT;

(* --------------------------------------------- *)

PROCEDURE Scan(InputLock:FileLock;MyPtr:FileInfoBlockPtr);
VAR
   worked: BOOLEAN;
   
BEGIN

  worked:=ExNext(InputLock,MyPtr^);
       
  WHILE worked DO

  
   IF MyPtr^.fibDirEntryType>0D THEN (* is dir *)
     WriteString("   <dir>  ");
     WriteString(MyPtr^.fibFileName);(* show name *)
     WriteLn;
   ELSE
     WriteLongInt(MyPtr^.fibSize,8);   (* show size *)
     WriteString("  ");                (* space over *)
     WriteString(MyPtr^.fibFileName);  (* show name *)
     WriteLn;
   END; (* MyPtr *)
    
   (* check for CTRL C *)
   IF SigBreakCtrlC IN SetSignal(SignalSet{},SignalSet{SigBreakCtrlC}) THEN
    WriteLn; WriteString("**BREAK");WriteLn;
    RETURN; (* leave the Scan Procedure *)
   END; (* IF SigBreakCtrlC *)

   worked:=ExNext(InputLock,MyPtr^);
   
  END; (* WHILE worked *) 

END Scan;

(* --------------------------------------------- *)

BEGIN (* of Dir *)
   ret:=FALSE;

   InputLock:=Lock(ADR(name),SharedLock);
   IF InputLock=NIL THEN
      WriteString("could not get lock on ");WriteString(name);WriteLn;
   ELSE
   InputFIBPtr := AllocMem (TSIZE(FileInfoBlock),MemReqSet{MemPublic,MemClear});
						      (* assign long-   *)
                                                      (* aligned memory *)
     IF InputFIBPtr = NIL THEN
        WriteString("Dir Couldn't allocate FileInfoBlock space");WriteLn;
     ELSE (* did allocate InputFIBPtr *)
       worked:=Examine(InputLock,InputFIBPtr^);
       IF worked THEN
         IF InputFIBPtr^.fibDirEntryType>0D THEN (* is dir *)
   	   WriteString(InputFIBPtr^.fibFileName);WriteLn; (* show name *) 
	   WriteLn;
   	   ret:=TRUE;
	   
           Scan(InputLock,InputFIBPtr);

         ELSE (* is not dir *)
           WriteString(InputFIBPtr^.fibFileName); (* show name *) 
           WriteString(" is not a directory or disk.");WriteLn;
         END; (* IF InputFIBPtr^ *)
       ELSE
         WriteString("count not examine input lock");WriteLn;
       END; (* IF worked *)

       FreeMem (InputFIBPtr,TSIZE(FileInfoBlock));
       
     END; (* IF InputFIBPtr *);

   UnLock(InputLock);
   
   END; (* IF InputLock *)


 RETURN ret;

END Dir;

(* ==================================================== *)
   
PROCEDURE test;
VAR
 ok:BOOLEAN;
 VAR blank:ARRAY[0..1] OF CHAR;
BEGIN
blank:="";

IF argc>=2 THEN
 ok:=Dir(argv^[1]^); (* do the directory specified *)
ELSE
 ok:=Dir(blank);     (* no arguments, so do the current directory *)
END; (* IF *)

END test;

(* ==================================================== *)

BEGIN

test;

END adir.
