MODULE disks;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM AmigaDOSExt IMPORT RootNodePtr,RootNode,DosLibraryPtr,DosInfoPtr,DosInfo,
		        DeviceList,DeviceListPtr,DLTVolume,DLTDevice;
FROM Interrupts IMPORT Forbid,Permit;
FROM FileHandler IMPORT DeviceNodePtr,DeviceNode;
FROM System IMPORT DOSBase;
FROM SYSTEM IMPORT SHIFT,ADDRESS,LONGWORD;
FROM AmigaDOS IMPORT BPTR,BSTR;

CONST
 max=10;
 
TYPE
 String=ARRAY[0..80] OF CHAR;
 
VAR
 list:ARRAY[0..max] OF String; (* for storing the list of volumes *)

(* =============================================== *)

PROCEDURE INCPtr(VAR pt:LONGWORD;n:LONGCARD);
	 (* this makes the compiler happy about incrementing pointers *)
VAR
 lc:LONGCARD;
BEGIN
 lc:=LONGCARD(pt);
 INC(lc,n);
 pt:=LONGWORD(lc);
END INCPtr;


(* ============================= *)

PROCEDURE PtrFromBPTR(BPtr:BPTR):ADDRESS;
BEGIN
  RETURN SHIFT(BPtr,2);  (* fast way to multiply by 4 *)
END PtrFromBPTR;

(* ============================= *)

PROCEDURE StringFromBCPL(BCpl:BSTR;VAR string:ARRAY OF CHAR);
 TYPE
   chptrType = POINTER TO CHAR;
 VAR 
 i,len:CARDINAL;
 chptr:chptrType;

BEGIN
 BCpl:=PtrFromBPTR(BCpl); (* convert to regular pointer *)
 chptr:=chptrType(BCpl);
 len:=CARDINAL(chptr^); (* length is in the first byte *)
 INCPtr(chptr,1);	(* point to first piece of data *)
IF len>=1 THEN
 FOR i:=0 TO len-1 DO
  string[i]:=chptr^;    (* put the data into the array *)
  INCPtr(chptr,1);	(* point to the next character *)
 END; (* FOR i *)
END; (* IF len *)
 string[len]:=CHR(0);   (* null terminate *)
END StringFromBCPL;

(* ============================= *)
PROCEDURE getdisks();
VAR
 name:String;
 rnptr:RootNodePtr;
 dlp:DosLibraryPtr;
 diptr:DosInfoPtr;
 dnptr:DeviceNodePtr;
 temp:LONGCARD;
 i,j:CARDINAL;
BEGIN

 i:=0;
 
 dlp:=DosLibraryPtr(DOSBase); (* get the pointer to the dos library *)
 
 IF dlp=NIL THEN 	      (* make sure it's open *)
  WriteString("DOSBase is NIL!");WriteLn;
 ELSE

  rnptr:=dlp^.dlRoot;
  diptr:=PtrFromBPTR(rnptr^.rnInfo);
  
  IF diptr=NIL THEN (* make sure the pointer is valid *)
   WriteString("DosInfoPtr is NIL!");WriteLn;
  ELSE

   Forbid; (* prevent the list from being changed while we look at it *)
  
   dnptr:=PtrFromBPTR(diptr^.diDevInfo); (* get Device list pointer *)

    WHILE dnptr<>NIL DO; (* check for being at the end of the list *)

     IF (dnptr^.dnType=DLTVolume) AND (dnptr^.dnTask<>NIL) THEN
	(* make sure this is a volume name AND and a disk type of device *)
	
       StringFromBCPL(dnptr^.dnName,name); (* convert to a regular string *)
       
       IF i<=max THEN
        list[i]:=name; (* put the volume name in the list *)
	i:=i+1;
       END; (* IF i *)

     END; (* IF dnptr^.dnType AND .. *)

     dnptr:=PtrFromBPTR(dnptr^.dnNext); (* get next DeviceNode *)

    END; (* WHILE *)
    
   Permit;
   
  END; (* IF diptr *)
 END; (* IF dlp *)

IF i>=1 THEN (* check for an empty list *)
 FOR j:=0 TO i-1 DO
  WriteString(list[j]);WriteLn; (* show the name *)
 END; (* FOR j *)
END; (* IF i *)

END getdisks;
(* ============================= *)

BEGIN (* of Module *)

getdisks();

END disks.

