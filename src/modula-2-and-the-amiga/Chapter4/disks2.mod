MODULE disks2;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM AmigaDOSExt IMPORT RootNodePtr,RootNode,DosLibraryPtr,DosInfoPtr,DosInfo,
		        DeviceList,DeviceListPtr,DLTVolume,DLTDevice;
FROM Interrupts IMPORT Forbid,Permit;
FROM FileHandler IMPORT DeviceNodePtr,DeviceNode;
FROM System IMPORT DOSBase;
FROM SYSTEM IMPORT SHIFT,ADDRESS,LONGWORD,TSIZE;
FROM AmigaDOS IMPORT BPTR,BSTR;
FROM Memory IMPORT AllocMem,FreeMem,MemReqSet,MemClear,MemPublic;
FROM Strings IMPORT CopyString;

(*
CONST
 max=10;
*)
 
TYPE
 String=ARRAY[0..80] OF CHAR;

  VolumeListPtr = POINTER TO VolumeList;

  VolumeList = RECORD
                next:VolumeListPtr;
                Name:String;
               END;

VAR
 ListHead,ListTail:VolumeListPtr;

(*
VAR
 list:ARRAY[0..max] OF String; (* for storing the list of volumes *)
*)
 
(* =============================================== *)

PROCEDURE INCPtr(VAR pt:LONGWORD;n:LONGCARD);
	 (* this makes the compiler happy about incrementing pointers *)
VAR
 lc:LONGCARD;
BEGIN
 lc:=LONGCARD(pt);
 INC(lc,n);
 pt:=LONGWORD(lc);
END INCPtr;


(* ============================= *)

PROCEDURE PtrFromBPTR(BPtr:BPTR):ADDRESS;
BEGIN
  RETURN SHIFT(BPtr,2);  (* fast way to multiply by 4 *)
END PtrFromBPTR;

(* ============================= *)

PROCEDURE StringFromBCPL(BCpl:BSTR;VAR string:ARRAY OF CHAR);
 TYPE
   chptrType = POINTER TO CHAR;
 VAR 
 i,len:CARDINAL;
 chptr:chptrType;

BEGIN
 BCpl:=PtrFromBPTR(BCpl); (* convert to regular pointer *)
 chptr:=chptrType(BCpl);
 len:=CARDINAL(chptr^); (* length is in the first byte *)
 INCPtr(chptr,1);	(* point to first piece of data *)
IF len>=1 THEN
 FOR i:=0 TO len-1 DO
  string[i]:=chptr^;    (* put the data into the array *)
  INCPtr(chptr,1);	(* point to the next character *)
 END; (* FOR i *)
END; (* IF len *)
 string[len]:=CHR(0);   (* null terminate *)
END StringFromBCPL;

(* ============================= *)

PROCEDURE FreeList;

VAR
 curr,next:VolumeListPtr;
 
BEGIN (* OF FreeList *)
 curr:=ListHead; (* get the head of the list *)
 WHILE curr<>NIL DO (* check for hitting the end *)
  next:=curr^.next;		   (* remember next entry *)
  FreeMem(curr,TSIZE(VolumeList)); (* zap this entry *)
  curr:=next;	  		   (* point to the next entry *)
 END; (* WHILE *)
 ListHead:=NIL; (* mark the list as empty *)
 ListTail:=NIL; 
END FreeList;

(* ============================= *)
PROCEDURE AddEntry(VAR name:String);

 VAR
  ptr:VolumeListPtr;


BEGIN
  ptr:=AllocMem(TSIZE(VolumeList),MemReqSet{MemPublic,MemClear});
  
  IF ptr<>NIL THEN (* make sure allocmem worked *)
  
   ptr^.next:=NIL; (* this new entry doesn't point to anything yet *)
   CopyString(ptr^.Name,name); (* put data in the new entry *)

   IF ListTail<>NIL THEN (* list already exists *)
   
    ListTail^.next:=ptr; (* point old end to new entry *)
    ListTail:=ptr;	 (* now end is the new entry *)

   ELSE			 (* first time *)
   
    ListHead:=ptr;
    ListTail:=ptr;	 (* only one entry, so tail and head are the same *)
    
   END; (* IF ListTail *)
   
  END; (* IF ptr *)

END AddEntry;

(* ============================= *)
PROCEDURE getdisks();
VAR
 name:String;
 rnptr:RootNodePtr;
 dlp:DosLibraryPtr;
 diptr:DosInfoPtr;
 dnptr:DeviceNodePtr;
 temp:LONGCARD;
(* i,j:CARDINAL; *)
 ptr:VolumeListPtr;
 
BEGIN

 ListHead:=NIL; (* mark the list as empty *)
 ListTail:=NIL; 
 
 dlp:=DosLibraryPtr(DOSBase); (* get the pointer to the dos library *)
 
 IF dlp=NIL THEN 	      (* make sure it's open *)
  WriteString("DOSBase is NIL!");WriteLn;
 ELSE

  rnptr:=dlp^.dlRoot;
  diptr:=PtrFromBPTR(rnptr^.rnInfo);
  
  IF diptr=NIL THEN (* make sure the pointer is valid *)
   WriteString("DosInfoPtr is NIL!");WriteLn;
  ELSE

   Forbid; (* prevent the list from being changed while we look at it *)
  
   dnptr:=PtrFromBPTR(diptr^.diDevInfo); (* get Device list pointer *)

    WHILE dnptr<>NIL DO; (* check for being at the end of the list *)

     IF (dnptr^.dnType=DLTVolume) AND (dnptr^.dnTask<>NIL) THEN
	(* make sure this is a volume name AND and a disk type of device *)
	
       StringFromBCPL(dnptr^.dnName,name); (* convert to a regular string *)
       
       AddEntry(name);

     END; (* IF dnptr^.dnType AND .. *)

     dnptr:=PtrFromBPTR(dnptr^.dnNext); (* get next DeviceNode *)

    END; (* WHILE *)
    
   Permit;
   
  END; (* IF diptr *)
 END; (* IF dlp *)


 ptr:=ListHead;			  (* point to first entry *)
 WHILE ptr<>NIL DO		  (* check for the end of the list *)
  WriteString(ptr^.Name);WriteLn; (* show the name *)
  ptr:=ptr^.next;		  (* point to next entry *)
 END; (* WHILE *)


FreeList(); (* free up the memory the linked list uses *)

END getdisks;
(* ============================= *)

BEGIN (* of Module *)

getdisks();

END disks2.

