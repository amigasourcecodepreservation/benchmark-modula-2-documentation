MODULE input;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,WriteCard;
IMPORT Terminal;


VAR
 x:ARRAY[0..80] OF CHAR;

PROCEDURE KBDInput(VAR entered:ARRAY OF CHAR );
VAR
 c:CHAR;
 hi,i:CARDINAL;
 
BEGIN
  i:=0;
  hi:=HIGH(entered);
  entered[0]:=CHR(0);
  WriteString(">");
  c:=CHR(0);

 WHILE  (c<>CHR(10)) AND (c<>CHR(13)) DO
  IF c<>CHR(0) THEN
   IF i<(hi-1) THEN entered[i]:=c; i:=i+1; END;
  END; (* IF c *)
  Terminal.Read(c);
 END; (* WHILE *)
 
 entered[i]:=CHR(0); (* null terminate *)

END KBDInput;

BEGIN (* of Module *)
KBDInput(x);
WriteString(x);WriteLn;


END input.

