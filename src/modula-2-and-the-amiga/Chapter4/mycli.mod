MODULE mycli;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM AmigaDOS IMPORT FileHandle,Execute,Open,Close,ModeNewFile;
FROM SYSTEM IMPORT ADR;

PROCEDURE Cli();
VAR
 window:FileHandle;
 dummy:BOOLEAN;

BEGIN (* of Cli *)

window:=Open(ADR("CON:0/0/640/200/MyCli"),ModeNewFile);
IF window<>NIL THEN

 dummy:=Execute(ADR(""),window,0);
 
 Close(window);
END; (* IF window *)

END Cli;

BEGIN (* of Module *)

Cli;

END mycli.

