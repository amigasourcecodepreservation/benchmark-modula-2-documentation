MODULE mycli2;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn;
FROM AmigaDOS IMPORT FileHandle,Execute,Open,Close,ModeNewFile;
FROM SYSTEM IMPORT ADR;

PROCEDURE Cli();
VAR
 window:FileHandle;
 dummy:BOOLEAN;

BEGIN (* of Cli *)

window:=Open(ADR("NIL:"),ModeNewFile);
IF window<>NIL THEN

 dummy:=Execute(ADR("newcli"),0,window);
 
 Close(window);
END; (* IF window *)

END Cli;

BEGIN (* of Module *)

Cli;
WriteString("see, I have gone on with my business");WriteLn;

END mycli2.

