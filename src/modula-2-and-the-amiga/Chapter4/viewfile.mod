MODULE viewfile;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)


(* Benchmark Modules *)

FROM System IMPORT argc,argv,WBenchMsg;

FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM Workbench IMPORT WBArgPtr,WBStartup,WBStartupPtr; 
FROM SYSTEM IMPORT ADDRESS,LONGWORD,TSIZE,ADR; 
FROM AmigaDOSProcess IMPORT Delay;
FROM AmigaDOS IMPORT FileLock,Examine,ParentDir,FileInfoBlock,UnLock,
		     FileInfoBlockPtr,FileHandle,Open,Close,Read,ModeOldFile,
		     SigBreakCtrlC,ModeNewFile,Write;
FROM Strings IMPORT CopyString,ConcatString,StringLength;
FROM Memory IMPORT AllocMem,FreeMem,MemReqSet,MemPublic,MemClear;
FROM Tasks IMPORT SignalSet,SetSignal;

CONST
 maxchar=80;
 
VAR
 buf:ARRAY[0..maxchar] OF CHAR;
    
  TYPE
   MyPathType= ARRAY[0..500] OF CHAR;

  VAR
    TempPath: MyPathType;


(* ======================================== *)

PROCEDURE ShowFile(name:ARRAY OF CHAR;out:FileHandle);
VAR
fh:FileHandle;
NumRead,NumWritten:LONGINT;
done:BOOLEAN;

BEGIN
  
 fh:=Open(ADR(name),ModeOldFile);
 
 IF fh=NIL THEN
  WriteString("could not open file ");WriteString(name);WriteLn;
 ELSE
 
  done:=FALSE;
  
  REPEAT
   
   NumRead:=Read(fh,ADR(buf),maxchar);
   
   IF NumRead<=0D THEN 
    done:=TRUE; 
   ELSE
    buf[CARDINAL(NumRead)]:=CHAR(0); (* null terminate *)
    
    IF out=NIL THEN
     WriteString(buf); (* send to CLI window *)
    ELSE
     NumWritten:=Write(out,ADR(buf),NumRead); (* send to our CON window *)
    END; (* IF out *)
    
      (* check for CTRL C *)
    IF SigBreakCtrlC IN SetSignal(SignalSet{},SignalSet{SigBreakCtrlC}) THEN
     WriteLn; WriteString("**BREAK");WriteLn;
     Close(fh); RETURN; 
    END; (* IF SigBreakCtrlC *)
    
   END; (* IF *)
   
  UNTIL done;
       
  Close(fh);
  
 END; (* IF fh *)
 
END ShowFile;

      
(* ============================= *)
PROCEDURE GetPath(InputLock:FileLock;VAR Path:MyPathType);
VAR
  dir: ARRAY[0..115] OF CHAR;
  InputFIBPtr : FileInfoBlockPtr;	 (* ptr to the file info block   *)
					 
  FIBMemSize  : LONGCARD;                (* size in bytes of the   *)
                                         (* FileInfoBlock record   *)

PROCEDURE BuildPath(InputLock:FileLock);

PROCEDURE BuildString;  (* add to the front of the string we're building *)
BEGIN (* of BuildString *)
  CopyString(TempPath,Path);   (* copy old path to temporary storage *)
  CopyString(Path,dir);	       (* put new entry at front of path *)
  ConcatString(Path,TempPath); (* tack old path onto end of new entry *)
END BuildString;

BEGIN (* OF BuildPath *)

      IF Examine(InputLock, InputFIBPtr^) THEN  (* get InFile info block *)
        InputLock:=ParentDir(InputLock);
	
        IF InputLock=NIL THEN (* root dir = device *)

            IF StringLength(InputFIBPtr^.fibFileName)=0 THEN
              dir:="RAM:"; (* fix for 1.2 bug *)
            ELSE
  	      CopyString(dir,InputFIBPtr^.fibFileName);
              ConcatString(dir,":");  (* add : after device name, i.e df0: *)
            END; (* IF InputFIBPtr^ *)
	    
	    BuildString; (* add to the front of the string we're building *)

        ELSE (* dir *)

            CopyString(dir,InputFIBPtr^.fibFileName);
            ConcatString(dir,"/");	     (* add / after directory name *)
	    
	    BuildString; (* add to the front of the string we're building *)

            BuildPath(InputLock); (* note that BuildPath calls itself *)
	    
            UnLock(InputLock);    (* free memory from ParentDir call *)
        END; (* IF InputLock *)
      ELSE
        WriteString ("GetPath Couldn't examine the InputLock's info block");
	WriteLn;
      END; (* IF Examine() *)
END BuildPath;

BEGIN (* of GetPath *)

 TempPath:="";
 Path:="";

 FIBMemSize := TSIZE(FileInfoBlock);     (* get size of FileInfoBlock *)
 InputFIBPtr := AllocMem (FIBMemSize,MemReqSet{MemPublic,MemClear});
     						    (* assign long-   *)
                                                    (* aligned memory *)
 IF ADDRESS(InputFIBPtr) = NIL THEN
  WriteString("GetPath Couldn't allocate FileInfoBlock space");
  WriteLn;
 ELSE
  BuildPath(InputLock); (* go to it if the AllocMem worked *)
  FreeMem (InputFIBPtr, FIBMemSize);
 END; (* IF ADDRESS *);

END GetPath;

   
         
PROCEDURE test;
VAR
 j,NumProjects:LONGINT;
 i:CARDINAL;
 name:POINTER TO ARRAY[0..99] OF CHAR;
 path:MyPathType;			
 args:WBArgPtr;
 startmsg:WBStartupPtr;
 out:FileHandle;
BEGIN



IF WBenchMsg=NIL THEN (* CLI *)

IF argc>=2 THEN
 FOR i:=1 TO argc-1 DO
  ShowFile(argv^[i]^,NIL);
 END; (* FOR i *)
ELSE
 WriteString("usage: viewfile file1 file2 ..");WriteLn;
END; (* IF *)

ELSE (* Workbench *)
    startmsg:=WBStartupPtr(WBenchMsg);
    args := startmsg^.smArgList;
    
    NumProjects:=startmsg^.smNumArgs-1D;
    
   IF NumProjects>=1D THEN

  out:=Open(ADR("CON:0/0/640/200/viewfile"),ModeNewFile);
  IF out=NIL THEN
     WriteString("could not open output window");WriteLn;
  ELSE

    FOR j := 0D TO NumProjects DO
    
      GetPath(args^.waLock,path);	  
      
      name := ADDRESS(args^.waName);
      
      ConcatString(path,name^); (* put whole name together *)
      
      
      IF j>=1D THEN (* skip 0, which is the program's name *)
       ShowFile(path,out);
      END; (* IF j *)
      
      INC(LONGCARD(args),SIZE(args^))
      
    END; (* FOR j *)
    
  Delay(100); (* pause to view stuff in window *)
    
  Close(out); (* close the CONsole window *)
  
  END; (* IF out *)
    
   ELSE
    WriteString("shift click on files to view");WriteLn;
    Delay(100); (* pause to view stuff in window *)
   END; (* IF NumProjects *)
   
   
END; (* IF WBenchMsg *)
 

END test;
(* ============================= *)

BEGIN (* of MODULE *)

test;

END viewfile.

