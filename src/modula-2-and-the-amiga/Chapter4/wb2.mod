MODULE wb2;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)


(* Benchmark Modules *)

FROM System IMPORT argc,argv,WBenchMsg;

FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM Workbench IMPORT WBArgPtr,WBStartup,WBStartupPtr; 
FROM SYSTEM IMPORT ADDRESS,LONGWORD,TSIZE; 
FROM AmigaDOSProcess IMPORT Delay;
FROM AmigaDOS IMPORT FileLock,Examine,ParentDir,FileInfoBlock,UnLock,
		     FileInfoBlockPtr;
FROM Strings IMPORT CopyString,ConcatString,StringLength;
FROM Memory IMPORT AllocMem,FreeMem,MemReqSet,MemPublic,MemClear;
    
  TYPE
   MyPathType= ARRAY[0..500] OF CHAR;

  VAR
    TempPath: MyPathType;

(* ============================= *)
PROCEDURE GetPath(InputLock:FileLock;VAR Path:MyPathType);
VAR
  dir: ARRAY[0..115] OF CHAR;
  InputFIBPtr : FileInfoBlockPtr;	 (* ptr to the file info block   *)
					 
  FIBMemSize  : LONGCARD;                (* size in bytes of the   *)
                                         (* FileInfoBlock record   *)

PROCEDURE BuildPath(InputLock:FileLock);

PROCEDURE BuildString;  (* add to the front of the string we're building *)
BEGIN (* of BuildString *)
  CopyString(TempPath,Path);   (* copy old path to temporary storage *)
  CopyString(Path,dir);	       (* put new entry at front of path *)
  ConcatString(Path,TempPath); (* tack old path onto end of new entry *)
END BuildString;

BEGIN (* OF BuildPath *)

      IF Examine(InputLock, InputFIBPtr^) THEN  (* get InFile info block *)
        InputLock:=ParentDir(InputLock);
	
        IF InputLock=NIL THEN (* root dir = device *)

            IF StringLength(InputFIBPtr^.fibFileName)=0 THEN
              dir:="RAM:"; (* fix for 1.2 bug *)
            ELSE
  	      CopyString(dir,InputFIBPtr^.fibFileName);
              ConcatString(dir,":");  (* add : after device name, i.e df0: *)
            END; (* IF InputFIBPtr^ *)
	    
	    BuildString; (* add to the front of the string we're building *)

        ELSE (* dir *)

            CopyString(dir,InputFIBPtr^.fibFileName);
            ConcatString(dir,"/");	     (* add / after directory name *)
	    
	    BuildString; (* add to the front of the string we're building *)

            BuildPath(InputLock); (* note that BuildPath calls itself *)
	    
            UnLock(InputLock);    (* free memory from ParentDir call *)
        END; (* IF InputLock *)
      ELSE
        WriteString ("GetPath Couldn't examine the InputLock's info block");
	WriteLn;
      END; (* IF Examine() *)
END BuildPath;

BEGIN (* of GetPath *)

 TempPath:="";
 Path:="";

 FIBMemSize := TSIZE(FileInfoBlock);     (* get size of FileInfoBlock *)
 InputFIBPtr := AllocMem (FIBMemSize,MemReqSet{MemPublic,MemClear});
     						    (* assign long-   *)
                                                    (* aligned memory *)
 IF ADDRESS(InputFIBPtr) = NIL THEN
  WriteString("GetPath Couldn't allocate FileInfoBlock space");
  WriteLn;
 ELSE
  BuildPath(InputLock); (* go to it if the AllocMem worked *)
  FreeMem (InputFIBPtr, FIBMemSize);
 END; (* IF ADDRESS *);

END GetPath;

   
         
PROCEDURE test;
VAR
 j,NumProjects:LONGINT;
 i:CARDINAL;
 name:POINTER TO ARRAY[0..99] OF CHAR;
 path:MyPathType;			 (* new line *)
 args:WBArgPtr;
 startmsg:WBStartupPtr;
BEGIN



IF WBenchMsg=NIL THEN (* CLI *)

IF argc>=1 THEN
 FOR i:=0 TO argc-1 DO
  WriteString("argv[");WriteCard(i,3);WriteString("]=");
  WriteString(argv^[i]^);WriteLn;
 END; (* FOR i *)
END; (* IF *)

ELSE (* Workbench *)
    startmsg:=WBStartupPtr(WBenchMsg);
    args := startmsg^.smArgList;
    
    NumProjects:=startmsg^.smNumArgs-1D;
    FOR j := 0D TO NumProjects DO
    
      GetPath(args^.waLock,path);	  (* added these two lines *)
      WriteString(path);
      
      name := ADDRESS(args^.waName);
      WriteString(name^);WriteLn;
      
      INC(LONGCARD(args),SIZE(args^))
      
    END; (* FOR j *)
 
Delay(100); (* pause to view stuff in window *)

END; (* IF WBenchMsg *)
 

END test;
(* ============================= *)

BEGIN (* of MODULE *)

test;

END wb2.

