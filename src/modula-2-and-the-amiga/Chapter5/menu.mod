MODULE menu;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM Intuition IMPORT NewWindow, Window,WindowPtr, WindowFlagsSet,WindowFlags,
                      IDCMPFlags, IDCMPFlagsSet,WBenchScreen,OpenWindow,
		      CloseWindow,ModifyIDCMP,IntuiMessagePtr,Menu,MenuItem,
		      MenuFlags,MenuFlagsSet,SetMenuStrip,ClearMenuStrip,
		      MenuItemFlags,MenuItemFlagsSet,MenuItemMutualExcludeSet,
		      IntuiText,HighComp,MENUNUM,ITEMNUM,MenuNull,ItemAddress,
		      MenuItemPtr;
FROM AmigaDOSProcess IMPORT Delay;
FROM SYSTEM IMPORT BYTE,ADR;
FROM Tasks IMPORT Wait,SignalSet;
FROM Ports IMPORT GetMsg,ReplyMsg,MessagePtr;
FROM Rasters IMPORT DrawModeSet,Jam2;

VAR
 OurMenu:Menu;
 OurMenuItem:MenuItem;
 OurText:IntuiText;
 
(* --------------------------------------------- *)

PROCEDURE CreateMenu();
BEGIN

 WITH OurText DO (* Text of First Item of First Menu *)
  FrontPen:=BYTE(0);
  BackPen:=BYTE(1);
  DrawMode:=Jam2; (* =DrawModeSet{0}; put two colors *)
  LeftEdge:=2;
  TopEdge:=1;
  ITextFont:=NIL; (* use default font *)
  IText:=ADR("Quit");
  NextText:=NIL;
 END; (* WITH *)


 WITH OurMenuItem DO (* First Item of First Menu *)
  NextItem:=NIL; (* only one item for now, so this is NIL *)
  LeftEdge:=0;
  TopEdge:=2;
  Width:=65;
  Height:=12;
  Flags:=MenuItemFlagsSet{ItemText,ItemEnabled}+HighComp;
  MutualExclude:=MenuItemMutualExcludeSet{};
  ItemFill:=ADR(OurText);  (* point to the IntuiText data *)
  SelectFill:=NIL;
  Command:=BYTE(0);
  SubItem:=NIL; (* no subitems for now *)
 END; (* WITH *)

 WITH OurMenu DO (* First Menu *)
  NextMenu:=NIL;  (* only one menu for now, so this is NIL *)
  LeftEdge:=0;
  TopEdge:=0;
  Width:=65;
  Height:=12;
  Flags:=MenuFlagsSet{MenuEnabled};
  MenuName:=ADR("Project");
  FirstItem:=ADR(OurMenuItem); (* point to first menu item *)
 END; (* WITH *)
 
END CreateMenu;


(* --------------------------------------------- *)

PROCEDURE DoMenu(code:CARDINAL;VAR Quit:BOOLEAN);
 VAR
 MenuNum,ItemNum :CARDINAL;
 ad:MenuItemPtr;
 
BEGIN

WHILE (code<>MenuNull) DO
 
 MenuNum:=MENUNUM(code); (* decode the information *)
 ItemNum:=ITEMNUM(code);

 CASE MenuNum OF
  0: (* First Menu *)
  
   CASE ItemNum OF
    0: (* First Item of First Menu *) 
     Quit:=TRUE; |
    ELSE 
     (* ignore it *)
   END; (* CASE ItemNum *) |
  ELSE 
   (* ignore it *)
 END; (* CASE MenuNum *)
 
 ad:= ItemAddress(OurMenu,code);
 IF ad=NIL THEN
  code:=MenuNull; (* in case ItemAddress can't find the menu address *)
 ELSE
  code:=ad^.NextSelect; (* in case multiple items were selected *)
 END; (* IF ad *)
  
END; (* WHILE *)
 
END DoMenu;

(* --------------------------------------------- *)

PROCEDURE MainLoop(WinPtr:WindowPtr);
VAR
 Quit:BOOLEAN;
 signal,inset: SignalSet;
 insig1: CARDINAL;
 MsgPtr: IntuiMessagePtr;
 class : IDCMPFlagsSet;
 code  : CARDINAL;
 
BEGIN (* of MainLoop *)
 Quit:=FALSE;
 
 ModifyIDCMP(WinPtr^,IDCMPFlagsSet{Closewindow,MenuPick});
  (* ask for closewindow and MenuPick messages *)
    
  insig1:=CARDINAL(WinPtr^.UserPort^.mpSigBit); (* Window's message port *)
  inset:=SignalSet{};	(* clear the set *)
  INCL(inset,insig1);	(* ports to look for messages from *)

 REPEAT

  signal:=Wait(inset); (* wait for messages. (Go to sleep) *)
  
  (* we will wake up when the program receives a message *)
  
  IF (insig1 IN signal) THEN (* this is an IDCMP message from the window *)

  REPEAT (* in case there are multiple messages *)
    
   MsgPtr:=GetMsg(WinPtr^.UserPort^); (* get the message *)
   
   IF MsgPtr<>NIL THEN  (* make sure the message exists *)
   
    class:=MsgPtr^.Class;	(* remember these because they go away when
    				  we ReplyMsg. *)
				  
    code:=MsgPtr^.Code; (* new *)
    
    ReplyMsg(MessagePtr(MsgPtr)); (* Reply to the message *)
      
    IF (class=IDCMPFlagsSet{Closewindow}) THEN (* check for a close message *)
     Quit:=TRUE;     (* set our flag in response to the message *)
    ELSIF (class=IDCMPFlagsSet{MenuPick}) THEN
     DoMenu(code,Quit);
    END; (* IF *)
    
   END; (* IF MsgPtr *)
   
  UNTIL MsgPtr=NIL;
   
  END; (* IF insig1 IN signal *)
  
 UNTIL Quit; (* repeat until the Quit flag is TRUE *)
 
END MainLoop;

(* --------------------------------------------- *)

PROCEDURE TryWindow;
VAR
 WinPtr:WindowPtr;
 NewWin:NewWindow;
BEGIN

 
    WITH NewWin DO
      LeftEdge    := 10;
      TopEdge     := 10;
      Width       := 500;
      Height      := 100;
      DetailPen   := BYTE(0);
      BlockPen    := BYTE(1);
      Title       := ADR("MyWindow");
      Flags:=  WindowFlagsSet{Activate,WindowClose,WindowSizing,WindowDrag,
      			 WindowDepth,NoCareRefresh};

      IDCMPFlags  := IDCMPFlagsSet{};
      Type        := WBenchScreen;
      FirstGadget := NIL;
      CheckMark   := NIL;
      Screen      := NIL;
      BitMap      := NIL;
      MinWidth    := 20;
      MinHeight   := 20; 
      MaxWidth    := 640;
      MaxHeight   := 400;

    END; (* WITH NewWin *)


    WinPtr :=  OpenWindow (NewWin);

    IF WinPtr = NIL THEN 
      WriteString("Cannot open Window");WriteLn;
      Delay(120); (* give time to read message if run from Workbench *)
    ELSE 
    
     CreateMenu;          (* new *)
     	
     SetMenuStrip(WinPtr^,OurMenu); (* New. Attach menu to the window *)

     MainLoop(WinPtr);
     
     ClearMenuStrip(WinPtr^); (* New. Un-attach the menu strip *)
     
     CloseWindow(WinPtr^);
     
    END; (* IF WinPtr *)

  

END TryWindow;

BEGIN (* of Module *)

TryWindow;

END menu.

