MODULE menu2;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM Intuition IMPORT NewWindow, Window,WindowPtr, WindowFlagsSet,WindowFlags,
                      IDCMPFlags, IDCMPFlagsSet,WBenchScreen,OpenWindow,
		      CloseWindow,ModifyIDCMP,IntuiMessagePtr,Menu,MenuItem,
		      MenuFlags,MenuFlagsSet,SetMenuStrip,ClearMenuStrip,
		      MenuItemFlags,MenuItemFlagsSet,MenuItemMutualExcludeSet,
		      IntuiText,HighComp,MENUNUM,ITEMNUM,MenuNull,ItemAddress,
		      MenuItemPtr,CheckWidth,CommWidth,SUBNUM;
FROM AmigaDOSProcess IMPORT Delay;
FROM SYSTEM IMPORT BYTE,ADR,ADDRESS,SHIFT;
FROM Tasks IMPORT Wait,SignalSet;
FROM Ports IMPORT GetMsg,ReplyMsg,MessagePtr;
FROM Rasters IMPORT DrawModeSet,Jam2;
FROM Drawing IMPORT Move,SetAPen,SetBPen,SetDrMd;
FROM Text IMPORT Text;

TYPE
 LONGBITSET = SET OF [0..31];

VAR
 OurMenu:Menu;
 OurMenuItem,OurMenuItem2,OurMenuItem3,
  SubItem1,SubItem2,SubItem3,SubItem4:MenuItem;
 OurText,OurText2,OurText3,SubText1,SubText2,SubText3,SubText4:IntuiText;
 ThisSet,ExclSet : LONGBITSET;
 
 Screen,Printer,Icons:BOOLEAN;
 
 
(* --------------------------------------------- *)
PROCEDURE FillIntuiText(VAR Int:IntuiText;text:ADDRESS;
	left,top:INTEGER);
BEGIN

 WITH Int DO (* Text of Second Item of First Menu *)
  FrontPen:=BYTE(0);
  BackPen:=BYTE(1);
  DrawMode:=Jam2; (* =DrawModeSet{0}; put two colors *)
  LeftEdge:=left;
  TopEdge:=top;
  ITextFont:=NIL; (* use default font *)
  IText:=text;
  NextText:=NIL;
 END; (* WITH *)

END FillIntuiText;
 
(* --------------------------------------------- *)

PROCEDURE CreateMenu();
BEGIN

 ExclSet:=LONGBITSET{0..31};

 FillIntuiText(OurText,ADR("Quit"),2,1);  (* Text of First Item of First Menu *)

 WITH OurMenuItem DO (* First Item of First Menu *)
  NextItem:=ADR(OurMenuItem2); (* point to next menu item *)
  LeftEdge:=0;
  TopEdge:=2;
  Width:=65+CommWidth; (* added for keyboard shortcut *)
  Height:=12;
  Flags:=MenuItemFlagsSet{ItemText,ItemEnabled,CommSeq}+HighComp;
  	(* CommSeq added for keyboard shortcut *)
  MutualExclude:=MenuItemMutualExcludeSet{};
  ItemFill:=ADR(OurText);  (* point to the IntuiText data *)
  SelectFill:=NIL;
  Command:=BYTE('Q'); (* new. keyboard shortcut *)
  SubItem:=NIL; (* no subitems for now *)
 END; (* WITH *)

 FillIntuiText(OurText2,ADR("Icons"),2,1);

 WITH OurMenuItem2 DO (* Second Item of First Menu *)
  NextItem:=ADR(OurMenuItem3);
  LeftEdge:=0;
  TopEdge:=14;
  Width:=65;
  Height:=12;
  Flags:=MenuItemFlagsSet{ItemText,ItemEnabled}+HighComp;
  MutualExclude:=MenuItemMutualExcludeSet{};
  ItemFill:=ADR(OurText2);  (* point to the IntuiText data *)
  SelectFill:=NIL;
  Command:=BYTE(0);
  SubItem:=ADR(SubItem1); (* point to the first subitem *)
 END; (* WITH *)

 FillIntuiText(SubText1,ADR("Yes"),2+CheckWidth,1);

 FillIntuiText(SubText2,ADR("No"),2+CheckWidth,1);

ThisSet:=LONGBITSET(SHIFT(1,0)); (* 0 means first subitem *)

 WITH SubItem1 DO (* First SubItem of Second Item of First Menu *)
  NextItem:=ADR(SubItem2); (* point to next subitem *)
  LeftEdge:=60;
  TopEdge:=0;
  Width:=35+CheckWidth;
  Height:=12;
  Flags:=MenuItemFlagsSet{ItemText,ItemEnabled,CheckIt,Checked}+HighComp;
  MutualExclude:=MenuItemMutualExcludeSet(ExclSet/ThisSet);
  ItemFill:=ADR(SubText1);  (* point to the IntuiText data *)
  SelectFill:=NIL;
  Command:=BYTE(0);
  SubItem:=NIL;
 END; (* WITH *)
 
 Icons:=TRUE; (* Because SubItem1 has 'Checked' flag set *)

ThisSet:=LONGBITSET(SHIFT(1,1));  (* second 1 means second subitem *)

 WITH SubItem2 DO (* Second SubItem of Second Item of First Menu *)
  NextItem:=NIL; (* no more subitems *)
  LeftEdge:=60;
  TopEdge:=14;
  Width:=35+CheckWidth;
  Height:=12;
  Flags:=MenuItemFlagsSet{ItemText,ItemEnabled,CheckIt}+HighComp;
  MutualExclude:=MenuItemMutualExcludeSet(ExclSet/ThisSet);
  ItemFill:=ADR(SubText2);  (* point to the IntuiText data *)
  SelectFill:=NIL;
  Command:=BYTE(0);
  SubItem:=NIL; 
 END; (* WITH *)

 FillIntuiText(OurText3,ADR("Output"),2,1);

 WITH OurMenuItem3 DO (* Third Item of First Menu *)
  NextItem:=NIL; (* last item *)
  LeftEdge:=0;
  TopEdge:=14+14;
  Width:=65;
  Height:=12;
  Flags:=MenuItemFlagsSet{ItemText,ItemEnabled}+HighComp;
  MutualExclude:=MenuItemMutualExcludeSet{};
  ItemFill:=ADR(OurText3);  (* point to the IntuiText data *)
  SelectFill:=NIL;
  Command:=BYTE(0);
  SubItem:=ADR(SubItem3); (* point to the first subitem *)
 END; (* WITH *)

 FillIntuiText(SubText3,ADR("Screen"),2+CheckWidth,1);

 FillIntuiText(SubText4,ADR("Printer"),2+CheckWidth,1);


 WITH SubItem3 DO (* First SubItem of Third Item of First Menu *)
  NextItem:=ADR(SubItem4); (* point to next subitem *)
  LeftEdge:=60;
  TopEdge:=0;
  Width:=65+CheckWidth;
  Height:=12;
  Flags:=MenuItemFlagsSet{ItemText,ItemEnabled,CheckIt,Checked,MenuToggle}+HighComp;
  MutualExclude:=MenuItemMutualExcludeSet{};
  ItemFill:=ADR(SubText3);  (* point to the IntuiText data *)
  SelectFill:=NIL;
  Command:=BYTE(0);
  SubItem:=NIL;
 END; (* WITH *)
 
 Screen:=TRUE; (* because SubItem3 has 'Checked' flag set *)

 WITH SubItem4 DO (* Second SubItem of Third Item of First Menu *)
  NextItem:=NIL; (* no more subitems *)
  LeftEdge:=60;
  TopEdge:=14;
  Width:=65+CheckWidth;
  Height:=12;
  Flags:=MenuItemFlagsSet{ItemText,ItemEnabled,CheckIt,MenuToggle}+HighComp;
  MutualExclude:=MenuItemMutualExcludeSet{};
  ItemFill:=ADR(SubText4);  (* point to the IntuiText data *)
  SelectFill:=NIL;
  Command:=BYTE(0);
  SubItem:=NIL; 
 END; (* WITH *)

 Printer:=FALSE; (* because SubItem4 does not have 'Checked' flag set *)

 WITH OurMenu DO (* First Menu *)
  NextMenu:=NIL;  (* only one menu for now, so this is NIL *)
  LeftEdge:=0;
  TopEdge:=0;
  Width:=65;
  Height:=12;
  Flags:=MenuFlagsSet{MenuEnabled};
  MenuName:=ADR("Project");
  FirstItem:=ADR(OurMenuItem); (* point to first menu item *)
 END; (* WITH *)
 
END CreateMenu;

(* --------------------------------------------- *)
PROCEDURE UpdateWindow(WinPtr:WindowPtr);

PROCEDURE ShowFlag(flag:BOOLEAN);
BEGIN
 IF flag THEN
  Text(WinPtr^.RPort^,ADR("TRUE "),5);
 ELSE
  Text(WinPtr^.RPort^,ADR("FALSE"),5);
 END; (* IF flag *)
END ShowFlag;

BEGIN (* of UpdateWindow *)
 SetAPen(WinPtr^.RPort^,1);
 SetBPen(WinPtr^.RPort^,0);
 SetDrMd(WinPtr^.RPort^,Jam2);
 Move(WinPtr^.RPort^,15,20);
 Text(WinPtr^.RPort^,ADR("Icons   "),8);
 ShowFlag(Icons);
 Move(WinPtr^.RPort^,15,40);
 Text(WinPtr^.RPort^,ADR("Screen  "),8);
 ShowFlag(Screen);
 Move(WinPtr^.RPort^,15,60);
 Text(WinPtr^.RPort^,ADR("Printer "),8);
 ShowFlag(Printer);
   
END UpdateWindow;

(* --------------------------------------------- *)

PROCEDURE DoMenu(code:CARDINAL;VAR Quit:BOOLEAN);
 VAR
 MenuNum,ItemNum,SubItemNum:CARDINAL;
 ad:MenuItemPtr;
 
BEGIN

WHILE (code<>MenuNull) DO
 
 MenuNum:=MENUNUM(code); (* decode the information *)
 ItemNum:=ITEMNUM(code);
 SubItemNum:= SUBNUM(code); (* new *)
 
 
 ad:= ItemAddress(OurMenu,code);
 
 IF ad<> NIL THEN (* make sure ItemAddress found the item *)
 
 CASE MenuNum OF
  0: (* First Menu *)
  
   CASE ItemNum OF
    0: (* First Item of First Menu: Quit *) 
     Quit:=TRUE; |
     
    1: (* Second Item of First Menu: Icons *)
    
     CASE SubItemNum OF
     0: Icons:=TRUE;  | (* Yes *)
     1: Icons:=FALSE; | (* No *)
     ELSE (* ignore it *)
     END; (* CASE SubItemNum *)
     
    |
    2: (* Third Item of First Menu: Output *)
    
     CASE SubItemNum OF
     0: Screen:=Checked IN ad^.Flags; |  (* Screen *)
     1: Printer:=Checked IN ad^.Flags; | (* Printer *)
     ELSE (* ignore it *)
     END; (* CASE SubItemNum *)
    
    |
    ELSE 
     (* ignore it *)
   END; (* CASE ItemNum *) |
  ELSE 
   (* ignore it *)
 END; (* CASE MenuNum *)
 
code:=ad^.NextSelect; (* in case multiple items were selected *)

ELSE (* ad is NIL *)
 code:=MenuNull; (* in case ItemAddress can't find the menu address *)
END; (* IF ad *)
  
END; (* WHILE *)

  (* make sure flags are set correctly *)
  Screen:=Checked IN SubItem3.Flags; 
  Printer:=Checked IN SubItem4.Flags;
  Icons:=Checked IN SubItem1.Flags;


END DoMenu;

(* --------------------------------------------- *)

PROCEDURE MainLoop(WinPtr:WindowPtr);
VAR
 Quit:BOOLEAN;
 signal,inset: SignalSet;
 insig1: CARDINAL;
 MsgPtr: IntuiMessagePtr;
 class : IDCMPFlagsSet;
 code  : CARDINAL;
 
BEGIN (* of MainLoop *)
 Quit:=FALSE;
 
 ModifyIDCMP(WinPtr^,IDCMPFlagsSet{Closewindow,MenuPick});
  (* ask for closewindow and MenuPick messages *)
    
  insig1:=CARDINAL(WinPtr^.UserPort^.mpSigBit); (* Window's message port *)
  inset:=SignalSet{};	(* clear the set *)
  INCL(inset,insig1);	(* ports to look for messages from *)

 REPEAT

  signal:=Wait(inset); (* wait for messages. (Go to sleep) *)
  
  (* we will wake up when the program receives a message *)
  
  IF (insig1 IN signal) THEN (* this is an IDCMP message from the window *)

  REPEAT (* in case there are multiple messages *)
    
   MsgPtr:=GetMsg(WinPtr^.UserPort^); (* get the message *)
   
   IF MsgPtr<>NIL THEN  (* make sure the message exists *)
   
    class:=MsgPtr^.Class;	(* remember these because they go away when
    				  we ReplyMsg. *)
				  
    code:=MsgPtr^.Code; 
 
    ReplyMsg(MessagePtr(MsgPtr)); (* Reply to the message *)
      
    IF (class=IDCMPFlagsSet{Closewindow}) THEN (* check for a close message *)
     Quit:=TRUE;     (* set our flag in response to the message *)
    ELSIF (class=IDCMPFlagsSet{MenuPick}) THEN
     DoMenu(code,Quit);
     UpdateWindow(WinPtr);
    END; (* IF *)
    
   END; (* IF MsgPtr *)
   
  UNTIL MsgPtr=NIL;
   
  END; (* IF insig1 IN signal *)
  
 UNTIL Quit; (* repeat until the Quit flag is TRUE *)
 
END MainLoop;

(* --------------------------------------------- *)

PROCEDURE TryWindow;
VAR
 WinPtr:WindowPtr;
 NewWin:NewWindow;
BEGIN

 
    WITH NewWin DO
      LeftEdge    := 10;
      TopEdge     := 10;
      Width       := 500;
      Height      := 100;
      DetailPen   := BYTE(0);
      BlockPen    := BYTE(1);
      Title       := ADR("MyWindow");
      Flags:=  WindowFlagsSet{Activate,WindowClose,WindowSizing,WindowDrag,
      			 WindowDepth,NoCareRefresh};

      IDCMPFlags  := IDCMPFlagsSet{};
      Type        := WBenchScreen;
      FirstGadget := NIL;
      CheckMark   := NIL;
      Screen      := NIL;
      BitMap      := NIL;
      MinWidth    := 20;
      MinHeight   := 20; 
      MaxWidth    := 640;
      MaxHeight   := 400;

    END; (* WITH NewWin *)


    WinPtr :=  OpenWindow (NewWin);

    IF WinPtr = NIL THEN 
      WriteString("Cannot open Window");WriteLn;
      Delay(120); (* give time to read message if run from Workbench *)
    ELSE 
    
     CreateMenu;          
     	
     SetMenuStrip(WinPtr^,OurMenu); 

     UpdateWindow(WinPtr);  (* new *)
     
     MainLoop(WinPtr);
     
     ClearMenuStrip(WinPtr^); 
     
     CloseWindow(WinPtr^);
     
    END; (* IF WinPtr *)

  

END TryWindow;

BEGIN (* of Module *)

TryWindow;

END menu2.

