MODULE menu3;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

(* Our Modules *)
FROM OurMenus IMPORT FillMenu,FillMenuItem,
		FreeMenu,StartMutExcl,EndMutExcl,DoMenuItem,DummyProc;
		

(* Benchmark Modules *)
FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM Intuition IMPORT NewWindow, Window,WindowPtr, WindowFlagsSet,WindowFlags,
                      IDCMPFlags, IDCMPFlagsSet,WBenchScreen,OpenWindow,
		      CloseWindow,ModifyIDCMP,IntuiMessagePtr,Menu,MenuItem,
		      MenuFlags,MenuFlagsSet,SetMenuStrip,ClearMenuStrip,
		      MenuItemFlags,MenuItemFlagsSet,MenuItemMutualExcludeSet,
		      IntuiText,HighComp,MENUNUM,ITEMNUM,MenuNull,ItemAddress,
		      MenuItemPtr,CheckWidth,CommWidth,SUBNUM,
		      MenuPtr;
FROM AmigaDOSProcess IMPORT Delay;
FROM SYSTEM IMPORT BYTE,ADR,ADDRESS,SHIFT;
FROM Tasks IMPORT Wait,SignalSet;
FROM Ports IMPORT GetMsg,ReplyMsg,MessagePtr;
FROM Rasters IMPORT DrawModeSet,Jam2;
FROM Drawing IMPORT Move,SetAPen,SetBPen,SetDrMd;
FROM Text IMPORT Text;


VAR
 OurMenu:MenuPtr;
 SubItem1,SubItem2,SubItem3,SubItem4:MenuItemPtr;

 
 Screen,Printer,Icons,Quit:BOOLEAN;
 
(* --------------------------------------------- *)
PROCEDURE SetQuit;
BEGIN
 Quit:=TRUE;
END SetQuit;
(* --------------------------------------------- *)

PROCEDURE CreateMenu();
VAR
 OurMenu2:MenuPtr;
 OurMenuItem,OurMenuItem2:MenuItemPtr;
 OurSubItem,OurSubItem2:MenuItemPtr;
 
BEGIN



 OurMenu:=FillMenu(NIL,"Project");

 OurMenuItem:=FillMenuItem(OurMenu,NIL,NIL,"Quit",SetQuit,'Q',
 		MenuItemFlagsSet{}); 
		
 OurMenuItem2:=FillMenuItem(NIL,OurMenuItem,NIL,"Icons",DummyProc,CHR(0),
 	MenuItemFlagsSet{});
 
 	StartMutExcl();
 
   OurSubItem:=FillMenuItem(NIL,NIL,OurMenuItem2,"Yes",DummyProc,CHR(0),
 	  MenuItemFlagsSet{CheckIt,Checked});

	SubItem1:=OurSubItem; (* save in global for flag setting *)

        Icons:=TRUE; (* Because SubItem1 has 'Checked' flag set *)
   
  OurSubItem2:=FillMenuItem(NIL,OurSubItem,NIL,"No",DummyProc,CHR(0),
 	  MenuItemFlagsSet{CheckIt});
	  
	EndMutExcl();
 
 OurMenuItem:=FillMenuItem(NIL,OurMenuItem2,NIL,"Output",DummyProc,CHR(0),
 	MenuItemFlagsSet{});
 
 
  OurSubItem:=FillMenuItem(NIL,NIL,OurMenuItem,"Screen",DummyProc,CHR(0),
 	  MenuItemFlagsSet{CheckIt,Checked,MenuToggle});

 	 SubItem3:=OurSubItem;  (* save in global for flag setting *)
	
 	 Screen:=TRUE; (* because SubItem3 has 'Checked' flag set *)
   
  OurSubItem2:=FillMenuItem(NIL,OurSubItem,NIL,"Printer",DummyProc,CHR(0),
 	  MenuItemFlagsSet{CheckIt,MenuToggle});
	  
	 SubItem4:=OurSubItem2;  (* save in global for flag setting *)

	 Printer:=FALSE; (* because SubItem4 does not have 'Checked' flag set *)

END CreateMenu;

(* --------------------------------------------- *)
PROCEDURE UpdateWindow(WinPtr:WindowPtr);

PROCEDURE ShowFlag(flag:BOOLEAN);
BEGIN
 IF flag THEN
  Text(WinPtr^.RPort^,ADR("TRUE "),5);
 ELSE
  Text(WinPtr^.RPort^,ADR("FALSE"),5);
 END; (* IF flag *)
END ShowFlag;

BEGIN (* of UpdateWindow *)
 SetAPen(WinPtr^.RPort^,1);
 SetBPen(WinPtr^.RPort^,0);
 SetDrMd(WinPtr^.RPort^,Jam2);
 Move(WinPtr^.RPort^,15,20);
 Text(WinPtr^.RPort^,ADR("Icons   "),8);
 ShowFlag(Icons);
 Move(WinPtr^.RPort^,15,40);
 Text(WinPtr^.RPort^,ADR("Screen  "),8);
 ShowFlag(Screen);
 Move(WinPtr^.RPort^,15,60);
 Text(WinPtr^.RPort^,ADR("Printer "),8);
 ShowFlag(Printer);
   
END UpdateWindow;

(* --------------------------------------------- *)

PROCEDURE DoMenu(code:CARDINAL);
 
BEGIN

 DoMenuItem(OurMenu,code);

  (* make sure flags are set correctly *)
  Screen:=Checked IN SubItem3^.Flags; 
  Printer:=Checked IN SubItem4^.Flags;
  Icons:=Checked IN SubItem1^.Flags;


END DoMenu;

(* --------------------------------------------- *)

PROCEDURE MainLoop(WinPtr:WindowPtr);
VAR
 signal,inset: SignalSet;
 insig1: CARDINAL;
 MsgPtr: IntuiMessagePtr;
 class : IDCMPFlagsSet;
 code  : CARDINAL;
 
BEGIN (* of MainLoop *)
 Quit:=FALSE;
 
 ModifyIDCMP(WinPtr^,IDCMPFlagsSet{Closewindow,MenuPick});
  (* ask for closewindow and MenuPick messages *)
    
  insig1:=CARDINAL(WinPtr^.UserPort^.mpSigBit); (* Window's message port *)
  inset:=SignalSet{};	(* clear the set *)
  INCL(inset,insig1);	(* ports to look for messages from *)

 REPEAT

  signal:=Wait(inset); (* wait for messages. (Go to sleep) *)
  
  (* we will wake up when the program receives a message *)
  
  IF (insig1 IN signal) THEN (* this is an IDCMP message from the window *)

  REPEAT (* in case there are multiple messages *)
    
   MsgPtr:=GetMsg(WinPtr^.UserPort^); (* get the message *)
   
   IF MsgPtr<>NIL THEN  (* make sure the message exists *)
   
    class:=MsgPtr^.Class;	(* remember these because they go away when
    				  we ReplyMsg. *)
				  
    code:=MsgPtr^.Code; 
 
    ReplyMsg(MessagePtr(MsgPtr)); (* Reply to the message *)
      
    IF (class=IDCMPFlagsSet{Closewindow}) THEN (* check for a close message *)
     Quit:=TRUE;     (* set our flag in response to the message *)
    ELSIF (class=IDCMPFlagsSet{MenuPick}) THEN
     DoMenu(code);
     UpdateWindow(WinPtr);
    END; (* IF *)
    
   END; (* IF MsgPtr *)
   
  UNTIL MsgPtr=NIL;
   
  END; (* IF insig1 IN signal *)
  
 UNTIL Quit; (* repeat until the Quit flag is TRUE *)
 
END MainLoop;

(* --------------------------------------------- *)

PROCEDURE TryWindow;
VAR
 WinPtr:WindowPtr;
 NewWin:NewWindow;
BEGIN

 
    WITH NewWin DO
      LeftEdge    := 10;
      TopEdge     := 10;
      Width       := 500;
      Height      := 100;
      DetailPen   := BYTE(0);
      BlockPen    := BYTE(1);
      Title       := ADR("MyWindow");
      Flags:=  WindowFlagsSet{Activate,WindowClose,WindowSizing,WindowDrag,
      			 WindowDepth,NoCareRefresh};

      IDCMPFlags  := IDCMPFlagsSet{};
      Type        := WBenchScreen;
      FirstGadget := NIL;
      CheckMark   := NIL;
      Screen      := NIL;
      BitMap      := NIL;
      MinWidth    := 20;
      MinHeight   := 20; 
      MaxWidth    := 640;
      MaxHeight   := 400;

    END; (* WITH NewWin *)


    WinPtr :=  OpenWindow (NewWin);

    IF WinPtr = NIL THEN 
      WriteString("Cannot open Window");WriteLn;
      Delay(120); (* give time to read message if run from Workbench *)
    ELSE 
    
     CreateMenu;          

     IF OurMenu<> NIL THEN SetMenuStrip(WinPtr^,OurMenu^); END;

      UpdateWindow(WinPtr);  (* new *)
     
      MainLoop(WinPtr);
     
     IF OurMenu<> NIL THEN ClearMenuStrip(WinPtr^); END;
     
     CloseWindow(WinPtr^);
     
     FreeMenu(OurMenu); (* free up the memory for the menu stuff *)
     
    END; (* IF WinPtr *)

  

END TryWindow;

BEGIN (* of Module *)

TryWindow;

END menu3.
