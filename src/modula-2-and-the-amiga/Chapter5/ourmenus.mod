IMPLEMENTATION MODULE OurMenus;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

(* FROM InOut IMPORT WriteString,WriteLn,WriteCard; *)
FROM Intuition IMPORT NewWindow, Window,WindowPtr, WindowFlagsSet,WindowFlags,
                      IDCMPFlags, IDCMPFlagsSet,WBenchScreen,OpenWindow,
		      CloseWindow,ModifyIDCMP,IntuiMessagePtr,Menu,MenuItem,
		      MenuFlags,MenuFlagsSet,SetMenuStrip,ClearMenuStrip,
		      MenuItemFlags,MenuItemFlagsSet,MenuItemMutualExcludeSet,
		      IntuiText,HighComp,MENUNUM,ITEMNUM,MenuNull,ItemAddress,
		      MenuItemPtr,CheckWidth,CommWidth,SUBNUM,IntuiTextPtr,
		      IntuiTextLength,MenuPtr;
FROM SYSTEM IMPORT BYTE,ADR,ADDRESS,SHIFT,TSIZE;
FROM Rasters IMPORT DrawModeSet,Jam2;
FROM Memory IMPORT AllocMem,FreeMem,MemReqSet,MemPublic,MemChip,MemClear;
FROM Strings IMPORT StringLength,CopyString;

TYPE
 
 ShortString=ARRAY[0..30] OF CHAR;
 ShortStringPtr=POINTER TO ShortString;
 
 LONGBITSET = SET OF [0..31];
 OurMenuItemType=RECORD
 		   mi:MenuItem;
		   p:PROC;
		 END;
 OurMenuItemPtrType=POINTER TO OurMenuItemType;
	
VAR
 MutExcl:BOOLEAN;
 MenNum:CARDINAL;

(* --------------------------------------------- *)
PROCEDURE FillString(VAR text:ARRAY OF CHAR):ShortStringPtr;
VAR
 p:ShortStringPtr;
BEGIN
 p:=AllocMem(TSIZE(ShortString),MemReqSet{MemPublic,MemClear});
 IF p<>NIL THEN CopyString(p^,text); END;
 RETURN p;
END FillString;
(* --------------------------------------------- *)
PROCEDURE FreeString(VAR p:ShortStringPtr);
BEGIN
 IF p<>NIL THEN
  FreeMem(p,TSIZE(ShortString));
  p:=NIL;
 END; (* IF *)
END FreeString;
(* --------------------------------------------- *)

PROCEDURE DoMenuItem(OurMenu:MenuPtr;code:CARDINAL);
VAR
 ad:MenuItemPtr;
 p:PROC;
 our:OurMenuItemPtrType;
BEGIN

WHILE (code<>MenuNull) DO
 
 ad:= ItemAddress(OurMenu^,code);
  
 IF ad<> NIL THEN (* make sure ItemAddress found the item *)
 
  our:=OurMenuItemPtrType(ad); (* convert the pointer *)
 
  p:=our^.p; (* get the procedure *)
 
  IF ADDRESS(p)<>NIL THEN
   p; (* call the procedure *)
  END; (* IF *)
 
 
 code:=ad^.NextSelect; (* in case multiple items were selected *)

 ELSE (* ad is NIL *)
  code:=MenuNull; (* in case ItemAddress can't find the menu address *)
 END; (* IF ad *)
  
END; (* WHILE *)

END DoMenuItem;

(* --------------------------------------------- *)

PROCEDURE StartMutExcl();
BEGIN
 MutExcl:=TRUE;
 MenNum:=0;
END StartMutExcl;
(* --------------------------------------------- *)

PROCEDURE EndMutExcl();
BEGIN
 MutExcl:=FALSE;
END EndMutExcl; 
(* --------------------------------------------- *)
PROCEDURE FillIntuiText(text:ARRAY OF CHAR;left,top:INTEGER;
			 frontPen,backPen:CARDINAL):IntuiTextPtr;

VAR
 Int:IntuiTextPtr;
 string:ShortStringPtr;
BEGIN

 string:=FillString(text);
 
 IF string<>NIL THEN
 
  Int:=AllocMem(TSIZE(IntuiText),MemReqSet{MemPublic});
 
  IF Int<> NIL THEN

 
   WITH Int^ DO 
    FrontPen:=BYTE(frontPen);
    BackPen:=BYTE(backPen);
    DrawMode:=Jam2; (* =DrawModeSet{0}; put two colors *)
    LeftEdge:=left;
    TopEdge:=top;
    ITextFont:=NIL; (* use default font *)
    IText:=string;
    NextText:=NIL;
   END; (* WITH *)

  ELSE
   FreeString(string);
  END; (* IF Int *)
 
 END; (* IF string *)
 
 RETURN Int;
 
END FillIntuiText;
 
(* --------------------------------------------- *)


PROCEDURE FreeIntuiText(VAR Int:IntuiTextPtr);

VAR
 curr,next:IntuiTextPtr;

BEGIN
 curr:=Int;
 Int:=NIL;
 WHILE curr<>NIL DO  (* be sure to free everything *)
  next:=curr^.NextText;
  FreeString(ShortStringPtr(curr^.IText));
  FreeMem(curr,TSIZE(IntuiText));
  curr:=next;
 END; (* WHILE curr *)

END FreeIntuiText;

(* --------------------------------------------- *)
PROCEDURE FillMenuItem(PrevMenu:MenuPtr;PrevItem,PrevSub:MenuItemPtr;
		text:ARRAY OF CHAR;proc:PROC;ShortCut:CHAR;
		flags:MenuItemFlagsSet):MenuItemPtr;
		
VAR
 OurMenuItem:MenuItemPtr;
 OurText:IntuiTextPtr;
 left,top,topedge,leftedge:INTEGER;
 ThisSet: LONGBITSET;
 our:OurMenuItemPtrType;
BEGIN
  
  left:=2;
  top:=1;
  topedge:=2;
  leftedge:=0;
  
  IF PrevItem<>NIL THEN
   topedge:=topedge+PrevItem^.TopEdge+PrevItem^.Height;
   leftedge:=PrevItem^.LeftEdge;
  END;

  IF PrevSub<>NIL THEN
   leftedge:=leftedge+PrevSub^.Width-5;
  END;
      
  IF CheckIt IN flags THEN left:=left+CheckWidth; END;

  INCL(flags,ItemText);
  INCL(flags,ItemEnabled);
  flags:=flags+HighComp;
 
  OurText:=FillIntuiText(text,left,top,0,1);
  
 IF OurText<> NIL THEN
   
 OurMenuItem:=AllocMem(TSIZE(OurMenuItemType),MemReqSet{MemPublic});

IF OurMenuItem<>NIL THEN
 
  our:=OurMenuItemPtrType(OurMenuItem); (* convert the pointer *)
  our^.p:=proc; (* fill in the procedure to call later *)   
 
 WITH OurMenuItem^ DO 
  NextItem:=NIL;
  LeftEdge:=leftedge;
  TopEdge:=topedge;

  Width:=IntuiTextLength(OurText^)+16;
  IF ShortCut<>CHR(0) THEN Width:=Width+CommWidth; END;
  IF  CheckIt IN flags THEN Width:=Width+CheckWidth; END;

  Height:=12;
  Flags:=flags;
  IF ShortCut<>CHR(0) THEN INCL(Flags,CommSeq); END;
  MutualExclude:=MenuItemMutualExcludeSet{};
  
  IF MutExcl THEN
   ThisSet:=LONGBITSET(SHIFT(1,MenNum));
   MutualExclude:=MenuItemMutualExcludeSet(LONGBITSET{0..31}/ThisSet);
   MenNum:=MenNum+1;
  END; (* IF *)

  ItemFill:=OurText;  (* point to the IntuiText data *)
  SelectFill:=NIL;
  Command:=BYTE(ShortCut); (* new. keyboard shortcut *)
  SubItem:=NIL; (* no subitems for now *)
 END; (* WITH *)
 
  IF PrevMenu<>NIL THEN
   PrevMenu^.FirstItem:=OurMenuItem; 
  END; (* IF *)
 
  IF PrevItem<>NIL THEN
   PrevItem^.NextItem:=OurMenuItem; 
  END; (* IF *)
 
  IF PrevSub<>NIL THEN
   PrevSub^.SubItem:=OurMenuItem;
  END; (* IF *)

ELSE
 FreeIntuiText(OurText);  
END; (* IF OurMenuItem *)

END; (* IF OurText *)

RETURN OurMenuItem;

END FillMenuItem;
(* --------------------------------------------- *)
PROCEDURE FillMenu(PrevMenu:MenuPtr;title:ARRAY OF CHAR):MenuPtr;
VAR
 OurMenu:MenuPtr;
 leftedge:INTEGER;
 string:ShortStringPtr;
BEGIN

 string:=FillString(title);
 
 IF string<>NIL THEN
   

 leftedge:=0;
 
 IF PrevMenu<>NIL THEN
  leftedge:=PrevMenu^.LeftEdge+PrevMenu^.Width+10;
 END; (* IF *)
 
 OurMenu:=AllocMem(TSIZE(Menu),MemReqSet{MemPublic});

 IF OurMenu<>NIL THEN
  
 WITH OurMenu ^ DO 
  NextMenu:=NIL;
  LeftEdge:=leftedge;
  TopEdge:=0;
  Width:=9*StringLength(title);
  Height:=12;
  Flags:=MenuFlagsSet{MenuEnabled};
  MenuName:=string;
  FirstItem:=NIL;
 END; (* WITH *)
 
 IF PrevMenu<> NIL THEN
  PrevMenu^.NextMenu:=OurMenu;
 END; (* IF PrevMenu *)
 
ELSE
 FreeMem(OurMenu,TSIZE(Menu));
END; (* IF OurMenu *)

END; (* IF string *)

 RETURN OurMenu;
 
END FillMenu;
(* --------------------------------------------- *)

PROCEDURE FreeMenuItems(VAR OurMenuItem:MenuItemPtr);
VAR
 curr,next,sub,nextsub:MenuItemPtr;
 OurText:IntuiTextPtr;
BEGIN
 curr:=OurMenuItem;
 WHILE curr <>NIL DO
  OurText:=curr^.ItemFill;
  FreeIntuiText(OurText);
  next:=curr^.NextItem;
  
  sub:=curr^.SubItem;
   
  WHILE sub<>NIL DO
   OurText:=sub^.ItemFill;
   FreeIntuiText(OurText);
   nextsub:=sub^.NextItem;
   FreeMem(sub,TSIZE(OurMenuItemType));
   sub:=nextsub;
  END; (* WHILE *)
  
  
  FreeMem(curr,TSIZE(OurMenuItemType));
  curr:=next;
 END; (* WHILE *)
 OurMenuItem:=NIL;
END FreeMenuItems;
(* --------------------------------------------- *)

PROCEDURE FreeMenu(VAR OurMenu:MenuPtr);
 VAR
  curr,next:MenuPtr;
BEGIN
 curr:=OurMenu;
 WHILE curr <>NIL DO
  FreeMenuItems(curr^.FirstItem);
  next:=curr^.NextMenu;
  FreeString(ShortStringPtr(curr^.MenuName));
  FreeMem(curr,TSIZE(Menu));
  curr:=next;
 END; (* WHILE *)
 OurMenu:=NIL;
END FreeMenu;
(* --------------------------------------------- *)

BEGIN (* of Module *)

EndMutExcl;

END OurMenus.

