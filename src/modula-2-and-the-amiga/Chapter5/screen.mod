MODULE screen;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM Intuition IMPORT NewWindow, Window,WindowPtr, WindowFlagsSet,WindowFlags,
                      IDCMPFlags, IDCMPFlagsSet,WBenchScreen,OpenWindow,
		      CloseWindow,OpenScreen,CloseScreen,CustomScreen,
		      ScreenPtr,NewScreen;
FROM AmigaDOSProcess IMPORT Delay;
FROM SYSTEM IMPORT BYTE,ADR;
FROM Views   IMPORT ViewModes,ViewModesSet;


PROCEDURE TryScreen;
VAR
 WinPtr:WindowPtr;
 NewWin:NewWindow;
 ScrnPtr:ScreenPtr;
 NewScrn:NewScreen;
 
BEGIN

   
  WITH NewScrn DO

    LeftEdge     := 0;
    TopEdge      := 0;
    Width        := 640;
    Height       := 200;
    Depth        := 1;		(* 1 bit plane, so 2^1=2 colors *)
    DetailPen    := BYTE(0);
    BlockPen     := BYTE(1);
    ViewModes    := ViewModesSet{Hires};
    Type         := CustomScreen;
    Font         := NIL; (* use default font *)
    DefaultTitle := ADR("My Screen");
    Gadgets      := NIL;
    CustomBitMap := NIL;

  END; (* WITH NewScrn *)

  ScrnPtr := OpenScreen (NewScrn);

  IF ScrnPtr = NIL THEN
    WriteString("Cannot open the new custom screen!");WriteLn;
    Delay(120); (* pause for the error message *)
    
  ELSE (* screen is open *)
  

 
    WITH NewWin DO
      LeftEdge    := 10;
      TopEdge     := 10;
      Width       := 500;
      Height      := 100;
      DetailPen   := BYTE(0);
      BlockPen    := BYTE(1);
      Title       := ADR("MyWindow");
      Flags:=  WindowFlagsSet{Activate,WindowClose,WindowSizing,WindowDrag,
      			 WindowDepth,NoCareRefresh};

      IDCMPFlags  := IDCMPFlagsSet{};
      Type        := CustomScreen;     (* changed *)
      FirstGadget := NIL;
      CheckMark   := NIL;
      Screen      := ScrnPtr;          (* changed *)
      BitMap      := NIL;
      MinWidth    := 20;
      MinHeight   := 20; 
      MaxWidth    := 640;
      MaxHeight   := 400;

    END; (* WITH NewWin *)


    WinPtr :=  OpenWindow (NewWin);

    IF WinPtr = NIL THEN 
      WriteString("Cannot open Window");WriteLn;
      Delay(120); (* give time to read message if run from Workbench *)
    ELSE 


     Delay(120); (* pause to look at the window *)
     
     CloseWindow(WinPtr^);
     
    END; (* IF WinPtr *)

  Delay(120); (* pause to look at the screen *)
     
  CloseScreen(ScrnPtr^);
  
 END; (* IF ScrnPtr *)
  

END TryScreen;

BEGIN (* of Module *)

TryScreen;

END screen.

