MODULE window;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM Intuition IMPORT NewWindow, Window,WindowPtr, WindowFlagsSet,WindowFlags,
                      IDCMPFlags, IDCMPFlagsSet,WBenchScreen,OpenWindow,
		      CloseWindow;
FROM AmigaDOSProcess IMPORT Delay;
FROM SYSTEM IMPORT BYTE,ADR;

PROCEDURE TryWindow;
VAR
 WinPtr:WindowPtr;
 NewWin:NewWindow;
BEGIN

 
    WITH NewWin DO
      LeftEdge    := 10;
      TopEdge     := 10;
      Width       := 500;
      Height      := 100;
      DetailPen   := BYTE(0);
      BlockPen    := BYTE(1);
      Title       := ADR("MyWindow");
      Flags:=  WindowFlagsSet{Activate,WindowClose,WindowSizing,WindowDrag,
      			 WindowDepth,NoCareRefresh};

      IDCMPFlags  := IDCMPFlagsSet{};
      Type        := WBenchScreen;
      FirstGadget := NIL;
      CheckMark   := NIL;
      Screen      := NIL;
      BitMap      := NIL;
      MinWidth    := 20;
      MinHeight   := 20; 
      MaxWidth    := 640;
      MaxHeight   := 400;

    END; (* WITH NewWin *)


    WinPtr :=  OpenWindow (NewWin);

    IF WinPtr = NIL THEN 
      WriteString("Cannot open Window");WriteLn;
      Delay(120); (* give time to read message if run from Workbench *)
    ELSE 


     Delay(120); (* pause to look at the window *)
     
     CloseWindow(WinPtr^);
     
    END; (* IF WinPtr *)

  

END TryWindow;

BEGIN (* of Module *)

TryWindow;

END window.

