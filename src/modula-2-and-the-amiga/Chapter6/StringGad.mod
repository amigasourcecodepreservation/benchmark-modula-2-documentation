MODULE StringGad;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

(* Our Modules *)
FROM OurMenus IMPORT FillIntuiText,FreeIntuiText; 

(* Benchmark Modules *)
FROM Rasters IMPORT Jam1,Jam2; 
FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM LongInOut IMPORT WriteLongInt; (* new *)
FROM Intuition IMPORT NewWindow, Window,WindowPtr, WindowFlagsSet,WindowFlags,
                      IDCMPFlags, IDCMPFlagsSet,WBenchScreen,OpenWindow,
		      CloseWindow,ModifyIDCMP,IntuiMessagePtr,
		      GadgetPtr,Gadget,BoolGadget,GadgetFlagsSet, 
		      GadgetActivationSet,GadgetActivation,IntuiTextPtr,
		      GadgetMutualExcludeSet,Border,
		      StrGadget,StringInfo,ActivateGadget,StringInfoPtr; (* new *)
		      
FROM AmigaDOSProcess IMPORT Delay;
FROM SYSTEM IMPORT BYTE,ADR;
FROM Tasks IMPORT Wait,SignalSet;
FROM Ports IMPORT GetMsg,ReplyMsg,MessagePtr;

CONST
 BufSize=100; (* new *)

TYPE
 borderlineType=ARRAY[0..4] OF	(* new *)
 		 RECORD
		  X,Y:INTEGER;
		 END;

VAR
 GadList:GadgetPtr;	
 gad1,gad2,gad3,gad4:Gadget;
 border1,border2,border3:Border;

 borderlines1,borderlines2,borderlines3:borderlineType;
 		(* new *)
		
 SpecInfo,SpecInfo2:StringInfo; (* new *)
 Buf,Buf2,UnBuf:ARRAY[0..BufSize] OF CHAR; (* new *)
 
(* --------------------------------------------- *)
PROCEDURE FillBorder(VAR b:Border;VAR bl:borderlineType;w,h,shift:INTEGER); 
					(* new *)
BEGIN

 bl[0].X:=0;       bl[0].Y:=0;
 bl[1].X:=w+shift; bl[1].Y:=0;
 bl[2].X:=w+shift; bl[2].Y:=h+shift;
 bl[3].X:=0;       bl[3].Y:=h+shift;
 bl[4].X:=0;       bl[4].Y:=0;

 WITH b DO
  LeftEdge:=-shift;
  TopEdge:=-shift;
  FrontPen:=BYTE(1);
  BackPen:=BYTE(0);
  DrawMode:=Jam1;
  Count:=BYTE(5);
  XY:=ADR(bl);
  NextBorder:=NIL;
 END; (* WITH b *)
 
END FillBorder; 
   
(* --------------------------------------------- *)

PROCEDURE InitGadgets():GadgetPtr;  
VAR
 Int1,Int2:IntuiTextPtr;

BEGIN

Int1:=FillIntuiText("OK",5,4,1,0);
 
IF Int1=NIL THEN RETURN NIL; END;

Int2:=FillIntuiText("Cancel",5,4,1,0);
 
IF Int2=NIL THEN FreeIntuiText(Int1); RETURN NIL; END;

  FillBorder(border1,borderlines1,25,15,1);
  
  WITH gad1 DO
   NextGadget:=ADR(gad2); (* point to next gadget *)
   LeftEdge:=10;
   TopEdge:=45;
   Width:=25;
   Height:=15;
   Flags:= GadgetFlagsSet{};
   Activation:=GadgetActivationSet{RelVerify};
   GadgetType:=BoolGadget;
   GadgetRender:=ADR(border1); (* border *)
   SelectRender:=NIL;
   GadgetText:=Int1; (* intuitext *)
   MutualExclude:=GadgetMutualExcludeSet{};
   SpecialInfo:=NIL;
   GadgetID:=1;		(* our id number *)
   UserData:=NIL;
  END; (* WITH gad1 *)

  FillBorder(border2,borderlines2,60,15,1); (* new *)

  WITH gad2 DO
   NextGadget:=ADR(gad3); (* new *)
   LeftEdge:=80;
   TopEdge:=45;
   Width:=60;
   Height:=15;
   Flags:= GadgetFlagsSet{};
   Activation:=GadgetActivationSet{RelVerify};
   GadgetType:=BoolGadget;
   GadgetRender:=ADR(border2); (* border *)
   SelectRender:=NIL;
   GadgetText:=Int2; (* intuitext *)
   MutualExclude:=GadgetMutualExcludeSet{};
   SpecialInfo:=NIL;
   GadgetID:=2;		(* our id number *)
   UserData:=NIL;
  END; (* WITH gad2 *)

  
  Buf:=""; (* initial value *)
  
  WITH SpecInfo DO (* new *)
   Buffer:=ADR(Buf);
   UndoBuffer:=ADR(UnBuf);
   BufferPos:=0;
   MaxChars:=BufSize;
   DispPos:=0;
   AltKeyMap:=NIL;
  END; (* WITH SpecInfo *)

  FillBorder(border3,borderlines3,130,12,3); (* new *)

  WITH gad3 DO        (* new *)
   NextGadget:=ADR(gad4); 
   LeftEdge:=10;
   TopEdge:=20;
   Width:=130;
   Height:=12;
   Flags:= GadgetFlagsSet{};
   Activation:=GadgetActivationSet{RelVerify};
   GadgetType:=StrGadget;
   GadgetRender:=ADR(border3); (* border *)
   SelectRender:=NIL;
   GadgetText:=NIL; (* intuitext *)
   MutualExclude:=GadgetMutualExcludeSet{};
   SpecialInfo:=ADR(SpecInfo);
   GadgetID:=3;		(* our id number *)
   UserData:=NIL;
  END; (* WITH gad3 *)

  Buf2:="0"; (* initial value *)
    
  WITH SpecInfo2 DO (* new *)
   Buffer:=ADR(Buf2);
   UndoBuffer:=ADR(UnBuf);
   BufferPos:=0;
   MaxChars:=BufSize;
   DispPos:=0;
   AltKeyMap:=NIL;
  END; (* WITH SpecInfo2 *)

  WITH gad4 DO        (* new *)
   NextGadget:=NIL; 
   LeftEdge:=150;
   TopEdge:=20;
   Width:=130;
   Height:=12;
   Flags:= GadgetFlagsSet{};
   Activation:=GadgetActivationSet{RelVerify,LongInt}; (* integer gadget *)
   GadgetType:=StrGadget;
   GadgetRender:=ADR(border3); (* border *)
   SelectRender:=NIL;
   GadgetText:=NIL; (* intuitext *)
   MutualExclude:=GadgetMutualExcludeSet{};
   SpecialInfo:=ADR(SpecInfo2);
   GadgetID:=4;		(* our id number *)
   UserData:=NIL;
  END; (* WITH gad4 *)
  
  RETURN ADR(gad1);


END  InitGadgets;
(* --------------------------------------------- *)
PROCEDURE FreeGadgets(VAR g:GadgetPtr); 
BEGIN

 
 WHILE g<>NIL DO 
  IF g^.GadgetText<>NIL THEN FreeIntuiText(g^.GadgetText); END;
  g:= g^.NextGadget;
 END; (* WHILE *)
 
 g:=NIL;
 

END FreeGadgets;
(* --------------------------------------------- *)

PROCEDURE MainLoop(WinPtr:WindowPtr);
VAR
 Quit:BOOLEAN;
 signal,inset: SignalSet;
 insig1: CARDINAL;
 MsgPtr: IntuiMessagePtr;
 class : IDCMPFlagsSet;
 WhichGadPtr:GadgetPtr; 
 SpecPtr:StringInfoPtr; (* new *)
 
BEGIN (* of MainLoop *)
 Quit:=FALSE;
 					
 ModifyIDCMP(WinPtr^,IDCMPFlagsSet{Closewindow,GadgetUp});
  (* ask for closewindow messages and GadgetUp messages *)
    
  insig1:=CARDINAL(WinPtr^.UserPort^.mpSigBit); (* Window's message port *)
  inset:=SignalSet{};	(* clear the set *)
  INCL(inset,insig1);	(* ports to look for messages from *)

 REPEAT

  signal:=Wait(inset); (* wait for messages. (Go to sleep) *)
  
  (* we will wake up when the program receives a message *)
  
  IF (insig1 IN signal) THEN (* this is an IDCMP message from the window *)
  
   MsgPtr:=GetMsg(WinPtr^.UserPort^); (* get the message *)
   
   IF MsgPtr<>NIL THEN  (* make sure the message exists *)
   
    class:=MsgPtr^.Class;	(* remember this because it goes away when
    				   we ReplyMsg. *)
				   
    WhichGadPtr :=GadgetPtr(MsgPtr^.IAddress); 
    
    ReplyMsg(MessagePtr(MsgPtr)); (* Reply to the message *)
      
    IF (class=IDCMPFlagsSet{Closewindow}) THEN (* check for a close message *)
     Quit:=TRUE;     (* set our flag in response to the message *)
    ELSIF (class=IDCMPFlagsSet{GadgetUp}) THEN (* check for a gadget *) 
    
     WriteString("got gadget number ");
     WriteCard(WhichGadPtr^.GadgetID,3);WriteLn;

     IF WhichGadPtr^.GadgetID=3 THEN (* new *)
      WriteString("string gad= ");WriteString(Buf);WriteLn;
     END; (* IF 3 *)

     IF WhichGadPtr^.GadgetID=4 THEN (* new *)
      WriteString("Integer gad= ");
      SpecPtr:=StringInfoPtr(WhichGadPtr^.SpecialInfo);
      IF SpecPtr<>NIL THEN
       WriteLongInt(SpecPtr^.LongInt,6);
       WriteLn;
      END; (* IF SpecPtr *)
     END; (* IF 4 *)
     
    END; (* IF *)
    
   END; (* IF MsgPtr *)
   
  END; (* IF insig1 IN signal *)
  
 UNTIL Quit; (* repeat until the Quit flag is TRUE *)
 
END MainLoop;

(* --------------------------------------------- *)

PROCEDURE TryWindow;
VAR
 WinPtr:WindowPtr;
 NewWin:NewWindow;
 dum:BOOLEAN;  (* new *)
 
BEGIN

GadList:=InitGadgets(); 

IF GadList<>NIL THEN  
 
    WITH NewWin DO
      LeftEdge    := 10;
      TopEdge     := 10;
      Width       := 500;
      Height      := 100;
      DetailPen   := BYTE(0);
      BlockPen    := BYTE(1);
      Title       := ADR("MyWindow");
      Flags:=  WindowFlagsSet{Activate,WindowClose,WindowSizing,WindowDrag,
      			 WindowDepth,NoCareRefresh};

      IDCMPFlags  := IDCMPFlagsSet{};
      Type        := WBenchScreen;
      FirstGadget := GadList;		 
      CheckMark   := NIL;
      Screen      := NIL;
      BitMap      := NIL;
      MinWidth    := 20;
      MinHeight   := 20; 
      MaxWidth    := 640;
      MaxHeight   := 400;

    END; (* WITH NewWin *)


    WinPtr :=  OpenWindow (NewWin);

    IF WinPtr = NIL THEN 
      WriteString("Cannot open Window");WriteLn;
      Delay(120); (* give time to read message if run from Workbench *)
    ELSE 

     dum:=ActivateGadget(gad3,WinPtr^,NIL); (* new *)

     MainLoop(WinPtr);	(* was Delay() before *)
     
     CloseWindow(WinPtr^);
     
    END; (* IF WinPtr *)
    
    
 FreeGadgets(GadList); 

ELSE 
 WriteString("Can't allocate gadgets");WriteLn;
 Delay(120); (* give time to read message if run from Workbench *)
END; (* IF GadList *)   

END TryWindow;

BEGIN (* of Module *)

TryWindow;

END StringGad.

