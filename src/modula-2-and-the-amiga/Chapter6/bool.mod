MODULE Bool;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

(* Our Modules *)
FROM OurMenus IMPORT FillIntuiText,FreeIntuiText; (* new *)

(* Benchmark Modules *)
FROM Rasters IMPORT Jam1,Jam2; (* new *)
FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM Intuition IMPORT NewWindow, Window,WindowPtr, WindowFlagsSet,WindowFlags,
                      IDCMPFlags, IDCMPFlagsSet,WBenchScreen,OpenWindow,
		      CloseWindow,ModifyIDCMP,IntuiMessagePtr,
		      GadgetPtr,Gadget,BoolGadget,GadgetFlagsSet, (* new *)
		      GadgetActivationSet,GadgetActivation,IntuiTextPtr,
		      GadgetMutualExcludeSet,Border;
		      
FROM AmigaDOSProcess IMPORT Delay;
FROM SYSTEM IMPORT BYTE,ADR;
FROM Tasks IMPORT Wait,SignalSet;
FROM Ports IMPORT GetMsg,ReplyMsg,MessagePtr;


VAR
 GadList:GadgetPtr;	(* new *)
 gad1,gad2:Gadget;
 border1:Border;

 borderlines:ARRAY[0..4] OF  
	   RECORD
	     X,Y :INTEGER;
	   END;

(* --------------------------------------------- *)
PROCEDURE FillBorder(VAR b:Border;w,h:INTEGER); (* new *)

BEGIN

 borderlines[0].X:=0; borderlines[0].Y:=0;
 borderlines[1].X:=w+1; borderlines[1].Y:=0;
 borderlines[2].X:=w+1; borderlines[2].Y:=h+1;
 borderlines[3].X:=0; borderlines[3].Y:=h+1;
 borderlines[4].X:=0; borderlines[4].Y:=0;

 WITH b DO
  LeftEdge:=-1;
  TopEdge:=-1;
  FrontPen:=BYTE(1);
  BackPen:=BYTE(0);
  DrawMode:=Jam1;
  Count:=BYTE(5);
  XY:=ADR(borderlines);
  NextBorder:=NIL;
 END; (* WITH b *)
 
END FillBorder; 
   
(* --------------------------------------------- *)

PROCEDURE InitGadgets():GadgetPtr;  (* new *)
VAR
 Int1,Int2:IntuiTextPtr;

BEGIN

Int1:=FillIntuiText("Start",5,4,1,0);
 
IF Int1=NIL THEN RETURN NIL; END;

Int2:=FillIntuiText("Stop",5,4,1,0);
 
IF Int2=NIL THEN FreeIntuiText(Int1); RETURN NIL; END;

  FillBorder(border1,50,15);
  
  WITH gad1 DO
   NextGadget:=ADR(gad2); (* point to next gadget *)
   LeftEdge:=10;
   TopEdge:=15;
   Width:=50;
   Height:=15;
   Flags:= GadgetFlagsSet{};
   Activation:=GadgetActivationSet{RelVerify};
   GadgetType:=BoolGadget;
   GadgetRender:=ADR(border1); (* border *)
   SelectRender:=NIL;
   GadgetText:=Int1; (* intuitext *)
   MutualExclude:=GadgetMutualExcludeSet{};
   SpecialInfo:=NIL;
   GadgetID:=1;		(* our id number *)
   UserData:=NIL;
  END; (* WITH gad1 *)


  WITH gad2 DO
   NextGadget:=NIL;
   LeftEdge:=10;
   TopEdge:=45;
   Width:=50;
   Height:=15;
   Flags:= GadgetFlagsSet{};
   Activation:=GadgetActivationSet{RelVerify};
   GadgetType:=BoolGadget;
   GadgetRender:=ADR(border1); (* border *)
   SelectRender:=NIL;
   GadgetText:=Int2; (* intuitext *)
   MutualExclude:=GadgetMutualExcludeSet{};
   SpecialInfo:=NIL;
   GadgetID:=2;		(* our id number *)
   UserData:=NIL;
  END; (* WITH gad2 *)
  
  RETURN ADR(gad1);


END  InitGadgets;
(* --------------------------------------------- *)
PROCEDURE FreeGadgets(VAR g:GadgetPtr); (* new *)
BEGIN

 
 WHILE g<>NIL DO 
  IF g^.GadgetText<>NIL THEN FreeIntuiText(g^.GadgetText); END;
  g:= g^.NextGadget;
 END; (* WHILE *)
 
 g:=NIL;
 

END FreeGadgets;
(* --------------------------------------------- *)

PROCEDURE MainLoop(WinPtr:WindowPtr);
VAR
 Quit:BOOLEAN;
 signal,inset: SignalSet;
 insig1: CARDINAL;
 MsgPtr: IntuiMessagePtr;
 class : IDCMPFlagsSet;
 WhichGadPtr:GadgetPtr; (* new *)
 
BEGIN (* of MainLoop *)
 Quit:=FALSE;
 					(* GadetUp is new *)
 ModifyIDCMP(WinPtr^,IDCMPFlagsSet{Closewindow,GadgetUp});
  (* ask for closewindow messages and GadgetUp messages *)
    
  insig1:=CARDINAL(WinPtr^.UserPort^.mpSigBit); (* Window's message port *)
  inset:=SignalSet{};	(* clear the set *)
  INCL(inset,insig1);	(* ports to look for messages from *)

 REPEAT

  signal:=Wait(inset); (* wait for messages. (Go to sleep) *)
  
  (* we will wake up when the program receives a message *)
  
  IF (insig1 IN signal) THEN (* this is an IDCMP message from the window *)
  
   MsgPtr:=GetMsg(WinPtr^.UserPort^); (* get the message *)
   
   IF MsgPtr<>NIL THEN  (* make sure the message exists *)
   
    class:=MsgPtr^.Class;	(* remember this because it goes away when
    				   we ReplyMsg. *)
				   
    WhichGadPtr :=GadgetPtr(MsgPtr^.IAddress); (* new *)
    
    ReplyMsg(MessagePtr(MsgPtr)); (* Reply to the message *)
      
    IF (class=IDCMPFlagsSet{Closewindow}) THEN (* check for a close message *)
     Quit:=TRUE;     (* set our flag in response to the message *)
    ELSIF (class=IDCMPFlagsSet{GadgetUp}) THEN (* check for a gadget *) (* new *)
     WriteString("got gadget number ");
     WriteCard(WhichGadPtr^.GadgetID,3);WriteLn;
    END; (* IF *)
    
   END; (* IF MsgPtr *)
   
  END; (* IF insig1 IN signal *)
  
 UNTIL Quit; (* repeat until the Quit flag is TRUE *)
 
END MainLoop;

(* --------------------------------------------- *)

PROCEDURE TryWindow;
VAR
 WinPtr:WindowPtr;
 NewWin:NewWindow;

BEGIN

GadList:=InitGadgets(); (* new *)

IF GadList<>NIL THEN  (* new *)
 
    WITH NewWin DO
      LeftEdge    := 10;
      TopEdge     := 10;
      Width       := 500;
      Height      := 100;
      DetailPen   := BYTE(0);
      BlockPen    := BYTE(1);
      Title       := ADR("MyWindow");
      Flags:=  WindowFlagsSet{Activate,WindowClose,WindowSizing,WindowDrag,
      			 WindowDepth,NoCareRefresh};

      IDCMPFlags  := IDCMPFlagsSet{};
      Type        := WBenchScreen;
      FirstGadget := GadList;		 (* new *)
      CheckMark   := NIL;
      Screen      := NIL;
      BitMap      := NIL;
      MinWidth    := 20;
      MinHeight   := 20; 
      MaxWidth    := 640;
      MaxHeight   := 400;

    END; (* WITH NewWin *)


    WinPtr :=  OpenWindow (NewWin);

    IF WinPtr = NIL THEN 
      WriteString("Cannot open Window");WriteLn;
      Delay(120); (* give time to read message if run from Workbench *)
    ELSE 


     MainLoop(WinPtr);	(* was Delay() before *)
     
     CloseWindow(WinPtr^);
     
    END; (* IF WinPtr *)
    
    
 FreeGadgets(GadList); (* new *)

ELSE (* new *)
 WriteString("Can't allocate gadgets");WriteLn;
 Delay(120); (* give time to read message if run from Workbench *)
END; (* IF GadList *)   (* new *)

END TryWindow;

BEGIN (* of Module *)

TryWindow;

END Bool.

