IMPLEMENTATION MODULE OurGadgets;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

(* Our Modules *)
FROM OurMenus IMPORT FillIntuiText,FreeIntuiText; 

(* Benchmark Modules *)
FROM Intuition IMPORT NewWindow, Window,WindowPtr, WindowFlagsSet,WindowFlags,
                      IDCMPFlags, IDCMPFlagsSet,WBenchScreen,OpenWindow,
		      CloseWindow,ModifyIDCMP,IntuiMessagePtr,
		      GadgetPtr,Gadget,BoolGadget,GadgetFlagsSet, 
		      GadgetActivationSet,GadgetActivation,IntuiTextPtr,
		      GadgetMutualExcludeSet,Border,BorderPtr,
		      StrGadget,StringInfo,ActivateGadget,StringInfoPtr,
		      PropInfo,PropGadget,KnobVMin,KnobHMin,PropInfoFlags,
		      PropInfoFlagsSet,PropInfoPtr,Image,MaxBody,MaxPot,
		      IntuiTextLength,ImagePtr;
		      
FROM Memory IMPORT AllocMem,FreeMem,MemReqSet,MemPublic,MemClear,MemChip;
FROM Rasters IMPORT Jam1,Jam2; 
FROM SYSTEM IMPORT BYTE,ADR,TSIZE,ADDRESS;
FROM Strings IMPORT CopyString,StringLength;
FROM Conversions IMPORT ConvNumberToString;

CONST
 BufSize=100;
 

TYPE
 borderlinePtr=POINTER TO borderlineType;

 borderlineType=ARRAY[0..4] OF	(* new *)
 		 RECORD
		  X,Y:INTEGER;
		 END;
		 
 VAR
  UnBuf:ARRAY[0..BufSize] OF CHAR;

(* ============================================== *)

PROCEDURE MakeBorder(w,h,shift:INTEGER):BorderPtr;
VAR
 bl:borderlinePtr;
 b:BorderPtr;
BEGIN
 bl:=AllocMem(TSIZE(borderlineType),MemReqSet{MemPublic,MemClear});
 IF bl=NIL THEN RETURN NIL; END;

 b:=AllocMem(TSIZE(Border),MemReqSet{MemPublic,MemClear});
 
 IF b=NIL THEN FreeMem(bl,TSIZE(borderlineType)); RETURN NIL; END;
 
 bl^[0].X:=0;       bl^[0].Y:=0;
 bl^[1].X:=w+shift; bl^[1].Y:=0;
 bl^[2].X:=w+shift; bl^[2].Y:=h+shift;
 bl^[3].X:=0;       bl^[3].Y:=h+shift;
 bl^[4].X:=0;       bl^[4].Y:=0;

 WITH b^ DO
  LeftEdge:=-shift;
  TopEdge:=-shift;
  FrontPen:=BYTE(1);
  BackPen:=BYTE(0);
  DrawMode:=Jam1;
  Count:=BYTE(5);
  XY:=bl;
  NextBorder:=NIL;
 END; (* WITH b *)

 RETURN b;

END MakeBorder;
(* ============================================== *)

PROCEDURE FreeBorder(VAR bor:BorderPtr);
BEGIN
IF bor<>NIL THEN

 IF bor^.XY<>NIL THEN
   FreeMem(bor^.XY,TSIZE(borderlineType)); 
 END; (* IF XY *)
 FreeMem(bor,TSIZE(Border));
 bor:=NIL; 
END; (* IF bor *)
END FreeBorder;
(* ============================================== *)

PROCEDURE MakeBool(prev:GadgetPtr;text:ARRAY OF CHAR;
		   id,left,top:CARDINAL):GadgetPtr;
VAR
 Int:IntuiTextPtr;
 gad:GadgetPtr;
 b:BorderPtr;
 w,h:CARDINAL;
BEGIN

Int:=FillIntuiText(text,2,2,1,0); (* was 5,4,1,0 *)
 
IF Int=NIL THEN RETURN NIL; END;

w:=IntuiTextLength(Int^)+3;
h:=8+3; (* 8 is the default font size *)

b:=MakeBorder(w,h,1);

IF b=NIL THEN FreeIntuiText(Int); RETURN NIL; END;
   
gad:=AllocMem(TSIZE(Gadget),MemReqSet{MemPublic,MemClear});

IF gad=NIL THEN FreeIntuiText(Int); FreeBorder(b); RETURN NIL; END;
    
  WITH gad^ DO
   NextGadget:=NIL;
   LeftEdge:=left;
   TopEdge:=top;
   Width:=w;
   Height:=h;
   Flags:= GadgetFlagsSet{};
   Activation:=GadgetActivationSet{RelVerify};
   GadgetType:=BoolGadget;
   GadgetRender:=b; (* border *)
   SelectRender:=NIL;
   GadgetText:=Int; (* intuitext *)
   MutualExclude:=GadgetMutualExcludeSet{};
   SpecialInfo:=NIL;
   GadgetID:=id;	(* our id number *)
   UserData:=NIL;
  END; (* WITH gad *)
  
  IF prev<>NIL THEN prev^.NextGadget:=gad; END;

  RETURN gad;
  
END MakeBool;
(* ============================================== *)

PROCEDURE FreeGad(VAR gad:GadgetPtr);
VAR
 SpecInfo:StringInfoPtr;
 Prop:PropInfoPtr;
BEGIN
 IF gad<> NIL THEN
  IF gad^.GadgetText<>NIL THEN FreeIntuiText(gad^.GadgetText); END;

  IF gad^.GadgetType<>PropGadget THEN
   IF gad^.GadgetRender<>NIL THEN FreeBorder(BorderPtr(gad^.GadgetRender)); END;
  END; (* IF *)

  IF gad^.GadgetType=StrGadget THEN
   SpecInfo:=gad^.SpecialInfo;
   IF SpecInfo<>NIL THEN
    IF SpecInfo^.Buffer<>NIL THEN FreeMem(SpecInfo^.Buffer,BufSize); END;
    FreeMem(SpecInfo,TSIZE(StringInfo));
   END; (* IF *)
  END; (* IF *)

  IF gad^.GadgetType=PropGadget THEN
   IF gad^.GadgetRender<>NIL THEN FreeMem(gad^.GadgetRender,TSIZE(Image)); END;
   Prop:=gad^.SpecialInfo;
   IF Prop<>NIL THEN
    FreeMem(Prop,TSIZE(PropInfo));
   END; (* IF *)
  END; (* IF *)
         
  FreeMem(gad,TSIZE(Gadget));
  gad:=NIL;
 END; (* IF gad *)
END FreeGad;
(* ============================================== *)

PROCEDURE FreeGadgets(VAR gadlist:GadgetPtr);
VAR
 curr,next:GadgetPtr;
BEGIN
 curr:=gadlist;
 WHILE curr<>NIL DO
  next:=curr^.NextGadget;
  FreeGad(curr);
  curr:=next;
 END; (* WHILE *)
 gadlist:=NIL;
END FreeGadgets;
(* ============================================== *)

PROCEDURE MakeaString(prev:GadgetPtr;init:ARRAY OF CHAR;
			ActFlags:GadgetActivationSet;
			id,left,top,w,h:CARDINAL):GadgetPtr;
TYPE
 St=POINTER TO ARRAY[0..BufSize] OF CHAR;
VAR
 b:BorderPtr;
 gad:GadgetPtr;
 Buf:ADDRESS;
 StPtr:St;
 SpecInfo:StringInfoPtr;
BEGIN
  
  Buf:=AllocMem(BufSize,MemReqSet{MemPublic,MemClear});

  IF Buf=NIL THEN RETURN NIL; END;
  
  StPtr:=Buf;
  
  CopyString(StPtr^,init);
            
  SpecInfo:=AllocMem(TSIZE(StringInfo),MemReqSet{MemPublic,MemClear});
  
  IF SpecInfo=NIL THEN 
   FreeMem(Buf,BufSize); 
   RETURN NIL;  
  END; (* IF *)

 
b:=MakeBorder(w,h,3);

IF b=NIL THEN 
 FreeMem(Buf,BufSize); 
 FreeMem(SpecInfo,TSIZE(StringInfo));  
 RETURN NIL; 
END; (* IF b *)
   
gad:=AllocMem(TSIZE(Gadget),MemReqSet{MemPublic,MemClear});

IF gad=NIL THEN  
 FreeMem(Buf,BufSize); 
 FreeMem(SpecInfo,TSIZE(StringInfo));
 FreeBorder(b);
 RETURN NIL; 
END; (* IF b *)

 
      
  WITH SpecInfo^ DO
   Buffer:=Buf;
   UndoBuffer:=ADR(UnBuf);
   BufferPos:=0;
   MaxChars:=BufSize;
   DispPos:=0;
   AltKeyMap:=NIL;
  END; (* WITH SpecInfo *)



  WITH gad^ DO       
   NextGadget:=NIL; 
   LeftEdge:=left;
   TopEdge:=top;
   Width:=w;
   Height:=h;
   Flags:= GadgetFlagsSet{};
   Activation:=ActFlags;
   GadgetType:=StrGadget;
   GadgetRender:=b; (* border *)
   SelectRender:=NIL;
   GadgetText:=NIL; (* intuitext *)
   MutualExclude:=GadgetMutualExcludeSet{};
   SpecialInfo:=SpecInfo;
   GadgetID:=id;		(* our id number *)
   UserData:=NIL;
  END; (* WITH gad *)
  
  IF prev<>NIL THEN prev^.NextGadget:=gad; END;

  RETURN gad;

END MakeaString;
(* ============================================== *)

PROCEDURE MakeString(prev:GadgetPtr;init:ARRAY OF CHAR;
			id,left,top,w,h:CARDINAL):GadgetPtr;
BEGIN
RETURN MakeaString(prev,init,GadgetActivationSet{RelVerify},id,left,top,w,h);

END MakeString;
(* ============================================== *)

PROCEDURE MakeInt(prev:GadgetPtr;init:LONGINT;
			id,left,top,w,h:CARDINAL):GadgetPtr;
VAR
initS:ARRAY[0..50] OF CHAR;
i,j,l:CARDINAL;

BEGIN
ConvNumberToString(initS,init,TRUE,10,12,' ');

 (* remove leading spaces *)
l:=StringLength(initS);
i:=0;
WHILE initS[i]=' ' DO i:=i+1; END;
IF i<>0 THEN
 FOR j:=i TO l+1 DO
  initS[j-i]:=initS[j];
 END; (* FOR j *)
END; (* IF i *)

RETURN MakeaString(prev,initS,GadgetActivationSet{RelVerify,LongInt},
		   id,left,top,w,h);
END MakeInt;
(* ============================================== *)


PROCEDURE MakeProp(prev:GadgetPtr;id,left,top,w,h:CARDINAL;
		   flags:PropInfoFlagsSet;ActFlags:GadgetActivationSet;
		   horizpot,vertpot,vertbody,horizbody:CARDINAL):GadgetPtr;
VAR
 gad:GadgetPtr;
 SpecInfo2:PropInfoPtr;
 KnobImage:ImagePtr;		   
BEGIN
        
  KnobImage:=AllocMem(TSIZE(Image),MemReqSet{MemPublic,MemClear});
  IF KnobImage=NIL THEN RETURN NIL; END;
	
  SpecInfo2:=AllocMem(TSIZE(PropInfo),MemReqSet{MemPublic,MemClear});
  IF SpecInfo2=NIL THEN FreeMem(KnobImage,TSIZE(Image)); RETURN NIL; END;

gad:=AllocMem(TSIZE(Gadget),MemReqSet{MemPublic,MemClear});

IF gad=NIL THEN  
 FreeMem(KnobImage,TSIZE(Image));
 FreeMem(SpecInfo2,TSIZE(PropInfo));
 RETURN NIL; 
END; (* IF b *)

  INCL(flags,AutoKnob);
  INCL(ActFlags,RelVerify);
  
  WITH SpecInfo2^ DO  (* new *)
   Flags:=flags;
   HorizPot:=horizpot;
   VertPot:=vertpot;
   VertBody:=vertbody;
   HorizBody:=horizbody;
  END; (* WITH SpecInfo2 *)

  WITH gad^ DO        
   NextGadget:=NIL; 
   LeftEdge:=left;
   TopEdge:=top;
   Width:=w;
   Height:=h;
   Flags:= GadgetFlagsSet{};
   Activation:=ActFlags;
   GadgetType:=PropGadget;
   GadgetRender:=KnobImage;  (* Knob image *)
   SelectRender:=NIL;
   GadgetText:=NIL; (* intuitext *)
   MutualExclude:=GadgetMutualExcludeSet{};
   SpecialInfo:=SpecInfo2;
   GadgetID:=id;	(* our id number *)
   UserData:=NIL;
  END; (* WITH gad *)
  
  IF prev<>NIL THEN prev^.NextGadget:=gad; END;

  RETURN gad;

END MakeProp;
(* ============================================== *)

BEGIN


END OurGadgets.
