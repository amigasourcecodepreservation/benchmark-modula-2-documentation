MODULE Prop;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

(* Our Modules *)
FROM OurMenus IMPORT FillIntuiText,FreeIntuiText; 

(* Benchmark Modules *)
FROM Rasters IMPORT Jam1,Jam2; 
FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM LongInOut IMPORT WriteLongInt; 
FROM Intuition IMPORT NewWindow, Window,WindowPtr, WindowFlagsSet,WindowFlags,
                      IDCMPFlags, IDCMPFlagsSet,WBenchScreen,OpenWindow,
		      CloseWindow,ModifyIDCMP,IntuiMessagePtr,
		      GadgetPtr,Gadget,BoolGadget,GadgetFlagsSet, 
		      GadgetActivationSet,GadgetActivation,IntuiTextPtr,
		      GadgetMutualExcludeSet,Border,
		      StrGadget,StringInfo,ActivateGadget,StringInfoPtr,
		      PropInfo,PropGadget,KnobVMin,KnobHMin,PropInfoFlags,
		      PropInfoFlagsSet,PropInfoPtr,Image,MaxBody,MaxPot;(* new *)
		      
FROM AmigaDOSProcess IMPORT Delay;
FROM SYSTEM IMPORT BYTE,ADR;
FROM Tasks IMPORT Wait,SignalSet;
FROM Ports IMPORT GetMsg,ReplyMsg,MessagePtr;

VAR
 GadList:GadgetPtr;	
 gad4:Gadget;
 		
 KnobImage:Image; (* new *)
 SpecInfo2:PropInfo;  (* new *)
   
(* --------------------------------------------- *)

PROCEDURE InitGadgets():GadgetPtr;  

BEGIN
        
  WITH SpecInfo2 DO  (* new *)
   Flags:=PropInfoFlagsSet{AutoKnob,FreeHoriz};
   HorizPot:=0;
   VertPot:=0;
   VertBody:=KnobVMin;
   HorizBody:=MaxBody DIV 11; (* so moves 10% each click *)
  END; (* WITH SpecInfo2 *)

  WITH gad4 DO        
   NextGadget:=NIL; 
   LeftEdge:=10;
   TopEdge:=20;
   Width:=130;
   Height:=12;
   Flags:= GadgetFlagsSet{};
   Activation:=GadgetActivationSet{RelVerify};
   GadgetType:=PropGadget; (* new *)
   GadgetRender:=ADR(KnobImage);  (* Knob image *) (* new *)
   SelectRender:=NIL;
   GadgetText:=NIL; (* intuitext *)
   MutualExclude:=GadgetMutualExcludeSet{};
   SpecialInfo:=ADR(SpecInfo2);
   GadgetID:=4;		(* our id number *)
   UserData:=NIL;
  END; (* WITH gad4 *)

  
  RETURN ADR(gad4);


END  InitGadgets;
(* --------------------------------------------- *)
PROCEDURE FreeGadgets(VAR g:GadgetPtr); 
BEGIN

 
 WHILE g<>NIL DO 
  IF g^.GadgetText<>NIL THEN FreeIntuiText(g^.GadgetText); END;
  g:= g^.NextGadget;
 END; (* WHILE *)
 
 g:=NIL;
 

END FreeGadgets;
(* --------------------------------------------- *)

PROCEDURE MainLoop(WinPtr:WindowPtr);
VAR
 Quit:BOOLEAN;
 signal,inset: SignalSet;
 insig1: CARDINAL;
 MsgPtr: IntuiMessagePtr;
 class : IDCMPFlagsSet;
 WhichGadPtr:GadgetPtr; 
 SpecPtr2:PropInfoPtr; (* new *)
 
BEGIN (* of MainLoop *)
 Quit:=FALSE;
 					
 ModifyIDCMP(WinPtr^,IDCMPFlagsSet{Closewindow,GadgetUp});
  (* ask for closewindow messages and GadgetUp messages *)
    
  insig1:=CARDINAL(WinPtr^.UserPort^.mpSigBit); (* Window's message port *)
  inset:=SignalSet{};	(* clear the set *)
  INCL(inset,insig1);	(* ports to look for messages from *)

 REPEAT 

  signal:=Wait(inset); (* wait for messages. (Go to sleep) *)
  
  (* we will wake up when the program receives a message *)
  
  IF (insig1 IN signal) THEN (* this is an IDCMP message from the window *)

  REPEAT (* new *)
      
   MsgPtr:=GetMsg(WinPtr^.UserPort^); (* get the message *)
   
   IF MsgPtr<>NIL THEN  (* make sure the message exists *)
   
    class:=MsgPtr^.Class;	(* remember this because it goes away when
    				   we ReplyMsg. *)
				   
    WhichGadPtr :=GadgetPtr(MsgPtr^.IAddress); 
    
    ReplyMsg(MessagePtr(MsgPtr)); (* Reply to the message *)
      
    IF (class=IDCMPFlagsSet{Closewindow}) THEN (* check for a close message *)
     Quit:=TRUE;     (* set our flag in response to the message *)
    ELSIF (class=IDCMPFlagsSet{GadgetUp}) THEN (* check for a gadget *) 
    
     WriteString("got gadget number ");
     WriteCard(WhichGadPtr^.GadgetID,3);WriteLn;


     IF WhichGadPtr^.GadgetID=4 THEN (* new *)
      WriteString("ProPortional gad= ");
      SpecPtr2:=PropInfoPtr(WhichGadPtr^.SpecialInfo);
      IF SpecPtr2<>NIL THEN
       WriteCard(LONGCARD(SpecPtr2^.HorizPot)*100D DIV LONGCARD(MaxPot),6); (* % *)
       WriteLn;
      END; (* IF SpecPtr *)
     END; (* IF 4 *)
     
    END; (* IF *)
    
   END; (* IF MsgPtr *)

  UNTIL MsgPtr=NIL; (* new *)
         
  END; (* IF insig1 IN signal *)
  
 UNTIL Quit; (* repeat until the Quit flag is TRUE *)
 
END MainLoop;

(* --------------------------------------------- *)

PROCEDURE TryWindow;
VAR
 WinPtr:WindowPtr;
 NewWin:NewWindow;
 dum:BOOLEAN;  
 
BEGIN

GadList:=InitGadgets(); 

IF GadList<>NIL THEN  
 
    WITH NewWin DO
      LeftEdge    := 10;
      TopEdge     := 10;
      Width       := 500;
      Height      := 100;
      DetailPen   := BYTE(0);
      BlockPen    := BYTE(1);
      Title       := ADR("MyWindow");
      Flags:=  WindowFlagsSet{Activate,WindowClose,WindowSizing,WindowDrag,
      			 WindowDepth,NoCareRefresh};

      IDCMPFlags  := IDCMPFlagsSet{};
      Type        := WBenchScreen;
      FirstGadget := GadList;		 
      CheckMark   := NIL;
      Screen      := NIL;
      BitMap      := NIL;
      MinWidth    := 20;
      MinHeight   := 20; 
      MaxWidth    := 640;
      MaxHeight   := 400;

    END; (* WITH NewWin *)


    WinPtr :=  OpenWindow (NewWin);

    IF WinPtr = NIL THEN 
      WriteString("Cannot open Window");WriteLn;
      Delay(120); (* give time to read message if run from Workbench *)
    ELSE 


     MainLoop(WinPtr);	(* was Delay() before *)
     
     CloseWindow(WinPtr^);
     
    END; (* IF WinPtr *)
    
    
 FreeGadgets(GadList); 

ELSE 
 WriteString("Can't allocate gadgets");WriteLn;
 Delay(120); (* give time to read message if run from Workbench *)
END; (* IF GadList *)   

END TryWindow;

BEGIN (* of Module *)

TryWindow;

END Prop.

