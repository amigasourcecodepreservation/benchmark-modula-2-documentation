MODULE Prop2;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

(* Our Modules *)
FROM OurMenus IMPORT FillIntuiText,FreeIntuiText; 

(* Benchmark Modules *)
FROM Rasters IMPORT Jam1,Jam2; 
FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM LongInOut IMPORT WriteLongInt; 
FROM Intuition IMPORT NewWindow, Window,WindowPtr, WindowFlagsSet,WindowFlags,
                      IDCMPFlags, IDCMPFlagsSet,WBenchScreen,OpenWindow,
		      CloseWindow,ModifyIDCMP,IntuiMessagePtr,
		      GadgetPtr,Gadget,BoolGadget,GadgetFlagsSet, 
		      GadgetActivationSet,GadgetActivation,IntuiTextPtr,
		      GadgetMutualExcludeSet,Border,
		      StrGadget,StringInfo,ActivateGadget,StringInfoPtr,
		      PropInfo,PropGadget,KnobVMin,KnobHMin,PropInfoFlags,
		      PropInfoFlagsSet,PropInfoPtr,Image,MaxBody,MaxPot;
		      
FROM Drawing IMPORT Move; (* new *)
FROM Text IMPORT Text; (* new *)
FROM Conversions IMPORT ConvNumberToString; (* new *)

FROM AmigaDOSProcess IMPORT Delay;
FROM SYSTEM IMPORT BYTE,ADR;
FROM Tasks IMPORT Wait,SignalSet;
FROM Ports IMPORT GetMsg,ReplyMsg,MessagePtr;

VAR
 GadList:GadgetPtr;	
 gad4,gad5,gad6:Gadget;
 		
 KnobImage,KnobImage2,KnobImage3:Image; 
 SpecInfo2,SpecInfo3,SpecInfo4:PropInfo;  

   
(* --------------------------------------------- *)
PROCEDURE ShowValue(WinPtr:WindowPtr;Gad:CARDINAL;Value:CARDINAL); (* new *)
VAR
 text:ARRAY[0..3] OF CHAR; (* new *)
BEGIN  
 ConvNumberToString(text,LONGCARD(Value)*100D DIV LONGCARD(MaxPot),
 		FALSE,10,3,' ');
 Move(WinPtr^.RPort^,150,29+20*(Gad-4));
 Text(WinPtr^.RPort^,ADR(text),3);
END ShowValue;
(* --------------------------------------------- *)

PROCEDURE InitGadgets():GadgetPtr;  

BEGIN
        
  WITH SpecInfo2 DO  
   Flags:=PropInfoFlagsSet{AutoKnob,FreeHoriz};
   HorizPot:=0;
   VertPot:=0;
   VertBody:=KnobVMin;
   HorizBody:=MaxBody DIV 11; (* so moves 10% each click *)
  END; (* WITH SpecInfo2 *)
        
  WITH SpecInfo3 DO  (* new *)
   Flags:=PropInfoFlagsSet{AutoKnob,FreeHoriz};
   HorizPot:=0;
   VertPot:=0;
   VertBody:=KnobVMin;
   HorizBody:=MaxBody DIV 11; (* so moves 10% each click *)
  END; (* WITH SpecInfo3 *)
        
  WITH SpecInfo4 DO  (* new *)
   Flags:=PropInfoFlagsSet{AutoKnob,FreeHoriz};
   HorizPot:=0;
   VertPot:=0;
   VertBody:=KnobVMin;
   HorizBody:=MaxBody DIV 11; (* so moves 10% each click *)
  END; (* WITH SpecInfo4 *)

  WITH gad4 DO        
   NextGadget:=ADR(gad5); (* new *)
   LeftEdge:=10;
   TopEdge:=20;
   Width:=130;
   Height:=12;
   Flags:= GadgetFlagsSet{};
   Activation:=GadgetActivationSet{RelVerify,FollowMouse,GadgImmediate};
   GadgetType:=PropGadget; 
   GadgetRender:=ADR(KnobImage);  (* Knob image *) 
   SelectRender:=NIL;
   GadgetText:=NIL; (* intuitext *)
   MutualExclude:=GadgetMutualExcludeSet{};
   SpecialInfo:=ADR(SpecInfo2);
   GadgetID:=4;		(* our id number *)
   UserData:=NIL;
  END; (* WITH gad4 *)

  WITH gad5 DO        (* new *)
   NextGadget:=ADR(gad6); 
   LeftEdge:=10;
   TopEdge:=40;
   Width:=130;
   Height:=12;
   Flags:= GadgetFlagsSet{};
   Activation:=GadgetActivationSet{RelVerify,FollowMouse,GadgImmediate};
			 (* FollowMouse and GadgImmediate are new *)
   GadgetType:=PropGadget; 
   GadgetRender:=ADR(KnobImage2);  (* Knob image *) 
   SelectRender:=NIL;
   GadgetText:=NIL; (* intuitext *)
   MutualExclude:=GadgetMutualExcludeSet{};
   SpecialInfo:=ADR(SpecInfo3);
   GadgetID:=5;		(* our id number *)
   UserData:=NIL;
  END; (* WITH gad5 *)

  WITH gad6 DO    	(* new *)    
   NextGadget:=NIL; 
   LeftEdge:=10;
   TopEdge:=60;
   Width:=130;
   Height:=12;
   Flags:= GadgetFlagsSet{};
   Activation:=GadgetActivationSet{RelVerify,FollowMouse,GadgImmediate};
   GadgetType:=PropGadget; 
   GadgetRender:=ADR(KnobImage3);  (* Knob image *) 
   SelectRender:=NIL;
   GadgetText:=NIL; (* intuitext *)
   MutualExclude:=GadgetMutualExcludeSet{};
   SpecialInfo:=ADR(SpecInfo4);
   GadgetID:=6;		(* our id number *)
   UserData:=NIL;
  END; (* WITH gad6 *)

  
  RETURN ADR(gad4);


END  InitGadgets;
(* --------------------------------------------- *)
PROCEDURE FreeGadgets(VAR g:GadgetPtr); 
BEGIN

 
 WHILE g<>NIL DO 
  IF g^.GadgetText<>NIL THEN FreeIntuiText(g^.GadgetText); END;
  g:= g^.NextGadget;
 END; (* WHILE *)
 
 g:=NIL;
 

END FreeGadgets;
(* --------------------------------------------- *)

PROCEDURE MainLoop(WinPtr:WindowPtr);
VAR
 Quit:BOOLEAN;
 signal,inset: SignalSet;
 insig1: CARDINAL;
 MsgPtr: IntuiMessagePtr;
 class : IDCMPFlagsSet;
 WhichGadPtr:GadgetPtr; 
 SpecPtr2:PropInfoPtr; 
 lastgad,X:CARDINAL; (* new *)
 NormFlags,BusyFlags:IDCMPFlagsSet; (* new *)
 
BEGIN (* of MainLoop *)
 Quit:=FALSE;
 lastgad:=0; (* new *)
 
 NormFlags:=IDCMPFlagsSet{Closewindow,GadgetUp,GadgetDown,MouseMove}; (* new *)
 BusyFlags:=IDCMPFlagsSet{Closewindow,GadgetUp,GadgetDown}; (* new *)
  
  ModifyIDCMP(WinPtr^,NormFlags);
  (* ask for messages *)
    
  insig1:=CARDINAL(WinPtr^.UserPort^.mpSigBit); (* Window's message port *)
  inset:=SignalSet{};	(* clear the set *)
  INCL(inset,insig1);	(* ports to look for messages from *)

 REPEAT 

  signal:=Wait(inset); (* wait for messages. (Go to sleep) *)
  
  (* we will wake up when the program receives a message *)
  
  IF (insig1 IN signal) THEN (* this is an IDCMP message from the window *)

    ModifyIDCMP(WinPtr^,BusyFlags); (* new *)

  REPEAT 
      
   MsgPtr:=GetMsg(WinPtr^.UserPort^); (* get the message *)
   
   IF MsgPtr<>NIL THEN  (* make sure the message exists *)
   
    class:=MsgPtr^.Class;	(* remember this because it goes away when
    				   we ReplyMsg. *)
				   
    WhichGadPtr :=GadgetPtr(MsgPtr^.IAddress); 
    
      
    ReplyMsg(MessagePtr(MsgPtr)); (* Reply to the message *)

            
    IF (class=IDCMPFlagsSet{Closewindow}) THEN (* check for a close message *)
     Quit:=TRUE;     (* set our flag in response to the message *)
     
    ELSIF (class=IDCMPFlagsSet{GadgetDown}) THEN (* new *)
 
     lastgad:=WhichGadPtr^.GadgetID; (* remember what gadget was clicked on *)
     
    ELSIF (class=IDCMPFlagsSet{MouseMove}) THEN (* new *)

     IF (lastgad>=4)AND (lastgad<=6) THEN (* new *)
   
      CASE lastgad OF
       4:  X:=SpecInfo2.HorizPot; |
       5:  X:=SpecInfo3.HorizPot; | 
       6:  X:=SpecInfo4.HorizPot; | 
      ELSE
      END; (* CASE *)

	ShowValue(WinPtr,lastgad,X); (* new *)
     END; (* IF lastgad *)
    
    ELSIF (class=IDCMPFlagsSet{GadgetUp}) THEN (* check for a gadget *) 
    
     IF (WhichGadPtr^.GadgetID>=4)AND (WhichGadPtr^.GadgetID<=6) THEN (* new *)
      SpecPtr2:=PropInfoPtr(WhichGadPtr^.SpecialInfo);
      IF SpecPtr2<>NIL THEN
	ShowValue(WinPtr,WhichGadPtr^.GadgetID,SpecPtr2^.HorizPot); (* new *)
      END; (* IF SpecPtr *)
     END; (* IF WhichGadPtr *)
     
     lastgad:=0; (* new *)
      
    END; (* IF class *)

     
    
   END; (* IF MsgPtr *)

  UNTIL MsgPtr=NIL; 
  
     IF (lastgad>=4)AND (lastgad<=6) THEN (* new *)
   
      CASE lastgad OF
       4:  X:=SpecInfo2.HorizPot; |
       5:  X:=SpecInfo3.HorizPot; | 
       6:  X:=SpecInfo4.HorizPot; | 
      ELSE
      END; (* CASE *)

	ShowValue(WinPtr,lastgad,X); (* new *)
     END; (* IF lastgad *)
 
     ModifyIDCMP(WinPtr^,NormFlags); (* new *)
          
  END; (* IF insig1 IN signal *)
  
 UNTIL Quit; (* repeat until the Quit flag is TRUE *)
 
END MainLoop;

(* --------------------------------------------- *)

PROCEDURE TryWindow;
VAR
 WinPtr:WindowPtr;
 NewWin:NewWindow;
 dum:BOOLEAN;  
 
BEGIN

GadList:=InitGadgets(); 

IF GadList<>NIL THEN  
  
    WITH NewWin DO
      LeftEdge    := 10;
      TopEdge     := 10;
      Width       := 500;
      Height      := 100;
      DetailPen   := BYTE(0);
      BlockPen    := BYTE(1);
      Title       := ADR("MyWindow");
      Flags:=  WindowFlagsSet{Activate,WindowClose,WindowSizing,WindowDrag,
      			 WindowDepth,NoCareRefresh};

      IDCMPFlags  := IDCMPFlagsSet{};
      Type        := WBenchScreen;
      FirstGadget := GadList;		 
      CheckMark   := NIL;
      Screen      := NIL;
      BitMap      := NIL;
      MinWidth    := 20;
      MinHeight   := 20; 
      MaxWidth    := 640;
      MaxHeight   := 400;

    END; (* WITH NewWin *)


    WinPtr :=  OpenWindow (NewWin);

    IF WinPtr = NIL THEN 
      WriteString("Cannot open Window");WriteLn;
      Delay(120); (* give time to read message if run from Workbench *)
    ELSE 

    ShowValue(WinPtr,4,SpecInfo2.HorizPot); (* new *)
    ShowValue(WinPtr,5,SpecInfo3.HorizPot); (* new *)
    ShowValue(WinPtr,6,SpecInfo4.HorizPot); (* new *)

     MainLoop(WinPtr);
     
     CloseWindow(WinPtr^);
     
    END; (* IF WinPtr *)
    
    
 FreeGadgets(GadList); 

ELSE 
 WriteString("Can't allocate gadgets");WriteLn;
 Delay(120); (* give time to read message if run from Workbench *)
END; (* IF GadList *)   

END TryWindow;

BEGIN (* of Module *)

TryWindow;

END Prop2.

