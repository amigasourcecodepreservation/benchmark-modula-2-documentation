MODULE TryOurGad;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

(* Our Modules *)
FROM OurMenus IMPORT FillIntuiText,FreeIntuiText; 
FROM OurGadgets IMPORT MakeBool,FreeGadgets,MakeString,MakeInt,MakeProp;

(* Benchmark Modules *)
FROM Rasters IMPORT Jam1,Jam2; 
FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM LongInOut IMPORT WriteLongInt; 
FROM Intuition IMPORT NewWindow, Window,WindowPtr, WindowFlagsSet,WindowFlags,
                      IDCMPFlags, IDCMPFlagsSet,WBenchScreen,OpenWindow,
		      CloseWindow,ModifyIDCMP,IntuiMessagePtr,
		      GadgetPtr,Gadget,BoolGadget,GadgetFlagsSet, 
		      GadgetActivationSet,GadgetActivation,IntuiTextPtr,
		      GadgetMutualExcludeSet,Border,
		      StrGadget,StringInfo,ActivateGadget,StringInfoPtr,
		      PropInfoFlagsSet,PropInfoFlags,KnobVMin,MaxBody,MaxPot,
		      PropInfoPtr; 
		      
FROM AmigaDOSProcess IMPORT Delay;
FROM SYSTEM IMPORT BYTE,ADR;
FROM Tasks IMPORT Wait,SignalSet;
FROM Ports IMPORT GetMsg,ReplyMsg,MessagePtr;
FROM Conversions IMPORT ConvNumberToString;
FROM Drawing IMPORT Move;
FROM Text IMPORT Text;

VAR
 gad3,GadList:GadgetPtr;	
   
(* --------------------------------------------- *)
PROCEDURE ShowValue(WinPtr:WindowPtr;Gad:CARDINAL;Value:CARDINAL); 
VAR
 text:ARRAY[0..3] OF CHAR;
BEGIN  
 ConvNumberToString(text,LONGCARD(Value)*100D DIV LONGCARD(MaxPot),
 		FALSE,10,3,' ');
 Move(WinPtr^.RPort^,120,69+20*(Gad-6));
 Text(WinPtr^.RPort^,ADR(text),3);
END ShowValue;
   
(* --------------------------------------------- *)

PROCEDURE InitGadgets():GadgetPtr;  
VAR
 g,g2,g3:GadgetPtr;
BEGIN

g:=MakeBool(NIL,"OK",1,10,45);
IF g=NIL THEN RETURN NIL; END;

g2:=MakeBool(g,"Cancel",2,80,45);
IF g=NIL THEN FreeGadgets(g); RETURN NIL; END;

g3:=MakeString(g2,"",3,10,20,130,12);
IF g3=NIL THEN FreeGadgets(g); RETURN NIL; END;
gad3:=g3; (* remember for activation later *)

g2:=MakeInt(g3,0D,4,150,20,130,12);
IF g2=NIL THEN FreeGadgets(g); RETURN NIL; END;

g3:=MakeProp(g2,5,150,40,130,12,PropInfoFlagsSet{FreeHoriz},
		   GadgetActivationSet{},0,0,KnobVMin,MaxBody DIV 11);
IF g3=NIL THEN FreeGadgets(g); RETURN NIL; END;

g2:=MakeProp(g3,6,150,60,130,12,PropInfoFlagsSet{FreeHoriz},
		   GadgetActivationSet{FollowMouse,GadgImmediate},
		   0,0,KnobVMin,MaxBody DIV 11);
IF g2=NIL THEN FreeGadgets(g); RETURN NIL; END;

RETURN g;

END  InitGadgets;
(* --------------------------------------------- *)

PROCEDURE MainLoop(WinPtr:WindowPtr);
TYPE
 StringPtr=POINTER TO ARRAY[0..200] OF CHAR;
VAR
 Quit:BOOLEAN;
 signal,inset: SignalSet;
 insig1: CARDINAL;
 MsgPtr: IntuiMessagePtr;
 class : IDCMPFlagsSet;
 WhichGadPtr:GadgetPtr; 
 SpecPtr:StringInfoPtr; 
 SpecPtr2:PropInfoPtr;
 Sptr:StringPtr;
 NormFlags,BusyFlags:IDCMPFlagsSet; (* new *)
 lastgad:GadgetPtr;
 X:CARDINAL; (* new *)
 
BEGIN (* of MainLoop *)
 Quit:=FALSE;
 					
 lastgad:=NIL;
 
 NormFlags:=IDCMPFlagsSet{Closewindow,GadgetUp,GadgetDown,MouseMove}; 
 BusyFlags:=IDCMPFlagsSet{Closewindow,GadgetUp,GadgetDown};
  
  ModifyIDCMP(WinPtr^,NormFlags);
  (* ask for messages *)

  insig1:=CARDINAL(WinPtr^.UserPort^.mpSigBit); (* Window's message port *)
  inset:=SignalSet{};	(* clear the set *)
  INCL(inset,insig1);	(* ports to look for messages from *)

 REPEAT

  signal:=Wait(inset); (* wait for messages. (Go to sleep) *)
  
  (* we will wake up when the program receives a message *)
  
  IF (insig1 IN signal) THEN (* this is an IDCMP message from the window *)
  
    ModifyIDCMP(WinPtr^,BusyFlags);

  REPEAT 
  
   MsgPtr:=GetMsg(WinPtr^.UserPort^); (* get the message *)
   
   IF MsgPtr<>NIL THEN  (* make sure the message exists *)
   
    class:=MsgPtr^.Class;	(* remember this because it goes away when
    				   we ReplyMsg. *)
				   
    WhichGadPtr :=GadgetPtr(MsgPtr^.IAddress); 
    
    ReplyMsg(MessagePtr(MsgPtr)); (* Reply to the message *)
      
    IF (class=IDCMPFlagsSet{Closewindow}) THEN (* check for a close message *)
     Quit:=TRUE;     (* set our flag in response to the message *)
    ELSIF (class=IDCMPFlagsSet{GadgetUp}) THEN (* check for a gadget *) 
    
     IF WhichGadPtr^.GadgetID<>6 THEN
      WriteString("got gadget number ");
      WriteCard(WhichGadPtr^.GadgetID,3);WriteLn;
     END; (* IF *)
     lastgad:=NIL;
     
     IF WhichGadPtr^.GadgetID=3 THEN
      SpecPtr:=StringInfoPtr(WhichGadPtr^.SpecialInfo);
      IF SpecPtr<>NIL THEN
       Sptr:=StringPtr(SpecPtr^.Buffer);
       IF Sptr<>NIL THEN
        WriteString("string gad= ");
        WriteString(Sptr^);WriteLn;
       END; (* IF Sptr *)
      END; (* IF SpecPtr *)
     END; (* IF 3 *)

     IF WhichGadPtr^.GadgetID=4 THEN 
      WriteString("Integer gad= ");
      SpecPtr:=StringInfoPtr(WhichGadPtr^.SpecialInfo);
      IF SpecPtr<>NIL THEN
       WriteLongInt(SpecPtr^.LongInt,6);
       WriteLn;
      END; (* IF SpecPtr *)
     END; (* IF 4 *)

     IF WhichGadPtr^.GadgetID=5 THEN 
      WriteString("ProPortional gad= ");
      SpecPtr2:=PropInfoPtr(WhichGadPtr^.SpecialInfo);
      IF SpecPtr2<>NIL THEN
       WriteCard(LONGCARD(SpecPtr2^.HorizPot)*100D DIV LONGCARD(MaxPot),6);(*%*)
       WriteLn;
      END; (* IF SpecPtr *)
     END; (* IF 5 *)

     IF WhichGadPtr^.GadgetID=6 THEN 
     
      SpecPtr2:=PropInfoPtr(WhichGadPtr^.SpecialInfo);
      IF SpecPtr2<>NIL THEN
       X:=SpecPtr2^.HorizPot;
        
       ShowValue(WinPtr,WhichGadPtr^.GadgetID,X);
      END; (* IF SpecPtr2 *)
      
     END; (* IF 6 *)
     
    ELSIF (class=IDCMPFlagsSet{GadgetDown}) THEN (* new *)
 
     lastgad:=WhichGadPtr; (* remember what gadget was clicked on *)
     
    ELSIF (class=IDCMPFlagsSet{MouseMove}) THEN

     IF (lastgad<>NIL) THEN 
      IF lastgad^.GadgetID=6 THEN
       SpecPtr2:=PropInfoPtr(lastgad^.SpecialInfo);
       IF SpecPtr2<>NIL THEN
        X:=SpecPtr2^.HorizPot;
         
        ShowValue(WinPtr,lastgad^.GadgetID,X); 
      END; (* IF =6 *)
     END; (* IF lastgad *)
      
     END; (* IF lastgad *)
     
   END; (* IF class *)
     
   END; (* IF MsgPtr *)

  UNTIL MsgPtr=NIL; 

   ModifyIDCMP(WinPtr^,NormFlags);
 
  END; (* IF insig1 IN signal *)
  
 UNTIL Quit; (* repeat until the Quit flag is TRUE *)
 
END MainLoop;

(* --------------------------------------------- *)

PROCEDURE TryWindow;
VAR
 WinPtr:WindowPtr;
 NewWin:NewWindow;
 dum:BOOLEAN;  
 
BEGIN

GadList:=InitGadgets(); 

IF GadList<>NIL THEN  
 
    WITH NewWin DO
      LeftEdge    := 10;
      TopEdge     := 10;
      Width       := 500;
      Height      := 100;
      DetailPen   := BYTE(0);
      BlockPen    := BYTE(1);
      Title       := ADR("MyWindow");
      Flags:=  WindowFlagsSet{Activate,WindowClose,WindowSizing,WindowDrag,
      			 WindowDepth,NoCareRefresh};

      IDCMPFlags  := IDCMPFlagsSet{};
      Type        := WBenchScreen;
      FirstGadget := GadList;		 
      CheckMark   := NIL;
      Screen      := NIL;
      BitMap      := NIL;
      MinWidth    := 20;
      MinHeight   := 20; 
      MaxWidth    := 640;
      MaxHeight   := 400;

    END; (* WITH NewWin *)


    WinPtr :=  OpenWindow (NewWin);

    IF WinPtr = NIL THEN 
      WriteString("Cannot open Window");WriteLn;
      Delay(120); (* give time to read message if run from Workbench *)
    ELSE 

     dum:=ActivateGadget(gad3^,WinPtr^,NIL); 

     ShowValue(WinPtr,6,0);

     MainLoop(WinPtr);	(* was Delay() before *)
     
     CloseWindow(WinPtr^);
     
    END; (* IF WinPtr *)
    
    
 FreeGadgets(GadList); 

ELSE 
 WriteString("Can't allocate gadgets");WriteLn;
 Delay(120); (* give time to read message if run from Workbench *)
END; (* IF GadList *)   

END TryWindow;

BEGIN (* of Module *)

TryWindow;

END TryOurGad.

