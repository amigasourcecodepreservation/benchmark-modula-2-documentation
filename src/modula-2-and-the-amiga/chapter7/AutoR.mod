MODULE AutoR;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM Intuition IMPORT NewWindow, Window,WindowPtr, WindowFlagsSet,WindowFlags,
                      IDCMPFlags, IDCMPFlagsSet,WBenchScreen,OpenWindow,
		      CloseWindow,ModifyIDCMP,IntuiMessagePtr,IntuiText,
		      AutoRequest;
FROM AmigaDOSProcess IMPORT Delay;
FROM SYSTEM IMPORT BYTE,ADR;
FROM Tasks IMPORT Wait,SignalSet;
FROM Ports IMPORT GetMsg,ReplyMsg,MessagePtr;
FROM Strings IMPORT StringLength;
FROM Rasters  IMPORT Jam2, Jam1, DrawModeSet;

(* ================================ *)

PROCEDURE Confirm(WinPtr:WindowPtr; MainText:ARRAY OF CHAR; 
		   PosText:ARRAY OF CHAR;
                   NegText:ARRAY OF CHAR):BOOLEAN;
VAR
BodyText,PositiveText,NegativeText:IntuiText;
Width: INTEGER;

BEGIN
    Width:=INTEGER(StringLength(MainText))*8+35; 
    
    WITH BodyText DO
      FrontPen := BYTE (2); BackPen := BYTE (1);
      DrawMode := Jam2;
      LeftEdge := 5; TopEdge := 5;
      ITextFont := NIL; NextText := NIL;
      IText := ADR(MainText); 
    END;
    
    PositiveText:= BodyText; (* copy it *)
    WITH PositiveText DO (* modify it *)
      LeftEdge :=6;  TopEdge := 3;
      NextText := NIL; IText := ADR (PosText);
    END;
    
    NegativeText := BodyText; (* copy it *)
    WITH NegativeText DO  (* modify it *)
      LeftEdge := 6;  TopEdge :=3;
      NextText := NIL; IText := ADR (NegText);
    END;
    
    IF Width<25 THEN Width:=40; END; (* make sure gadgets are visible *)
    
    RETURN( AutoRequest(WinPtr^,ADR(BodyText),ADR(PositiveText), (* do it *)
                        ADR(NegativeText),IDCMPFlagsSet{},
                        IDCMPFlagsSet{},Width,50) );

END Confirm;


(* --------------------------------------------- *)

PROCEDURE MainLoop(WinPtr:WindowPtr);
VAR
 Quit:BOOLEAN;
 signal,inset: SignalSet;
 insig1: CARDINAL;
 MsgPtr: IntuiMessagePtr;
 class : IDCMPFlagsSet;

 
BEGIN (* of MainLoop *)
 Quit:=FALSE;
 
 ModifyIDCMP(WinPtr^,IDCMPFlagsSet{Closewindow});
  (* ask for closewindow messages *)
    
  insig1:=CARDINAL(WinPtr^.UserPort^.mpSigBit); (* Window's message port *)
  inset:=SignalSet{};	(* clear the set *)
  INCL(inset,insig1);	(* ports to look for messages from *)

 REPEAT

  signal:=Wait(inset); (* wait for messages. (Go to sleep) *)
  
  (* we will wake up when the program receives a message *)
  
  IF (insig1 IN signal) THEN (* this is an IDCMP message from the window *)
  
  REPEAT

   MsgPtr:=GetMsg(WinPtr^.UserPort^); (* get the message *)
   
   IF MsgPtr<>NIL THEN  (* make sure the message exists *)
   
    class:=MsgPtr^.Class;	(* remember this because it goes away when
    				   we ReplyMsg. *)
    
    ReplyMsg(MessagePtr(MsgPtr)); (* Reply to the message *)
      
    IF (class=IDCMPFlagsSet{Closewindow}) THEN (* check for a close message *)
     IF Confirm(WinPtr,"Really Quit?","Yes","No") THEN Quit:=TRUE; END;     (* set our flag in response to the message *)
    END; (* IF *)
    
   END; (* IF MsgPtr *)
  
  UNTIL MsgPtr=NIL;
   
  END; (* IF insig1 IN signal *)
  
 UNTIL Quit; (* repeat until the Quit flag is TRUE *)
 
END MainLoop;

(* --------------------------------------------- *)

PROCEDURE TryWindow;
VAR
 WinPtr:WindowPtr;
 NewWin:NewWindow;
BEGIN

 
    WITH NewWin DO
      LeftEdge    := 10;
      TopEdge     := 10;
      Width       := 500;
      Height      := 100;
      DetailPen   := BYTE(0);
      BlockPen    := BYTE(1);
      Title       := ADR("MyWindow");
      Flags:=  WindowFlagsSet{Activate,WindowClose,WindowSizing,WindowDrag,
      			 WindowDepth,NoCareRefresh};

      IDCMPFlags  := IDCMPFlagsSet{};
      Type        := WBenchScreen;
      FirstGadget := NIL;
      CheckMark   := NIL;
      Screen      := NIL;
      BitMap      := NIL;
      MinWidth    := 20;
      MinHeight   := 20; 
      MaxWidth    := 640;
      MaxHeight   := 400;

    END; (* WITH NewWin *)


    WinPtr :=  OpenWindow (NewWin);

    IF WinPtr = NIL THEN 
      WriteString("Cannot open Window");WriteLn;
      Delay(120); (* give time to read message if run from Workbench *)
    ELSE 


     MainLoop(WinPtr);	(* was Delay() before *)
     
     CloseWindow(WinPtr^);
     
    END; (* IF WinPtr *)

  

END TryWindow;

BEGIN (* of Module *)

TryWindow;

END AutoR.

