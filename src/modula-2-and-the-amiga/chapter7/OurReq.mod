IMPLEMENTATION MODULE OurReq;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

(* Our Modules *)
FROM OurGadgets IMPORT MakeBool,FreeGadgets,MakeString,MakeInt,MakeProp;

(* Benchmark Modules *)
FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM LongInOut IMPORT WriteLongInt; 
FROM Intuition IMPORT NewWindow, Window,WindowPtr, WindowFlagsSet,WindowFlags,
                      IDCMPFlags, IDCMPFlagsSet,WBenchScreen,CustomScreen,
		      OpenWindow,CloseWindow,ModifyIDCMP,IntuiMessagePtr,
		      GadgetPtr,Gadget,BoolGadget,GadgetFlagsSet, 
		      GadgetActivationSet,GadgetActivation,IntuiTextPtr,
		      GadgetMutualExcludeSet,Border,IntuiText,
		      StrGadget,StringInfo,ActivateGadget,StringInfoPtr,
		      PropInfoFlagsSet,PropInfoFlags,KnobVMin,MaxBody,MaxPot,
		      PropInfoPtr,AutoRequest;
		      
FROM AmigaDOSProcess IMPORT Delay;
FROM SYSTEM IMPORT BYTE,ADR;
FROM Tasks IMPORT Wait,SignalSet;
FROM Ports IMPORT GetMsg,ReplyMsg,MessagePtr;
FROM Conversions IMPORT ConvNumberToString;
FROM Drawing IMPORT Move,SetAPen,SetBPen,SetDrMd;
FROM Text IMPORT Text;
FROM Strings IMPORT StringLength,CopyString;
FROM Rasters  IMPORT Jam2, Jam1, DrawModeSet;

(* ================================ *)

PROCEDURE Confirm(WinPtr:WindowPtr; MainText:ARRAY OF CHAR; 
		   PosText:ARRAY OF CHAR;
                   NegText:ARRAY OF CHAR):BOOLEAN;
VAR
BodyText,PositiveText,NegativeText:IntuiText;
Width: INTEGER;

BEGIN
    Width:=INTEGER(StringLength(MainText))*8+35; 
    
    WITH BodyText DO
      FrontPen := BYTE (2); BackPen := BYTE (1);
      DrawMode := Jam2;
      LeftEdge := 5; TopEdge := 5;
      ITextFont := NIL; NextText := NIL;
      IText := ADR(MainText); 
    END;
    
    PositiveText:= BodyText; (* copy it *)
    WITH PositiveText DO (* modify it *)
      LeftEdge :=6;  TopEdge := 3;
      NextText := NIL; IText := ADR (PosText);
    END;
    
    NegativeText := BodyText; (* copy it *)
    WITH NegativeText DO  (* modify it *)
      LeftEdge := 6;  TopEdge :=3;
      NextText := NIL; IText := ADR (NegText);
    END;
    
    IF Width<25 THEN Width:=40; END; (* make sure gadgets are visible *)
    
    RETURN( AutoRequest(WinPtr^,ADR(BodyText),ADR(PositiveText), (* do it *)
                        ADR(NegativeText),IDCMPFlagsSet{},
                        IDCMPFlagsSet{},Width,50) );

END Confirm;
   
(* --------------------------------------------- *)
PROCEDURE Doit(OWinPtr:WindowPtr;MainText:ARRAY OF CHAR;int:BOOLEAN;
		VAR StringAns:ARRAY OF CHAR;VAR LongAns:LONGINT):BOOLEAN;
VAR
 GadList,gad3:GadgetPtr;
 WinPtr:WindowPtr;
 NewWin:NewWindow;
 OK,dum:BOOLEAN;  
 
(* --------------------------------------------- *)
PROCEDURE InitGadgets(InitString:ARRAY OF CHAR; InitLong:LONGINT;
				int:BOOLEAN):GadgetPtr;  
VAR
 g,g2,g3:GadgetPtr;
BEGIN (* IF InitGadgets *)

g:=MakeBool(NIL,"OK",1,50,55);
IF g=NIL THEN RETURN NIL; END;

g2:=MakeBool(g,"Cancel",2,130,55);
IF g=NIL THEN FreeGadgets(g); RETURN NIL; END;

IF NOT int THEN
 g3:=MakeString(g2,InitString,3,50,35,130,12);
ELSE
 g3:=MakeInt(g2,InitLong,3,50,35,130,12);
END; (* IF *)

IF g3=NIL THEN FreeGadgets(g); RETURN NIL; END;

gad3:=g3; (* remember for activation later *)

RETURN g;

END  InitGadgets;
(* --------------------------------------------- *)

PROCEDURE MainLoop(WinPtr:WindowPtr):BOOLEAN;
TYPE
 StringPtr=POINTER TO ARRAY[0..200] OF CHAR;
VAR
 Quit:BOOLEAN;
 signal,inset: SignalSet;
 insig1: CARDINAL;
 MsgPtr: IntuiMessagePtr;
 class : IDCMPFlagsSet;
 WhichGadPtr:GadgetPtr; 
 SpecPtr:StringInfoPtr; 
 SpecPtr2:PropInfoPtr;
 Sptr:StringPtr;
 NormFlags,BusyFlags:IDCMPFlagsSet; 
 lastgad:GadgetPtr;
 X:CARDINAL; 
 ans:BOOLEAN;
  
BEGIN (* of MainLoop *)
 Quit:=FALSE;
 ans:=FALSE;
 lastgad:=NIL;
 
 NormFlags:=IDCMPFlagsSet{Closewindow,GadgetUp}; 
 BusyFlags:=IDCMPFlagsSet{Closewindow,GadgetUp};
  
  ModifyIDCMP(WinPtr^,NormFlags);
  (* ask for messages *)

  insig1:=CARDINAL(WinPtr^.UserPort^.mpSigBit); (* Window's message port *)
  inset:=SignalSet{};	(* clear the set *)
  INCL(inset,insig1);	(* ports to look for messages from *)

 REPEAT

  signal:=Wait(inset); (* wait for messages. (Go to sleep) *)
  
  (* we will wake up when the program receives a message *)
  
  IF (insig1 IN signal) THEN (* this is an IDCMP message from the window *)
  
    ModifyIDCMP(WinPtr^,BusyFlags);

  REPEAT 
  
   MsgPtr:=GetMsg(WinPtr^.UserPort^); (* get the message *)
   
   IF MsgPtr<>NIL THEN  (* make sure the message exists *)
   
    class:=MsgPtr^.Class;	(* remember this because it goes away when
    				   we ReplyMsg. *)
				   
    WhichGadPtr :=GadgetPtr(MsgPtr^.IAddress); 
    
    ReplyMsg(MessagePtr(MsgPtr)); (* Reply to the message *)
      
    IF (class=IDCMPFlagsSet{Closewindow}) THEN (* check for a close message *)
     ans:=FALSE; (* same as Cancel *)
     Quit:=TRUE;     (* set our flag in response to the message *)

    ELSIF (class=IDCMPFlagsSet{GadgetUp}) THEN (* check for a gadget *) 
    
     IF WhichGadPtr^.GadgetID=1 THEN (* OK *)
      SpecPtr:=StringInfoPtr(gad3^.SpecialInfo);
      IF SpecPtr<>NIL THEN
      
       IF NOT int THEN
        Sptr:=StringPtr(SpecPtr^.Buffer);
        IF Sptr<>NIL THEN CopyString(StringAns,Sptr^); END;(* Copy to answer *)
       ELSE
        LongAns:=SpecPtr^.LongInt; (* copy to answer *)
       END; (* IF int *)
      
      END; (* IF SPecPtr *)

      ans:=TRUE;
      Quit:=TRUE;
     END; (* IF 1 *)
     
     IF WhichGadPtr^.GadgetID=2 THEN (* Cancel *)
      ans:=FALSE;
      Quit:=TRUE;
     END; (* IF 2 *)

     
     IF WhichGadPtr^.GadgetID=3 THEN (* String or Integer gadget *)
     
     IF NOT int THEN (* was String Gadget *)
     
      SpecPtr:=StringInfoPtr(WhichGadPtr^.SpecialInfo);
      IF SpecPtr<>NIL THEN
       Sptr:=StringPtr(SpecPtr^.Buffer);
       IF Sptr<>NIL THEN
        CopyString(StringAns,Sptr^); (* Copy to answer *)
        Quit:=TRUE;
        ans:=TRUE; (* same as OK *)
       END; (* IF Sptr *)
      END; (* IF SpecPtr *)
      
     ELSE (* was Integer Gadget *)

      SpecPtr:=StringInfoPtr(WhichGadPtr^.SpecialInfo);
      IF SpecPtr<>NIL THEN
       LongAns:=SpecPtr^.LongInt; (* copy to answer *)
       Quit:=TRUE;
       ans:=TRUE; (* same as OK *)
      END; (* IF SpecPtr *)
      
     END; (* IF int *)

     END; (* IF 3 *)

     
   END; (* IF class *)
     
   END; (* IF MsgPtr *)

  UNTIL MsgPtr=NIL; 

   ModifyIDCMP(WinPtr^,NormFlags);
 
  END; (* IF insig1 IN signal *)
  
 UNTIL Quit; (* repeat until the Quit flag is TRUE *)
 
 RETURN ans;

END MainLoop;

(* --------------------------------------------- *)

BEGIN (* of Doit *)
 

GadList:=InitGadgets(StringAns,LongAns,int); 
OK:=FALSE;

IF GadList<>NIL THEN  
 
    WITH NewWin DO
      LeftEdge    := 10;
      TopEdge     := 10;
      Width       := 230;
      Height      := 100;
      DetailPen   := BYTE(0);
      BlockPen    := BYTE(1);
      Title       := ADR("Enter your answer");
      Flags:=  WindowFlagsSet{Activate,WindowClose,WindowDrag,
      			 WindowDepth,NoCareRefresh};

      IDCMPFlags  := IDCMPFlagsSet{};
      Type        := CustomScreen;
      FirstGadget := GadList;		 
      CheckMark   := NIL;
      Screen      := OWinPtr^.WScreen; (* open on the same screen as your window *)
      BitMap      := NIL;
      MinWidth    := 20;
      MinHeight   := 20; 
      MaxWidth    := 640;
      MaxHeight   := 400;

    END; (* WITH NewWin *)


    WinPtr :=  OpenWindow (NewWin);

    IF WinPtr = NIL THEN 
      WriteString("Cannot open Window");WriteLn;
      Delay(120); (* give time to read message if run from Workbench *)
      RETURN FALSE;
    ELSE 

     Move(WinPtr^.RPort^,20,20);
     SetAPen(WinPtr^.RPort^,1);
     SetBPen(WinPtr^.RPort^,0);
     SetDrMd(WinPtr^.RPort^,Jam2);
     Text(WinPtr^.RPort^,ADR(MainText),StringLength(MainText));
     	 (* dislay the message in the window *)


     dum:=ActivateGadget(gad3^,WinPtr^,NIL); 


     OK:=MainLoop(WinPtr);
     
     CloseWindow(WinPtr^);
     
    END; (* IF WinPtr *)
    
    
 FreeGadgets(GadList); 
 
 RETURN OK;
 
ELSE 
 WriteString("Can't allocate gadgets");WriteLn;
 Delay(120); (* give time to read message if run from Workbench *)
 RETURN FALSE;
END; (* IF GadList *)   

END Doit;
(* --------------------------------------------- *)

PROCEDURE GetString(WinPtr:WindowPtr;MainText:ARRAY OF CHAR;
		    VAR Answer:ARRAY OF CHAR):BOOLEAN;
(* Returns TRUE if click on 'OK', FALSE if on 'Cancel' *)
VAR
 dum:LONGINT;
BEGIN
 RETURN Doit(WinPtr,MainText,FALSE,Answer,dum);
END GetString;

(* --------------------------------------------- *)

PROCEDURE GetInt(WinPtr:WindowPtr;MainText:ARRAY OF CHAR;
		    VAR Answer:LONGINT):BOOLEAN;
(* Returns TRUE if click on 'OK', FALSE if on 'Cancel' *)
VAR
 dum:ARRAY[0..20] OF CHAR;
BEGIN
 RETURN Doit(WinPtr,MainText,TRUE,dum,Answer);
END GetInt;
(* --------------------------------------------- *)

BEGIN (* of Module *)


END OurReq.

