MODULE TryOurReq;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

(* Our Modules *)
FROM OurReq IMPORT Confirm,GetString,GetInt;

(* Benchmark Modules *)
FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM Intuition IMPORT NewWindow, Window,WindowPtr, WindowFlagsSet,WindowFlags,
                      IDCMPFlags, IDCMPFlagsSet,WBenchScreen,OpenWindow,
		      CloseWindow,ModifyIDCMP,IntuiMessagePtr,IntuiText,
		      AutoRequest;
FROM AmigaDOSProcess IMPORT Delay;
FROM SYSTEM IMPORT BYTE,ADR;
FROM Tasks IMPORT Wait,SignalSet;
FROM Ports IMPORT GetMsg,ReplyMsg,MessagePtr;
FROM Strings IMPORT StringLength;
FROM Rasters  IMPORT Jam2, Jam1, DrawModeSet;
FROM LongInOut IMPORT WriteLongInt;

  
(* --------------------------------------------- *)

PROCEDURE MainLoop(WinPtr:WindowPtr);
VAR
 Quit:BOOLEAN;
 signal,inset: SignalSet;
 insig1: CARDINAL;
 MsgPtr: IntuiMessagePtr;
 class : IDCMPFlagsSet;

 
BEGIN (* of MainLoop *)
 Quit:=FALSE;
 
 ModifyIDCMP(WinPtr^,IDCMPFlagsSet{Closewindow});
  (* ask for closewindow messages *)
    
  insig1:=CARDINAL(WinPtr^.UserPort^.mpSigBit); (* Window's message port *)
  inset:=SignalSet{};	(* clear the set *)
  INCL(inset,insig1);	(* ports to look for messages from *)

 REPEAT

  signal:=Wait(inset); (* wait for messages. (Go to sleep) *)
  
  (* we will wake up when the program receives a message *)
  
  IF (insig1 IN signal) THEN (* this is an IDCMP message from the window *)
  
  REPEAT

   MsgPtr:=GetMsg(WinPtr^.UserPort^); (* get the message *)
   
   IF MsgPtr<>NIL THEN  (* make sure the message exists *)
   
    class:=MsgPtr^.Class;	(* remember this because it goes away when
    				   we ReplyMsg. *)
    
    ReplyMsg(MessagePtr(MsgPtr)); (* Reply to the message *)
      
    IF (class=IDCMPFlagsSet{Closewindow}) THEN (* check for a close message *)
     IF Confirm(WinPtr,"Really Quit?","Yes","No") THEN Quit:=TRUE; END;     (* set our flag in response to the message *)
    END; (* IF *)
    
   END; (* IF MsgPtr *)
  
  UNTIL MsgPtr=NIL;
   
  END; (* IF insig1 IN signal *)
  
 UNTIL Quit; (* repeat until the Quit flag is TRUE *)
 
END MainLoop;

(* --------------------------------------------- *)

PROCEDURE TryWindow;
VAR
 WinPtr:WindowPtr;
 NewWin:NewWindow;
 Answer:ARRAY[0..80] OF CHAR;
 Answer2:LONGINT;
 
BEGIN

 
    WITH NewWin DO
      LeftEdge    := 10;
      TopEdge     := 10;
      Width       := 500;
      Height      := 100;
      DetailPen   := BYTE(0);
      BlockPen    := BYTE(1);
      Title       := ADR("MyWindow");
      Flags:=  WindowFlagsSet{Activate,WindowClose,WindowSizing,WindowDrag,
      			 WindowDepth,NoCareRefresh};

      IDCMPFlags  := IDCMPFlagsSet{};
      Type        := WBenchScreen;
      FirstGadget := NIL;
      CheckMark   := NIL;
      Screen      := NIL;
      BitMap      := NIL;
      MinWidth    := 20;
      MinHeight   := 20; 
      MaxWidth    := 640;
      MaxHeight   := 400;

    END; (* WITH NewWin *)


    WinPtr :=  OpenWindow (NewWin);

    IF WinPtr = NIL THEN 
      WriteString("Cannot open Window");WriteLn;
      Delay(120); (* give time to read message if run from Workbench *)
    ELSE 


     Answer:="DefaultFileName";
  
  
     IF GetString(WinPtr,"Enter file name",Answer) THEN
      WriteString("ans=");WriteString(Answer);WriteLn;
     ELSE
      WriteString("Cancelled");WriteLn;
      WriteString("ans=");WriteString(Answer);WriteLn;
     END;
		    

     Answer2:=12D;
  

     IF GetInt(WinPtr,"Enter Number",Answer2) THEN
      WriteString("ans=");WriteLongInt(Answer2,7);WriteLn;
     ELSE
      WriteString("Cancelled");WriteLn;
      WriteString("ans=");WriteLongInt(Answer2,7);WriteLn;
     END;
		    
     
     MainLoop(WinPtr);	(* was Delay() before *)
     
     CloseWindow(WinPtr^);
     
    END; (* IF WinPtr *)

  

END TryWindow;

BEGIN (* of Module *)

TryWindow;

END TryOurReq.

