MODULE GraphDemo;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

FROM InOut IMPORT WriteString,WriteLn,WriteCard;
FROM Intuition IMPORT NewWindow, Window,WindowPtr, WindowFlagsSet,WindowFlags,
                      IDCMPFlags, IDCMPFlagsSet,WBenchScreen,OpenWindow,
		      CloseWindow,ModifyIDCMP,IntuiMessagePtr;
FROM AmigaDOSProcess IMPORT Delay;
FROM SYSTEM IMPORT BYTE,ADR;
FROM Tasks IMPORT Wait,SignalSet;
FROM Ports IMPORT GetMsg,ReplyMsg,MessagePtr;
FROM Drawing IMPORT SetDrMd,SetAPen,SetBPen,Move,Draw,WritePixel,RectFill,
		    ClearScreen;
FROM Rasters  IMPORT Jam2, Jam1, DrawModeSet,Complement;
FROM Text IMPORT Text;

(* --------------------------------------------- *)

PROCEDURE MainLoop(WinPtr:WindowPtr);
VAR
 Quit:BOOLEAN;
 signal,inset: SignalSet;
 insig1: CARDINAL;
 MsgPtr: IntuiMessagePtr;
 class : IDCMPFlagsSet;

 
BEGIN (* of MainLoop *)
 Quit:=FALSE;
 
 ModifyIDCMP(WinPtr^,IDCMPFlagsSet{Closewindow});
  (* ask for closewindow messages *)
    
  insig1:=CARDINAL(WinPtr^.UserPort^.mpSigBit); (* Window's message port *)
  inset:=SignalSet{};	(* clear the set *)
  INCL(inset,insig1);	(* ports to look for messages from *)

 REPEAT

  signal:=Wait(inset); (* wait for messages. (Go to sleep) *)
  
  (* we will wake up when the program receives a message *)
  
  IF (insig1 IN signal) THEN (* this is an IDCMP message from the window *)
  
   REPEAT
   
   MsgPtr:=GetMsg(WinPtr^.UserPort^); (* get the message *)
   
   
   IF MsgPtr<>NIL THEN  (* make sure the message exists *)
   
    class:=MsgPtr^.Class;	(* remember this because it goes away when
    				   we ReplyMsg. *)
    
    ReplyMsg(MessagePtr(MsgPtr)); (* Reply to the message *)
      
    IF (class=IDCMPFlagsSet{Closewindow}) THEN (* check for a close message *)
     Quit:=TRUE;     (* set our flag in response to the message *)
    END; (* IF *)
    
   END; (* IF MsgPtr *)
  
  UNTIL MsgPtr=NIL;
   
  END; (* IF insig1 IN signal *)
  
 UNTIL Quit; (* repeat until the Quit flag is TRUE *)
 
END MainLoop;

(* --------------------------------------------- *)
PROCEDURE cls(WinPtr:WindowPtr); (* clears a window without zapping borders *)
VAR
 x,y,w,h:INTEGER;
BEGIN

IF WinPtr<>NIL THEN
 x:=INTEGER(WinPtr^.BorderLeft);
 y:=INTEGER(WinPtr^.BorderTop);
 w:=WinPtr^.Width-INTEGER(WinPtr^.BorderRight);
 h:=WinPtr^.Height-INTEGER(WinPtr^.BorderBottom);
 
 IF (w>x) AND (h>y) THEN 	    (*  make sure RectFill is safe *)
  SetDrMd(WinPtr^.RPort^,Jam1);     (* draw PenA *)
  SetAPen(WinPtr^.RPort^,0); 	    (* set the color to background *)
  RectFill(WinPtr^.RPort^,x,y,w,h); (* Clear the window *)
 END; (* IF w and h *)
END; (* IF WinPtr *)
 
END cls;
(* --------------------------------------------- *)

PROCEDURE DoDemo(WinPtr:WindowPtr);
VAR
 i:CARDINAL;
 dum:INTEGER;
BEGIN

SetDrMd(WinPtr^.RPort^,Jam1); (* set drawing mode. Put 1 color in raster *)
SetAPen(WinPtr^.RPort^,1);    (* set the color *)

FOR i:=1 TO 15 DO
 dum:=WritePixel(WinPtr^.RPort^,4*i+100,4*i+20); (* put up some pixels *)
END; (* FOR i *)

Delay(120); 		      (* pause *)

cls(WinPtr);		      (* Clear the window *)

Delay(120); 		      (* pause *)

SetDrMd(WinPtr^.RPort^,Jam1); (* set drawing mode. Put 1 color in raster *)
SetAPen(WinPtr^.RPort^,1);    (* set the color *)

FOR i:=1 TO 10 DO
 dum:=WritePixel(WinPtr^.RPort^,4*i+100,20); (* put some pixels back *)
END; (* FOR i *)

SetAPen(WinPtr^.RPort^,2);    (* change the color *)

Move(WinPtr^.RPort^,14,20);   (* set starting position *)
Draw(WinPtr^.RPort^,100,80);  (* draw a line to this position *)
Draw(WinPtr^.RPort^,14,80);   (* now draw a line to this position *)
Draw(WinPtr^.RPort^,14,20);   (* draw back to where we started *)

SetAPen(WinPtr^.RPort^,2); 		   (* set color *)
RectFill(WinPtr^.RPort^,200,20,300,60);    (* Draw Filled Rectangle *)

Move(WinPtr^.RPort^,200,30);		   (* set position *)
SetAPen(WinPtr^.RPort^,0);  		   (* draw in the background color *)
SetDrMd(WinPtr^.RPort^,Jam1);		   (* put only pen A *)
Text(WinPtr^.RPort^,ADR("Jam1 PenA=0"),11);(* 'erase' the text *)

Move(WinPtr^.RPort^,200,40);		   (* set position *)
SetAPen(WinPtr^.RPort^,1);  		   (* set color  *)
SetBPen(WinPtr^.RPort^,0);  		   (* set background color *)
SetDrMd(WinPtr^.RPort^,Jam2);		   (* put A and B Pen up *)
Text(WinPtr^.RPort^,ADR("Jam2 PenA=1 PenB=0"),18); (* put up the text *)


Move(WinPtr^.RPort^,190,50);		   (* set position *)
SetDrMd(WinPtr^.RPort^,Complement);        (* Invert the bits *)
Text(WinPtr^.RPort^,ADR("Complement"),10); (* put up the text *)

Delay(120);				   (* pause to see the text *)

(* repeat last steps to erase the text *)
Move(WinPtr^.RPort^,190,50);		   (* set position *)
SetDrMd(WinPtr^.RPort^,Complement);	   (* Invert the bits *)
Text(WinPtr^.RPort^,ADR("Complement"),10); (* put up the text *)


END DoDemo;
(* --------------------------------------------- *)

PROCEDURE TryWindow;
VAR
 WinPtr:WindowPtr;
 NewWin:NewWindow;
BEGIN

 
    WITH NewWin DO
      LeftEdge    := 10;
      TopEdge     := 10;
      Width       := 500;
      Height      := 100;
      DetailPen   := BYTE(0);
      BlockPen    := BYTE(1);
      Title       := ADR("MyWindow");
      Flags:=  WindowFlagsSet{Activate,WindowClose,WindowSizing,WindowDrag,
      			 WindowDepth,NoCareRefresh};

      IDCMPFlags  := IDCMPFlagsSet{};
      Type        := WBenchScreen;
      FirstGadget := NIL;
      CheckMark   := NIL;
      Screen      := NIL;
      BitMap      := NIL;
      MinWidth    := 20;
      MinHeight   := 20; 
      MaxWidth    := 640;
      MaxHeight   := 400;

    END; (* WITH NewWin *)


    WinPtr :=  OpenWindow (NewWin);

    IF WinPtr = NIL THEN 
      WriteString("Cannot open Window");WriteLn;
      Delay(120); (* give time to read message if run from Workbench *)
    ELSE 


     DoDemo(WinPtr); (* new *)

     MainLoop(WinPtr);
     
     CloseWindow(WinPtr^);
     
    END; (* IF WinPtr *)

  

END TryWindow;

BEGIN (* of Module *)

TryWindow;

END GraphDemo.

