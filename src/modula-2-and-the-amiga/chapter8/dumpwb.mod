MODULE dumpWB;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

(* Our Modules *)
FROM screendump IMPORT DumpRPort;

(* Benchmark Modules *)
FROM Intuition IMPORT ScreenPtr,OpenWorkBench;
FROM InOut IMPORT WriteString;
FROM AmigaDOSProcess IMPORT Delay;


PROCEDURE doit();
VAR
 err:LONGINT;
 ScrnPtr:ScreenPtr;
BEGIN

ScrnPtr:=OpenWorkBench(); (* get Workbench screen pointer *)

err:=1D;
IF ScrnPtr<>NIL THEN
 err:=DumpRPort(ScrnPtr);
END;

IF err<>0D THEN
 WriteString("error trying to dump screen");
 Delay(120);
END; (* IF err *)

END doit;

BEGIN

doit();

END dumpWB.
