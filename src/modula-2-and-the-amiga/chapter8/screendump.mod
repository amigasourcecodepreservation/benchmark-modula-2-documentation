IMPLEMENTATION MODULE ScreenDump;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

(* Benchmark Modules *)

FROM InOut IMPORT WriteCard,WriteLn,WriteString;
FROM PrinterDevice IMPORT IODRPReq,Special,SpecialSet,RPDDumpRPort,
                        IOPrtCmdReq;
FROM IODevices IMPORT DoIO,IORequestPtr,IOStdReq,IORequest,IOFlagsSet,
  	              DevicePtr,UnitPtr,OpenDevice,CloseDevice;
FROM Intuition IMPORT ScreenPtr;
FROM SYSTEM IMPORT ADR,BYTE,TSIZE,ADDRESS;
FROM Ports IMPORT MsgPortPtr;
FROM PortsUtil IMPORT CreatePort,DeletePort;
FROM Nodes IMPORT NTMessage,NodePtr;


CONST
PrinterName="printer.device";

VAR
   request : IODRPReq;
   printerPort : MsgPortPtr;
   Inp         : IORequestPtr;


PROCEDURE DumpRPort(ScrnPtr:ScreenPtr):LONGINT;
(* Dumps the specified screen to the printer. Return 0 is all goes OK,
   or the error number (and message) if there are problems *)
VAR
   error       : LONGCARD;
   error2      : LONGINT;
BEGIN

IF ScrnPtr=NIL THEN RETURN 100; END;

error2:=0;

WITH request DO
   ioDevice:=DevicePtr(0);
   ioUnit:=UnitPtr(0);
   ioError:=BYTE(0);
   ioFlags:=IOFlagsSet{0};
END;

   printerPort :=CreatePort(ADR("my.print.port"),0); (* get a port *)
   
   IF printerPort=MsgPortPtr(0) THEN
      RETURN 99;
   ELSE

   request.ioMessage.mnNode.lnType:=BYTE(NTMessage);
   request.ioMessage.mnNode.lnPri:=BYTE(0);
   request.ioMessage.mnReplyPort:=printerPort; (* point to our port *)
   request.ioMessage.mnNode.lnName:=NIL;
   request.ioMessage.mnNode.lnSucc:=NIL;
   request.ioMessage.mnNode.lnPred:=NIL;

   error:=OpenDevice(ADR(PrinterName),0,ADDRESS(ADR(request)),0D);
   	 (* open the printer device *)
	 
  error2:=LONGINT(error);
  
  IF error = 0D THEN
   
   request.ioCommand:=RPDDumpRPort;
   
   request.ioRastPort:=ADR(ScrnPtr^.RastPort);
  
   request.ioColorMap:=ScrnPtr^.ViewPort.ColorMap;

   request.ioModes:=LONGCARD(ScrnPtr^.ViewPort.Modes);
   
   request.ioSrcX:=0;
   request.ioSrcY:=0;
       
   request.ioSrcWidth:=ScrnPtr^.ViewPort.DWidth;
   request.ioSrcHeight:=ScrnPtr^.ViewPort.DHeight;
   
   request.ioDestCols:=0; 
   request.ioDestRows:=0;
   
   request.ioSpecial:=SpecialSet{SpecialAspect,SpecialFullRows,SpecialFullCols};

   Inp  :=  IORequestPtr(ADR(request));
   
   error2:= DoIO(Inp); (* actually do the screen dump *)

   IF error2<>0D THEN
    WriteString("Inp^.ioError="); WriteCard(CARDINAL(Inp^.ioError),10);WriteLn;
    WriteString("error2="); WriteCard(CARDINAL(error2),10);WriteLn;
   END; (* IF error2 *)

  CloseDevice(ADDRESS(ADR(request))); (* close the printer device *)

END; (* IF error=0 *)

DeletePort(printerPort^); (* delete our port *)

 END; (* IF printerport *)
 
RETURN error2;

END DumpRPort;

BEGIN

END ScreenDump.

