MODULE Paint;
(* Copyright 199x Armadillo Computing *)
(* Original Author Mike Lawrence *)
(* Benchmark version *)

(* Our Modules *)
FROM OurMenus IMPORT FillMenu,FillMenuItem,
		FreeMenu,StartMutExcl,EndMutExcl,DoMenuItem,DummyProc;
FROM OurReq IMPORT Confirm,GetString,GetInt;
FROM screendump IMPORT DumpRPort;
FROM Palette IMPORT Palette;

(* Benchmark Modules *)
FROM InOut IMPORT WriteString,WriteLn,WriteCard,WriteHex;
FROM Intuition IMPORT NewWindow, Window,WindowPtr, WindowFlagsSet,WindowFlags,
                      IDCMPFlags, IDCMPFlagsSet,WBenchScreen,OpenWindow,
		      CloseWindow,ModifyIDCMP,IntuiMessagePtr,Menu,MenuItem,
		      MenuFlags,MenuFlagsSet,SetMenuStrip,ClearMenuStrip,
		      MenuItemFlags,MenuItemFlagsSet,MenuItemMutualExcludeSet,
		      IntuiText,HighComp,MENUNUM,ITEMNUM,MenuNull,ItemAddress,
		      MenuItemPtr,CheckWidth,CommWidth,SUBNUM,
		      MenuPtr,OpenScreen,CloseScreen,CustomScreen,
		      ScreenPtr,NewScreen,DrawImage,Image,ShowTitle,
		      OpenWorkBench,ScreenToFront,ReportMouse;
FROM Views IMPORT LoadRGB4,ViewModes,ViewModesSet;
FROM AmigaDOSProcess IMPORT Delay;
FROM AmigaDOS IMPORT FileLock,Examine,ParentDir,FileInfoBlock,UnLock,
		     FileInfoBlockPtr,FileHandle,Open,Close,
		     ModeOldFile,ModeNewFile,Execute;
FROM SYSTEM IMPORT BYTE,ADR,ADDRESS,SHIFT,TSIZE;
FROM Tasks IMPORT Wait,SignalSet;
FROM Ports IMPORT GetMsg,ReplyMsg,MessagePtr;
FROM Rasters IMPORT DrawModeSet,Jam2,Jam1,Complement;
FROM Drawing IMPORT Move,SetAPen,SetBPen,SetDrMd,WritePixel,Draw,RectFill;
FROM Text IMPORT Text;
FROM Workbench IMPORT WBArgPtr,WBStartup,WBStartupPtr; 
FROM System IMPORT argc,argv,WBenchMsg;
FROM Graphics IMPORT BitMap;
FROM Strings IMPORT CopyString,ConcatString,StringLength;
FROM InputEvents IMPORT IECodeLButton,IECodeUpPrefix;
FROM Memory IMPORT AllocMem,FreeMem,MemReqSet,MemPublic,MemClear;

(* IFF Stuff *)
FROM IFF IMPORT IFFDone;
FROM ReadPict IMPORT ILBMFrame,ReadPicture;
FROM RemAlloc IMPORT ChipAlloc,RemFree;
FROM PutPict IMPORT PutPicture;

TYPE
edittype=(none,dots,linestart,lineend,text,rectstart,rectend);
MyPathType= ARRAY[0..500] OF CHAR;

VAR
 TempPath: MyPathType;
 fill:BOOLEAN;
 editmode,oldmode:edittype;
 lastmousex,lastmousey,mousex,mousey :CARDINAL;
 startx,starty,wx,wy :CARDINAL;
 OurMenu:MenuPtr;
 WinPtr:WindowPtr;
 ScrnPtr:ScreenPtr;
 FileName:MyPathType;
 buf:ARRAY[0..511] OF CHAR;
 Quit,NeedToSave:BOOLEAN;
 MouseIsDown,IsOneToZap:BOOLEAN;
 texttoplot:ARRAY[0..80] OF CHAR;
 reg:CARDINAL;
 
(* --------------------------------------------- *)
PROCEDURE CancelMode; FORWARD;

(* --------------------------------------------- *)
 
PROCEDURE Cli(); (* from mycli2.mod *)
VAR
 window:FileHandle;
 dummy:BOOLEAN;
 s:ScreenPtr;
 
BEGIN (* of Cli *)

s:=OpenWorkBench();
IF s<>NIL THEN
 ScreenToFront(s^); (* put workbench in front where we can see it *)
 window:=Open(ADR("NIL:"),ModeNewFile);
 IF window<>NIL THEN
  dummy:=Execute(ADR("newcli"),0,window); (* change to "newshell" for shell *)
  Close(window);
 END; (* IF window *)
END; (* IF s *)

END Cli;

(* --------------------------------------------- *)
 
PROCEDURE Save;
VAR
 fh:FileHandle;
 ok:BOOLEAN;
BEGIN
IF ScrnPtr<>NIL THEN
 
 IF GetString(WinPtr,"Name to save as:",FileName) THEN
  fh:=Open(ADR(FileName),ModeNewFile);
  
  IF fh=NIL THEN
   ok:=Confirm(WinPtr,"Could not create file","OK","OK");
  ELSE
   ShowTitle(ScrnPtr^,FALSE); (* so Title won't be saved *)
   IF PutPicture(fh,ScrnPtr^.BitMap,320,200,
   	ScrnPtr^.ViewPort.ColorMap^.ColorTable,ADR(buf),SIZE(buf)) THEN
    ok:=Confirm(WinPtr,"Could not save file","OK","OK");
   ELSE (* save worked *)
    NeedToSave:=FALSE;
   END; (* IF *)
   Close(fh);
  END; (* IF fh *)

 END; (* IF GetString *)
 
END; (* IF ScrnPtr *)

END Save;
(* --------------------------------------------- *)
PROCEDURE SetQuit;
BEGIN
  IF Confirm(WinPtr,"Really Quit?","Yes","No") THEN
   Quit:=TRUE; 
   IF NeedToSave THEN
     IF Confirm(WinPtr,"Save First?","Yes","No") THEN Save; END;
   END; (* IF *)
  END; (* IF *)
END SetQuit;
(* --------------------------------------------- *)
 
PROCEDURE SetColors(ScrnPtr:ScreenPtr;VAR frame:ILBMFrame;colors:INTEGER);

BEGIN
IF ScrnPtr<>NIL THEN

  LoadRGB4(ScrnPtr^.ViewPort,ADR(frame.colorMap),colors);

END; (* IF ScrnPtr *)
END SetColors;
(* --------------------------------------------- *)

PROCEDURE DoLoad;
VAR
 fh:FileHandle;
 frame:ILBMFrame;
 bm:BitMap;
 im:Image;
 ok:BOOLEAN;
 colors:INTEGER;
 
BEGIN
 
 fh:=Open(ADR(FileName),ModeOldFile);
 IF fh=NIL THEN
  ok:=Confirm(WinPtr,"Can't open that file","OK","OK");
 ELSE
  IF ReadPicture(fh,bm,frame,ChipAlloc)=IFFDone THEN
  
   colors:=SHIFT(1,INTEGER(frame.bmHdr.nPlanes));  (* fast way to do 2^n *)
   IF colors>32 THEN colors:=32; END;

   WITH im DO
    LeftEdge:=0;
    TopEdge:=0;
    Width:=frame.bmHdr.w;
    Height:=frame.bmHdr.h;
    Depth:=INTEGER(frame.bmHdr.nPlanes);
    ImageData:=bm.Planes[0];
    PlanePick:=BYTE(colors-1);
    PlaneOnOff:=BYTE(0);
    NextImage:=NIL;
   END; (* WITH im *)
   SetColors(ScrnPtr,frame,colors);
   DrawImage(WinPtr^.RPort^,im,0,0); 
   NeedToSave:=FALSE;
   RemFree(bm.Planes[0]);
  ELSE
   ok:=Confirm(WinPtr,"Not an IFF file?","OK","OK");
  END; (* IF IFFDone *)
  Close(fh);
 END; (* IF fh *)
 
END DoLoad;
(* --------------------------------------------- *)

PROCEDURE Load;
VAR
 pic:FileHandle;
 frame:ILBMFrame;
 bm:BitMap;
 im:Image;
BEGIN

 IF NeedToSave THEN
   IF Confirm(WinPtr,"Save First?","Yes","No") THEN Save; END;
 END; (* IF *)
  
 IF GetString(WinPtr,"File to load:",FileName) THEN
  DoLoad;
 END; (* IF GetString *)

END Load;

(* ============================= *)
PROCEDURE GetPath(InputLock:FileLock;VAR Path:MyPathType); (* from wb2.mod *)
VAR
  dir: ARRAY[0..115] OF CHAR;
  InputFIBPtr : FileInfoBlockPtr;	 (* ptr to the file info block   *)
					 
  FIBMemSize  : LONGCARD;                (* size in bytes of the   *)
                                         (* FileInfoBlock record   *)

PROCEDURE BuildPath(InputLock:FileLock);

PROCEDURE BuildString;  (* add to the front of the string we're building *)
BEGIN (* of BuildString *)
  CopyString(TempPath,Path);   (* copy old path to temporary storage *)
  CopyString(Path,dir);	       (* put new entry at front of path *)
  ConcatString(Path,TempPath); (* tack old path onto end of new entry *)
END BuildString;

BEGIN (* OF BuildPath *)

      IF Examine(InputLock, InputFIBPtr^) THEN  (* get InFile info block *)
        InputLock:=ParentDir(InputLock);
	
        IF InputLock=NIL THEN (* root dir = device *)

            IF StringLength(InputFIBPtr^.fibFileName)=0 THEN
              dir:="RAM:"; (* fix for 1.2 bug *)
            ELSE
  	      CopyString(dir,InputFIBPtr^.fibFileName);
              ConcatString(dir,":");  (* add : after device name, i.e df0: *)
            END; (* IF InputFIBPtr^ *)
	    
	    BuildString; (* add to the front of the string we're building *)

        ELSE (* dir *)

            CopyString(dir,InputFIBPtr^.fibFileName);
            ConcatString(dir,"/");	     (* add / after directory name *)
	    
	    BuildString; (* add to the front of the string we're building *)

            BuildPath(InputLock); (* note that BuildPath calls itself *)
	    
            UnLock(InputLock);    (* free memory from ParentDir call *)
        END; (* IF InputLock *)
      ELSE
        WriteString ("GetPath Couldn't examine the InputLock's info block");
	WriteLn;
      END; (* IF Examine() *)
END BuildPath;

BEGIN (* of GetPath *)

 TempPath:="";
 Path:="";

 FIBMemSize := TSIZE(FileInfoBlock);     (* get size of FileInfoBlock *)
 InputFIBPtr := AllocMem (FIBMemSize,MemReqSet{MemPublic,MemClear});
     						    (* assign long-   *)
                                                    (* aligned memory *)
 IF ADDRESS(InputFIBPtr) = NIL THEN
  WriteString("GetPath Couldn't allocate FileInfoBlock space");
  WriteLn;
 ELSE
  BuildPath(InputLock); (* go to it if the AllocMem worked *)
  FreeMem (InputFIBPtr, FIBMemSize);
 END; (* IF ADDRESS *);

END GetPath;
(* --------------------------------------------- *)
PROCEDURE InitLoad; (* from wb2.mod *)
VAR
 NumProjects:LONGINT;
 name:POINTER TO ARRAY[0..99] OF CHAR;
 args:WBArgPtr;
 startmsg:WBStartupPtr;
 
BEGIN
 
IF WBenchMsg=NIL THEN (* CLI *)

 IF argc>=2 THEN (* typed something after the program name *)
   CopyString(FileName,argv^[1]^); (* get the name typed *)
   DoLoad(); (* load the picture *)
 END; (* IF argc *)

ELSE (* Workbench *)

    startmsg:=WBStartupPtr(WBenchMsg);
    args := startmsg^.smArgList;
    
    NumProjects:=startmsg^.smNumArgs-1D;
    
    IF NumProjects>=1D THEN (* selected an icon(s) *)
    
      name := ADDRESS(args^.waName); (* the program's name *)
      INC(LONGCARD(args),SIZE(args^));
    
      GetPath(args^.waLock,FileName); (* get the path to the icon *)
      
      name := ADDRESS(args^.waName); (* the icon's name *)
      ConcatString(FileName,name^); (* add the name *)
      DoLoad(); (* load the picture *)
      
    END; (* IF NumProjects *)
 
END; (* IF WBenchMsg *)
 
END InitLoad;
(* --------------------------------------------- *)
PROCEDURE Print;
VAR
 err:LONGINT;
 ok:BOOLEAN;
BEGIN

IF Confirm(WinPtr,"Ready to Print?","Go","Cancel") THEN
 err:=1D;
 IF ScrnPtr<>NIL THEN
  ShowTitle(ScrnPtr^,FALSE); (* so Title won't be printed *)
  err:=DumpRPort(ScrnPtr);
 END;
 
 IF err<>0D THEN
  ok:=Confirm(WinPtr,"Could not print","OK","OK");
 END; (* IF err *)
 
END; (* IF Confirm *)

END Print;
(* --------------------------------------------- *)
PROCEDURE Show;
BEGIN
 ShowTitle(ScrnPtr^,TRUE);
END Show;
(* --------------------------------------------- *)
PROCEDURE Hide;
BEGIN
 ShowTitle(ScrnPtr^,FALSE);
END Hide;

(* =============================================== *)

PROCEDURE DoLine;
BEGIN
 CancelMode;
 editmode:=linestart;
END DoLine;

(* ------------------------------------------------- *)

PROCEDURE DoDots;
BEGIN
 CancelMode;
 editmode:=dots;
END DoDots;

(* ------------------------------------------------- *)

PROCEDURE DoRect;
BEGIN
 CancelMode;
 editmode:=rectstart;
 fill:=FALSE;
END DoRect;

(* ------------------------------------------------- *)

PROCEDURE DoFillRect;
BEGIN
 CancelMode;
 editmode:=rectstart;
 fill:=TRUE;
END DoFillRect;

(* ------------------------------------------------- *)

PROCEDURE DoText;
BEGIN
 IF GetString(WinPtr,"Enter Text",texttoplot) THEN
  IF StringLength(texttoplot)>0 THEN
   CancelMode; 
   oldmode:=editmode;
   editmode:=text;
  END; (* IF *)
 END; (* IF *)
END DoText;

(* --------------------------------------------- *)
PROCEDURE cls(WinPtr:WindowPtr); (* clears a window without zapping borders *)
VAR					(* from GraphDemo.mod *)
 x,y,w,h:INTEGER;
BEGIN

IF WinPtr<>NIL THEN
 x:=INTEGER(WinPtr^.BorderLeft);
 y:=INTEGER(WinPtr^.BorderTop);
 w:=WinPtr^.Width-INTEGER(WinPtr^.BorderRight);
 h:=WinPtr^.Height-INTEGER(WinPtr^.BorderBottom);
 
 IF (w>x) AND (h>y) THEN 	    (*  make sure RectFill is safe *)
  SetDrMd(WinPtr^.RPort^,Jam1);     (* draw PenA *)
  SetAPen(WinPtr^.RPort^,0); 	    (* set the color to background *)
  RectFill(WinPtr^.RPort^,x,y,w,h); (* Clear the window *)
 END; (* IF w and h *)
END; (* IF WinPtr *)
 
END cls;
(* --------------------------------------------- *)
PROCEDURE Clear;
BEGIN

IF Confirm(WinPtr,"Erase Screen?","Yes","No") THEN
 CancelMode();
 IF NeedToSave THEN
   IF Confirm(WinPtr,"Save First?","Yes","No") THEN Save; END;
 END; (* IF *)
 cls(WinPtr);
 FileName:="UnNamed";
 NeedToSave:=FALSE;
END; (* IF *)

END Clear;
(* --------------------------------------------- *)
PROCEDURE DoPalette;
VAR
 canceled,error:BOOLEAN;
 errmsg:ARRAY[0..80] OF CHAR;
BEGIN
 Palette(ScrnPtr,TRUE,"Edit Colors",errmsg,reg,10,10,canceled,error);
 IF (NOT canceled) AND (NOT error) THEN
  NeedToSave:=TRUE;
 END; (* IF *)
END DoPalette;
(* --------------------------------------------- *)

PROCEDURE CreateMenu(); (* from menu3.mod *)
VAR
 OurMenu1,OurMenu2:MenuPtr;
 OurMenuItem,OurMenuItem2:MenuItemPtr;
 OurSubItem,OurSubItem2:MenuItemPtr;
 
BEGIN



 OurMenu:=FillMenu(NIL,"Project");


 OurMenuItem2:=FillMenuItem(OurMenu,NIL,NIL,    "New  ",Clear,CHR(0),
 		MenuItemFlagsSet{}); 
		
 OurMenuItem:=FillMenuItem(NIL,OurMenuItem2,NIL,"Load ",Load,'L',
 		MenuItemFlagsSet{}); 

 OurMenuItem2:=FillMenuItem(NIL,OurMenuItem,NIL,"Save ",Save,'S',
 		MenuItemFlagsSet{}); 

 OurMenuItem:=FillMenuItem(NIL,OurMenuItem2,NIL,"Print",Print,'P',
 		MenuItemFlagsSet{}); 
		
 OurMenuItem2:=FillMenuItem(NIL,OurMenuItem,NIL,"Quit ",SetQuit,'Q',
 		MenuItemFlagsSet{}); 

		
 OurMenu1:=FillMenu(OurMenu,"Edit");
   
   StartMutExcl();
   
   OurMenuItem:=FillMenuItem(OurMenu1,NIL,NIL,    "Line     ",DoLine,CHR(0),
    		MenuItemFlagsSet{CheckIt});

   OurMenuItem2:=FillMenuItem(NIL,OurMenuItem,NIL,"Box      ",DoRect,CHR(0),
 		MenuItemFlagsSet{CheckIt}); 

   OurMenuItem:=FillMenuItem(NIL,OurMenuItem2,NIL,"Solid Box",DoFillRect,CHR(0),
 		MenuItemFlagsSet{CheckIt}); 

   OurMenuItem2:=FillMenuItem(NIL,OurMenuItem,NIL,"Dots     ",DoDots,CHR(0),
 		MenuItemFlagsSet{CheckIt}); 

   EndMutExcl();

   OurMenuItem:=FillMenuItem(NIL,OurMenuItem2,NIL,"Text     ",DoText,CHR(0),
 		MenuItemFlagsSet{}); 
		
   OurMenuItem2:=FillMenuItem(NIL,OurMenuItem,NIL,"Palette  ",DoPalette,CHR(0),
 		MenuItemFlagsSet{}); 
   
 OurMenu2:=FillMenu(OurMenu1,"Other");

 OurMenuItem:=FillMenuItem(OurMenu2,NIL,NIL,    "Show Title",Show,CHR(0),
 		MenuItemFlagsSet{}); 

 OurMenuItem2:=FillMenuItem(NIL,OurMenuItem,NIL,"Hide Title",Hide,CHR(0),
 		MenuItemFlagsSet{}); 

 OurMenuItem:=FillMenuItem(NIL,OurMenuItem2,NIL,"New CLI   ",Cli,CHR(0),
 		MenuItemFlagsSet{}); 



END CreateMenu;

(* ------------------------------------------------- *)


PROCEDURE DoMenu(code:CARDINAL); (* from menu3.mod *)
 
BEGIN

IF code=MenuNull THEN
 CancelMode;
ELSE
 DoMenuItem(OurMenu,code);
END; (* IF code *)

  (* make sure flags are set correctly *)
  (*  Screen:=Checked IN SubItem3^.Flags;  *)

  
END DoMenu;


(* ------------------------------------------------- *)

PROCEDURE ZapOldText();
BEGIN
 IF IsOneToZap THEN
  Move(WinPtr^.RPort^,lastmousex,lastmousey); 
  SetAPen(WinPtr^.RPort^,reg); 
  SetBPen(WinPtr^.RPort^,0);
  SetDrMd(WinPtr^.RPort^,Complement);
  Text(WinPtr^.RPort^,ADR(texttoplot),StringLength(texttoplot));
  IsOneToZap:=FALSE;
 END; (* IF IsOneToZap *)

END ZapOldText;


(* =============================================== *)

PROCEDURE ZapOldLine();
BEGIN
 IF IsOneToZap THEN
  SetDrMd(WinPtr^.RPort^,Complement);
  SetAPen(WinPtr^.RPort^,reg);
  SetBPen(WinPtr^.RPort^,0);
  Move(WinPtr^.RPort^,startx,starty);
  Draw(WinPtr^.RPort^,lastmousex,lastmousey);
  IsOneToZap:=FALSE;
 END; (* IF IsOneToZap *)

END ZapOldLine;


(* =============================================== *)

PROCEDURE Rect(x1,y1,x2,y2:CARDINAL);
BEGIN
 Move(WinPtr^.RPort^,x1,y1);
 Draw(WinPtr^.RPort^,x2,y1);
 Draw(WinPtr^.RPort^,x2,y2);
 Draw(WinPtr^.RPort^,x1,y2);
 Draw(WinPtr^.RPort^,x1,y1);
END Rect;
(* =============================================== *)

PROCEDURE ZapOldRect();
BEGIN
 IF IsOneToZap THEN
  SetDrMd(WinPtr^.RPort^,Complement);
  SetAPen(WinPtr^.RPort^,reg);
  SetBPen(WinPtr^.RPort^,0);
  Rect(startx,starty,lastmousex,lastmousey);
  IsOneToZap:=FALSE;
 END; (* IF IsOneToZap *)

END ZapOldRect;

(* =============================================== *)
PROCEDURE CancelMode();
BEGIN

IF editmode=lineend THEN
 ZapOldLine;
 editmode:=linestart;
ELSIF editmode=rectend THEN
 ZapOldRect;
 editmode:=rectstart;
ELSIF editmode=text THEN
 ZapOldText;
 editmode:=oldmode;
END; (* IF editmode *)

END CancelMode;

(* =============================================== *)
PROCEDURE swap(VAR a,b:CARDINAL);
VAR
 t:CARDINAL;
BEGIN
t:=a;
a:=b;
b:=t;
END swap;
(* =============================================== *)

PROCEDURE HandleClick(mousex,mousey:CARDINAL);
VAR
 dum:LONGINT;
BEGIN
IF WinPtr=NIL THEN
(* PrintMsg("WinPtr NIL in HandleClick"); *)
ELSE
IF WinPtr^.RPort=NIL THEN
(* PrintMsg("WinPtr^.Rport NIL in HandleClick"); *)
ELSE

IF editmode=linestart THEN
 startx:=mousex;
 starty:=mousey;
 IsOneToZap:=FALSE;
 editmode:=lineend;
ELSIF editmode=lineend THEN
 ZapOldLine;
 Move(WinPtr^.RPort^,startx,starty);

 SetAPen(WinPtr^.RPort^,reg);
 SetBPen(WinPtr^.RPort^,0);
 SetDrMd(WinPtr^.RPort^,Jam1);

 Draw(WinPtr^.RPort^,mousex,mousey);
 NeedToSave:=TRUE;
 editmode:=linestart; 
 
ELSIF editmode=rectstart THEN
 startx:=mousex;
 starty:=mousey;
 IsOneToZap:=FALSE;
 editmode:=rectend;
ELSIF editmode=rectend THEN
 ZapOldRect;

 SetAPen(WinPtr^.RPort^,reg);
 SetBPen(WinPtr^.RPort^,0);
 SetDrMd(WinPtr^.RPort^,Jam1);
 
 IF fill THEN
  IF mousey<starty THEN swap(mousey,starty); END;
   (* need to do this to make RectFill happy *)
  IF mousex<startx THEN swap(mousex,startx); END;
  RectFill(WinPtr^.RPort^,startx,starty,mousex,mousey);
 ELSE
  Rect(startx,starty,mousex,mousey);
 END;
  
 NeedToSave:=TRUE;
 editmode:=rectstart; (* none; *)
 
ELSIF editmode=text THEN
 ZapOldText;
 Move(WinPtr^.RPort^,mousex,mousey);

 SetAPen(WinPtr^.RPort^,reg); 
 SetBPen(WinPtr^.RPort^,0);
 SetDrMd(WinPtr^.RPort^,Jam1);

 Text(WinPtr^.RPort^,ADR(texttoplot),StringLength(texttoplot));
 NeedToSave:=TRUE;
 editmode:=oldmode; 
ELSIF editmode=dots THEN
 IF MouseIsDown THEN
  SetAPen(WinPtr^.RPort^,reg);
  SetDrMd(WinPtr^.RPort^,Jam1);
  dum:=WritePixel(WinPtr^.RPort^,mousex,mousey);
  NeedToSave:=TRUE;
 END; (* IF *)
END; (* IF editmode *)
END; (* IF WinPtr^.RPort *)
END; (* IF WinPtr *)
END HandleClick;

(* =============================================== *)

PROCEDURE HandleMouse(mousex,mousey:CARDINAL);
VAR
 dum:LONGINT;
BEGIN
IF WinPtr=NIL THEN
(* PrintMsg("WinPtr NIL in HandleMouse"); *)
ELSE
IF WinPtr^.RPort=NIL THEN
(* PrintMsg("WinPtr^.Rport NIL in HandleMouse"); *)
ELSE

CASE CARDINAL(editmode) OF

CARDINAL(linestart): |
 
CARDINAL(dots):
 IF MouseIsDown THEN
  SetAPen(WinPtr^.RPort^,reg);
  SetDrMd(WinPtr^.RPort^,Jam1);
  dum:=WritePixel(WinPtr^.RPort^,mousex,mousey);
  NeedToSave:=TRUE;
 END; (* IF *) |

CARDINAL(lineend):
  ZapOldLine;

  SetDrMd(WinPtr^.RPort^,Complement);
  SetAPen(WinPtr^.RPort^,reg);
  SetBPen(WinPtr^.RPort^,0);
  Move(WinPtr^.RPort^,startx,starty);
  Draw(WinPtr^.RPort^,mousex,mousey);
  lastmousex:=mousex;
  lastmousey:=mousey;
  IsOneToZap:=TRUE; |
 
CARDINAL(rectend):
  ZapOldRect;

  SetDrMd(WinPtr^.RPort^,Complement);
  SetAPen(WinPtr^.RPort^,reg);
  SetBPen(WinPtr^.RPort^,0);
  Rect(startx,starty,mousex,mousey);
  lastmousex:=mousex;
  lastmousey:=mousey;
  IsOneToZap:=TRUE; |
 
CARDINAL(text):
 ZapOldText;
 Move(WinPtr^.RPort^,mousex,mousey);

 SetAPen(WinPtr^.RPort^,reg); 
 SetBPen(WinPtr^.RPort^,0);

 SetDrMd(WinPtr^.RPort^,Complement);
 Text(WinPtr^.RPort^,ADR(texttoplot),StringLength(texttoplot));

 lastmousex:=mousex;
 lastmousey:=mousey;
 
 IsOneToZap:=TRUE; |
 
 ELSE 

END; (* CASE editmode *)
END; (* IF WinPtr^.RPort *)
END; (* IF WinPtr *)
END HandleMouse;

(* =============================================== *)

PROCEDURE MainLoop(WinPtr:WindowPtr); (* from menu3.mod *)
VAR
 signal,inset: SignalSet;
 insig1: CARDINAL;
 MsgPtr: IntuiMessagePtr;
 class : IDCMPFlagsSet;
 code  : CARDINAL;
 normflags,busyflags,busy2:IDCMPFlagsSet;
 wasmode:edittype;
 
BEGIN (* of MainLoop *)
 Quit:=FALSE;
 MouseIsDown:=FALSE;
 
 normflags:=IDCMPFlagsSet{Closewindow,MenuPick,MouseMove,MouseButtons,
 			  InactiveWindow,ActiveWindow};
 busyflags:=normflags;
 EXCL(busyflags,MouseMove);
 
 busy2:=busyflags;
 EXCL(busy2,InactiveWindow);
 EXCL(busy2,ActiveWindow); (* so text entry window doesn't cancel text mode *)
  
 ModifyIDCMP(WinPtr^,normflags); (* ask for messages *)
 
 ReportMouse(WinPtr^,TRUE);  (* give us move messages even with the button up *)
    
  insig1:=CARDINAL(WinPtr^.UserPort^.mpSigBit); (* Window's message port *)
  inset:=SignalSet{};	(* clear the set *)
  INCL(inset,insig1);	(* ports to look for messages from *)

 REPEAT

  signal:=Wait(inset); (* wait for messages. (Go to sleep) *)
  
  (* we will wake up when the program receives a message *)
  
  IF (insig1 IN signal) THEN (* this is an IDCMP message from the window *)

  REPEAT (* in case there are multiple messages *)
    
   MsgPtr:=GetMsg(WinPtr^.UserPort^); (* get the message *)
   
   IF MsgPtr<>NIL THEN  (* make sure the message exists *)
   
    class:=MsgPtr^.Class;	(* remember these because they go away when
    				  we ReplyMsg. *)
				  
    code:=MsgPtr^.Code; 
    mousex:=MsgPtr^.MouseX;
    mousey:=MsgPtr^.MouseY;
    
    ReplyMsg(MessagePtr(MsgPtr)); (* Reply to the message *)
      
    IF (class=IDCMPFlagsSet{Closewindow}) THEN (* check for a close message *)
    
     ModifyIDCMP(WinPtr^,busyflags);
     SetQuit();     (* set our flag in response to the message *)
     IF NOT Quit THEN ModifyIDCMP(WinPtr^,normflags); END;
     
    ELSIF (class=IDCMPFlagsSet{MenuPick}) THEN
     
     ModifyIDCMP(WinPtr^,busy2);
     DoMenu(code);
     IF NOT Quit THEN ModifyIDCMP(WinPtr^,normflags); END;
     
    ELSIF (class=IDCMPFlagsSet{MouseButtons}) THEN
     
     ModifyIDCMP(WinPtr^,busyflags);
     (*  WriteString("got:");WriteHex(code,5);WriteLn; *)
     IF (code = IECodeLButton)  THEN
      MouseIsDown:=TRUE;
      HandleClick(mousex,mousey); (* do what we need to *)
     ELSIF (code = IECodeLButton+IECodeUpPrefix) THEN
      MouseIsDown:=FALSE;
     END; (* IF code *)
     ModifyIDCMP(WinPtr^,normflags);
     
    ELSIF (class=IDCMPFlagsSet{MouseMove}) THEN
    
     IF editmode<>dots THEN ModifyIDCMP(WinPtr^,busyflags); END;
      HandleMouse(mousex,mousey); (* do what we need to *)
     IF editmode<>dots THEN ModifyIDCMP(WinPtr^,normflags); END;

    ELSIF (class=IDCMPFlagsSet{InactiveWindow}) THEN
    
      ModifyIDCMP(WinPtr^,busyflags);
      wasmode:=editmode;
      CancelMode();
      IF wasmode<>text THEN oldmode:=editmode; END; (* remember for later *)
      editmode:=none; (* so when we click back, we won't draw into the window *)
      IF oldmode=dots THEN oldmode:=none; END;
      ModifyIDCMP(WinPtr^,normflags);
     
    ELSIF (class=IDCMPFlagsSet{ActiveWindow}) THEN
    
      ModifyIDCMP(WinPtr^,busyflags);
      editmode:=oldmode; (* put mode back *)
      MouseIsDown:=FALSE; (* for dots mode *)
      ModifyIDCMP(WinPtr^,normflags);
       
    END; (* IF class *)
    
   END; (* IF MsgPtr *)
   
  UNTIL MsgPtr=NIL;
   
  END; (* IF insig1 IN signal *)
  
 UNTIL Quit; (* repeat until the Quit flag is TRUE *)
 
END MainLoop;

(* --------------------------------------------- *)

PROCEDURE TryWindow; (* from screen.mod *)
VAR
 NewWin:NewWindow;
 NewScrn:NewScreen;
BEGIN
   
  WITH NewScrn DO

    LeftEdge     := 0;
    TopEdge      := 0;
    Width        := 320;
    Height       := 200;
    Depth        := 5;		(* 5 bit planes, so 2^5=32 colors *)
    DetailPen    := BYTE(0);
    BlockPen     := BYTE(1);
    ViewModes    := ViewModesSet{};
    Type         := CustomScreen;
    Font         := NIL; (* use default font *)
    DefaultTitle := ADR("Paint");
    Gadgets      := NIL;
    CustomBitMap := NIL;

  END; (* WITH NewScrn *)

  ScrnPtr := OpenScreen (NewScrn);

  IF ScrnPtr = NIL THEN
    WriteString("Cannot open the new custom screen!");WriteLn;
    Delay(120); (* pause for the error message *)
    
  ELSE (* screen is open *)

 
    WITH NewWin DO
      LeftEdge    := 0;
      TopEdge     := 0;
      Width       := 320;
      Height      := 200;
      DetailPen   := BYTE(0);
      BlockPen    := BYTE(1);
      Title       := NIL; (* need this for borderless backdrop window *)
      Flags:=  WindowFlagsSet{Activate,NoCareRefresh,Borderless,BackDrop};

      IDCMPFlags  := IDCMPFlagsSet{};
      Type        := CustomScreen;
      FirstGadget := NIL;
      CheckMark   := NIL;
      Screen      := ScrnPtr;
      BitMap      := NIL;
      MinWidth    := 20;
      MinHeight   := 20; 
      MaxWidth    := 640;
      MaxHeight   := 400;

    END; (* WITH NewWin *)


    WinPtr :=  OpenWindow (NewWin);

    IF WinPtr = NIL THEN 
      WriteString("Cannot open Window");WriteLn;
      Delay(120); (* give time to read message if run from Workbench *)
    ELSE 
    
     CreateMenu;          

     IF OurMenu<> NIL THEN SetMenuStrip(WinPtr^,OurMenu^); END;

      FileName:="UnNamed";
      NeedToSave:=FALSE;
      editmode:=none;
      oldmode:=editmode;
      fill:=FALSE;
      reg:=1;
      texttoplot:="";
      
      InitLoad(); (* check for CLI or Workbench inputs *)

      MainLoop(WinPtr);
     
     IF OurMenu<> NIL THEN ClearMenuStrip(WinPtr^); END;
     
     CloseWindow(WinPtr^);
     
     FreeMenu(OurMenu); (* free up the memory for the menu stuff *)
     
    END; (* IF WinPtr *)
     
    CloseScreen(ScrnPtr^);

  END; (* IF ScrnPtr *)

END TryWindow;

BEGIN (* of Module *)

TryWindow;

END Paint.
