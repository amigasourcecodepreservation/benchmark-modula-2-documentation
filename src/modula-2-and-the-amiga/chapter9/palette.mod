IMPLEMENTATION MODULE Palette;
(* Original Author Mike Lawrence *)
(* Benchmark version *)

(* Our Modules *)
			
FROM OurGadgets IMPORT MakeBool,FreeGadgets,MakeString,MakeInt,MakeProp;

(* Benchmark Modules *)

FROM Interrupts IMPORT Forbid,Permit;
FROM Tasks IMPORT Wait,SignalSet;
FROM InputEvents IMPORT IECodeLButton;
FROM SYSTEM IMPORT ADDRESS,ADR,BYTE,TSIZE,SHIFT;
FROM Intuition IMPORT WindowPtr,IDCMPFlags,IDCMPFlagsSet,IntuiMessagePtr,
                      ScreenPtr,IntuiText,
                      GadgetFlagsSet,GadgetActivationSet,GadgetPtr,MenuItemPtr,
		      ReportMouse,ModifyIDCMP,WindowFlagsSet,
		      WindowFlags,GadgetFlags,GadgetActivation,BoolGadget,
		      StrGadget,PropGadget,PropInfoFlags,PropInfoFlagsSet,
		      PropInfoPtr,MaxBody,KnobVMin,MaxPot,ModifyProp,
		      RefreshGadgets,ActivateGadget,OpenWindow,CloseWindow,
		      NewWindow,CustomScreen,WBenchScreen;
FROM Ports     IMPORT ReplyMsg,WaitPort,GetMsg,MessagePtr;
FROM Drawing      IMPORT Draw,Move,SetAPen,SetBPen,SetDrMd,WritePixel,
                      RectFill;
FROM Strings   IMPORT CopyString;
FROM Rasters IMPORT DrawModeSet,Jam1,Jam2,Complement;
FROM Views IMPORT LoadRGB4,ViewModesSet, ViewModes,ColorMap,ColorTable,
 		   GetColorMap,ColorMapPtr,FreeColorMap;
FROM Memory IMPORT AllocMem,FreeMem,MemReqSet,MemClear,MemPublic,MemChip;


CONST
 BoxXsize=16; 
 BoxXdist=BoxXsize;
 BoxYsize=8; 
 BoxYdist=BoxYsize;
 x1=10;
 y2=60;
 y1=y2+4*BoxYdist; 
 x2=x1+8*BoxXdist;
 fontsize=8D;
 rows=LONGCARD(y1-y2) DIV fontsize;


VAR
 ResetCol:LONGINT;
 mousegad:INTEGER;
 BusyFlags, NormFlags:IDCMPFlagsSet;
 Globalcanceled:BOOLEAN;

 latch:CARDINAL;
 gadlist,RpropGad,GpropGad,BpropGad:GadgetPtr;
 start,wasstart:LONGCARD;

 Rpropinfo,Gpropinfo,Bpropinfo:PropInfoPtr;
 pFlags: PropInfoFlagsSet;
 WinPtr: WindowPtr;
 Quit,Test :BOOLEAN;
 mousex,mousey,lastvpot :CARDINAL;

(* =============================================== *)

PROCEDURE GetMap(ScrnPtr:ScreenPtr;VAR Cmap:ColorMap);
BEGIN
 Forbid();
 IF ScrnPtr<>NIL THEN

 IF ScrnPtr^.ViewPort.ColorMap<>NIL THEN

 IF ScrnPtr^.ViewPort.ColorMap^.ColorTable <>NIL THEN

  IF Cmap.ColorTable<>NIL THEN

   Cmap.ColorTable^:=ScrnPtr^.ViewPort.ColorMap^.ColorTable^;
   Cmap.Flags:=ScrnPtr^.ViewPort.ColorMap^.Flags;
   Cmap.Type:=ScrnPtr^.ViewPort.ColorMap^.Type;
   Cmap.Count:=ScrnPtr^.ViewPort.ColorMap^.Count;
  END; (* IF Cmap *)
  
 END; (* IF ScrnPtr *)
 END; (* IF ScrnPtr *)
 END; (* IF ScrnPtr *)
  Permit();
END GetMap;
(* =============================================== *)

PROCEDURE SetMap(ScrnPtr:ScreenPtr;VAR Cmap:ColorMap);
BEGIN

IF ScrnPtr<>NIL THEN

 IF Cmap.ColorTable<>NIL THEN

  LoadRGB4(ScrnPtr^.ViewPort,Cmap.ColorTable,Cmap.Count);
 END; (* IF Cmap.ColorTable *)
END; (* IF ScrnPtr *)
  

END SetMap;

(* =============================================== *)
PROCEDURE TakeApart(color:LONGINT;VAR r,g,b:LONGINT);
BEGIN
 r:=SHIFT(color,-8);
 color:=color-SHIFT(r,8);
 g:=SHIFT(color,-4);
 b:=color-SHIFT(g,4);

END TakeApart;
(* =============================================== *)
PROCEDURE PutTogether(VAR color:LONGINT;r,g,b:LONGINT);
BEGIN
 color:=b+SHIFT(g,4)+SHIFT(r,8);

END PutTogether;
(* =============================================== *)

PROCEDURE SetAColor(ScrnPtr:ScreenPtr;VAR Cmap:ColorMap;reg,col:LONGINT);
BEGIN
   
  IF ScrnPtr=NIL THEN RETURN; END; (* quit if no screen pointer *)

  IF reg<0D THEN reg:=0D; END;  IF reg>31D THEN reg:=31D; END;

   IF reg<=LONGINT(Cmap.Count)-1D THEN
      Cmap.ColorTable^[CARDINAL(reg)]:=CARDINAL(col);
      SetMap(ScrnPtr,Cmap);
   END; (* IF reg *)
 
END SetAColor;
(* =============================================== *)

PROCEDURE CheckForMes(ScrnPtr:ScreenPtr;VAR Cmap:ColorMap;
		goback:BOOLEAN;VAR newdir:BOOLEAN;VAR reg:CARDINAL); FORWARD;
(* =============================================== *)

PROCEDURE FixGad(ScrnPtr:ScreenPtr;reg:CARDINAL);
VAR
 temp,temp2:LONGCARD;
 col,r,g,b:LONGINT;
BEGIN

IF WinPtr<>NIL THEN
   SetAPen(WinPtr^.RPort^,reg);
   SetDrMd(WinPtr^.RPort^,Jam1);
   RectFill(WinPtr^.RPort^,15,15,50,40);
END; (* IF WinPtr *)

     IF ScrnPtr=NIL THEN RETURN; END; (* quit if no screenptr *)
     IF ScrnPtr^.ViewPort.ColorMap=NIL THEN RETURN; END;
     IF ScrnPtr^.ViewPort.ColorMap^.ColorTable=NIL THEN RETURN; END;
     IF WinPtr=NIL THEN RETURN; END;
     
      temp:=16;

      col:=LONGCARD(ScrnPtr^.ViewPort.ColorMap^.ColorTable^[CARDINAL(reg)]);
      ResetCol:=col;  
      TakeApart(col,r,g,b);

	temp2:=(15D-r)*LONGINT(MaxPot) DIV 15D;
	
	IF (RpropGad<>NIL) THEN
           ModifyProp(RpropGad^,WinPtr^,NIL,pFlags,0,temp2,20,
			(LONGCARD(MaxBody) DIV temp) );
	END; (* IF  *)


	temp2:=(15D-g)*LONGINT(MaxPot) DIV 15D;
	
	IF (GpropGad<>NIL) THEN
           ModifyProp(GpropGad^,WinPtr^,NIL,pFlags,0,temp2,20,
			(LONGCARD(MaxBody) DIV temp) );
	END; (* IF  *)

	temp2:=(15D-b)*LONGINT(MaxPot) DIV 15D;
		
	IF (BpropGad<>NIL) THEN
           ModifyProp(BpropGad^,WinPtr^,NIL,pFlags,0,temp2,20,
			(LONGCARD(MaxBody) DIV temp) );
	END; (* IF  *)
	     
END FixGad;

(* =============================================== *)

PROCEDURE SlideColor(ScrnPtr:ScreenPtr;VAR Cmap:ColorMap;doit:BOOLEAN;
			which:CARDINAL;VAR reg:CARDINAL);
VAR
 inc:CARDINAL;
 col:LONGINT;
 r,g,b:LONGINT;
 
BEGIN

     IF ScrnPtr=NIL THEN RETURN; END; (* quit if no screenptr *)
     IF ScrnPtr^.ViewPort.ColorMap=NIL THEN RETURN; END;
     IF ScrnPtr^.ViewPort.ColorMap^.ColorTable=NIL THEN RETURN; END;
     IF WinPtr=NIL THEN RETURN; END;
  
  col:=LONGCARD(ScrnPtr^.ViewPort.ColorMap^.ColorTable^[CARDINAL(reg)]);
  
  TakeApart(col,r,g,b);

     CASE which OF
     9:  IF Rpropinfo<>NIL THEN latch:=Rpropinfo^.VertPot; END; | (* red *)
     8:  IF Gpropinfo<>NIL THEN latch:=Gpropinfo^.VertPot; END; | (* green *)
     7:  IF Bpropinfo<>NIL THEN latch:=Bpropinfo^.VertPot; END; | (* blue *)
     END; (* CASE *)

	    
  
  IF (latch<>lastvpot) OR doit THEN
    start:= 15D-LONGCARD (latch) * 15D DIV LONGCARD(MaxPot) ;
    
    IF (start<>wasstart) OR doit THEN 
     CASE which OF
     9:  PutTogether(col,LONGINT(start),g,b); |
     8:  PutTogether(col,r,LONGINT(start),b); |
     7:  PutTogether(col,r,g,LONGINT(start)); |
     END; (* CASE *)
     
     
     SetAColor(ScrnPtr,Cmap,reg,col); 
    END; (* IF start *)
    
    
    lastvpot:=latch;
    wasstart:=start;
  END; (* IF latch *)

END SlideColor;

(* =============================================== *)
PROCEDURE GetSelect(ScrnPtr:ScreenPtr;mousex,mousey:CARDINAL;VAR reg:CARDINAL);
VAR
 at:LONGCARD;
 hilite,which:CARDINAL;
 numcols:CARDINAL;

BEGIN

  IF ScrnPtr=NIL THEN RETURN; END;
  IF ScrnPtr^.RastPort.BitMap=NIL THEN RETURN; END;

  numcols:=CARDINAL(ScrnPtr^.RastPort.BitMap^.Depth);
  numcols:=SHIFT(1,numcols);

 which:=(mousex-x1) DIV BoxXdist + 8* ((mousey-y2) DIV BoxYdist);
 IF which>numcols-1 THEN which:=numcols-1; END;
 reg:=which;
 FixGad(ScrnPtr,reg);
 
END GetSelect;


(* =============================================== *)

PROCEDURE CheckForMes(ScrnPtr:ScreenPtr;VAR Cmap:ColorMap;
		goback:BOOLEAN;VAR newdir:BOOLEAN;VAR reg:CARDINAL);

VAR 
    WhichGadPtr:GadgetPtr;
    whichgad : INTEGER;
    lastchar: CHAR;
    MsgPtr: IntuiMessagePtr;
    class : IDCMPFlagsSet;
    code  : CARDINAL;
    diddir: BOOLEAN;
    temp:LONGCARD;
    pos:INTEGER;

PROCEDURE SetNorm;
BEGIN
IF goback THEN
 IF WinPtr<>NIL THEN ModifyIDCMP(WinPtr^,NormFlags); END;
END; (* IF goback *)
END SetNorm;

BEGIN (* of CheckForMes *)

newdir:=FALSE;

  REPEAT
  IF WinPtr<>NIL THEN
   IF WinPtr^.UserPort<>NIL THEN
    MsgPtr:=GetMsg(WinPtr^.UserPort^);
   ELSE
    MsgPtr:=NIL;
   END; (* IF WinPtr *)
  ELSE
    MsgPtr:=NIL;
  END; (* IF WinPtr *)

    IF MsgPtr <> NIL THEN

     class:=MsgPtr^.Class;
     code :=MsgPtr^.Code;
     mousex:=MsgPtr^.MouseX;
     mousey:=MsgPtr^.MouseY;


  WhichGadPtr :=GadgetPtr(MsgPtr^.IAddress); 

     ReplyMsg(MessagePtr(MsgPtr));

     IF (class=IDCMPFlagsSet{Closewindow}) THEN
       ModifyIDCMP(WinPtr^,BusyFlags);
       Quit:=TRUE;
       Globalcanceled:=TRUE;
       SetNorm;

     ELSIF (class=IDCMPFlagsSet{MouseMove}) THEN
       ModifyIDCMP(WinPtr^,BusyFlags);
        IF mousegad<>0 THEN SlideColor(ScrnPtr,Cmap,FALSE,mousegad,reg); END;
       SetNorm;
     ELSIF  ( (class = IDCMPFlagsSet{MouseButtons}) & 
		(code = IECodeLButton) ) THEN
       ModifyIDCMP(WinPtr^,BusyFlags);
       IF (mousex>x1) AND (mousex<x2) AND (mousey<y1) AND (mousey>y2) THEN
	GetSelect(ScrnPtr,mousex,mousey,reg);
       END; (* IF mousex *)
       SetNorm;

     ELSIF (class = IDCMPFlagsSet{GadgetDown}) THEN
       IF WhichGadPtr<>NIL THEN 
         whichgad:=WhichGadPtr^.GadgetID;
       ELSE 
         whichgad:=0;
       END; (* IF WhichGadPtr *)
      mousegad:=whichgad; IF (mousegad<7) OR (mousegad>9) THEN mousegad:=0; END;

     ELSIF (class = IDCMPFlagsSet{GadgetUp}) THEN
       ModifyIDCMP(WinPtr^,BusyFlags);
       mousegad:=0;

      IF WhichGadPtr<>NIL THEN 
        whichgad:=WhichGadPtr^.GadgetID;
      ELSE 
        whichgad:=0;
      END; (* IF WhichGadPtr *)
       

       CASE whichgad OF
	7,8,9:   (* proportional gadget *)
           SlideColor(ScrnPtr,Cmap,TRUE,whichgad,reg);
	 |
        10: Quit:=TRUE; 		     (* Accept button *)
	   Globalcanceled:=FALSE;
	 |
	2: 				     (* Reset  *)
	 SetAColor(ScrnPtr,Cmap,reg,ResetCol);
	 FixGad(ScrnPtr,reg);
         |
        1: Quit:=TRUE; Globalcanceled:=TRUE; (* Cancel *)
	 |
       END; (* CASE whichgad *)
       SetNorm;
     END; (* IF (class= *)
   END; (* IF MsgPtr<>NIL *)
 UNTIL (MsgPtr =NIL);


END CheckForMes;
(* =============================================== *)
PROCEDURE MainLoop(ScrnPtr:ScreenPtr;VAR Cmap:ColorMap;VAR reg:CARDINAL);

VAR 
    signal,inset: SignalSet;
    insig1,insig2: CARDINAL;
    MsgPtr: IntuiMessagePtr;
    class : IDCMPFlagsSet;
    code  : CARDINAL;
    dum   : BOOLEAN;
BEGIN
 Globalcanceled:=FALSE;
 BusyFlags:=NormFlags;
 EXCL(BusyFlags,MouseMove); (* shut off mousemove messages while
                                          processing a message *)
 ModifyIDCMP(WinPtr^,NormFlags);

  insig1:=CARDINAL(WinPtr^.UserPort^.mpSigBit);

  inset:=SignalSet{};
  INCL(inset,insig1);

 REPEAT
  signal:= Wait(inset); (* wait for messages *)

  IF ( insig1 IN signal) THEN (* IDCMP message from window *)
    CheckForMes(ScrnPtr,Cmap,TRUE,dum,reg);
    ModifyIDCMP(WinPtr^,NormFlags); (* make sure flags get set back *)
  END; (* IF insig1 IN signal *)
 UNTIL Quit;

  ModifyIDCMP(WinPtr^,IDCMPFlagsSet{Closewindow});

END MainLoop;

(* =============================================== *)
PROCEDURE CleanMessages;
VAR
 MsgPtr: IntuiMessagePtr;
BEGIN

 REPEAT

  MsgPtr:=GetMsg(WinPtr^.UserPort^);
  IF MsgPtr <> NIL THEN
    ReplyMsg(MessagePtr(MsgPtr));
  END;
 UNTIL (MsgPtr =NIL);
END CleanMessages;

(* =============================================== *)

PROCEDURE AddBox(ScrnPtr:ScreenPtr);
VAR
 fr:CARDINAL;
 x,y,i,numcols:CARDINAL;
 
BEGIN
 fr:=1;
 IF WinPtr<>NIL THEN
  SetAPen(WinPtr^.RPort^,fr);
  SetBPen(WinPtr^.RPort^,fr);
  SetDrMd(WinPtr^.RPort^,Jam1);

  Move(WinPtr^.RPort^,x1-1,y1+1);
  Draw(WinPtr^.RPort^,x2+1,y1+1);
  Draw(WinPtr^.RPort^,x2+1,y2-1);
  Draw(WinPtr^.RPort^,x1-1,y2-1);
  Draw(WinPtr^.RPort^,x1-1,y1+1); (* box *)

  SetAPen(WinPtr^.RPort^,fr);
  SetBPen(WinPtr^.RPort^,0);
  SetDrMd(WinPtr^.RPort^,Jam1); 

  IF ScrnPtr=NIL THEN RETURN; END;
  IF ScrnPtr^.RastPort.BitMap=NIL THEN RETURN; END;
    
  numcols:=CARDINAL(ScrnPtr^.RastPort.BitMap^.Depth);

  numcols:=SHIFT(1,numcols);

  IF numcols>32 THEN numcols:=32; END;
  IF numcols<2 THEN numcols:=2; END;
  x:=x1;
  y:=y2;
  FOR i:=0 TO numcols-1 DO
    SetAPen(WinPtr^.RPort^,i);

    RectFill(WinPtr^.RPort^,x,y,x+BoxXsize,y+BoxYsize);
    x:=x+BoxXdist;
    IF x+BoxXsize>x2 THEN
     y:=y+BoxYdist;
     x:=x1;
    END; (* IF x2 *)
    
  END; (* FOR i *)


 END; (* IF WinPtr *)
END AddBox;



(* =============================================== *)
PROCEDURE SetupReqs(VAR gadlist:GadgetPtr):BOOLEAN;
CONST
 bk=0;
 left=10;
 top=45;
 ht=10;
 top2=13;
 left2=50;
 xshift=-60; 
 yshift=y2-y1-17; 
 
VAR
 ptr1,ptr2 :GadgetPtr;
 worked:BOOLEAN;

 
BEGIN

 worked:=TRUE;
 gadlist:=NIL;


 ptr1:=MakeBool(NIL,"Reset",2,left+86,top);

 IF ptr1=NIL THEN 
  worked:=FALSE;
 ELSE

  gadlist:=ptr1;

     
     ptr2:=MakeBool(ptr1,"OK",10,left,top);

     IF ptr2=NIL THEN
       worked:=FALSE;
     ELSE
      

      ptr1:=MakeBool(ptr2,"Cancel",1,left+27,top);

      IF ptr1=NIL THEN
	worked:=FALSE;
      ELSE

	 pFlags:=PropInfoFlagsSet{AutoKnob,FreeVert};
	
	 ptr2:=MakeProp(ptr1,7,x2+41+xshift,y2+yshift,20,y1-y2+1,
	 	pFlags,GadgetActivationSet{RelVerify,FollowMouse,GadgImmediate},
		0,start,20,20);
			    	   
        IF ptr2=NIL THEN
	  worked:=FALSE;
	ELSE

          Bpropinfo:=PropInfoPtr(ptr2^.SpecialInfo);

          BpropGad:=ptr2;


	 pFlags:=PropInfoFlagsSet{AutoKnob,FreeVert};
 
	 ptr1:=MakeProp(ptr2,8,x2+23+xshift,y2+yshift,20,y1-y2+1,
	 	pFlags,GadgetActivationSet{RelVerify,FollowMouse,GadgImmediate},
		0,start,20,20);

        IF ptr1=NIL THEN
	  worked:=FALSE;
	ELSE

          Gpropinfo:=PropInfoPtr(ptr1^.SpecialInfo);
          GpropGad:=ptr1;
	 
	 pFlags:=PropInfoFlagsSet{AutoKnob,FreeVert};

 	 ptr2:=MakeProp(ptr1,9,x2+5+xshift,y2+yshift,20,y1-y2+1,
	 	pFlags,GadgetActivationSet{RelVerify,FollowMouse,GadgImmediate},
		0,start,20,20);

        IF ptr2=NIL THEN
	  worked:=FALSE;
	ELSE

          Rpropinfo:=PropInfoPtr(ptr2^.SpecialInfo);

          RpropGad:=ptr2;

	END; (* IF red prop gad *)

	END; (* IF green prop gad *)
	
	END; (* IF blue prop gad *)

      END; (* IF CancelBool *)
     END; (* IF AcceptBool *)

 END; (* IF ResetBool *)
 RETURN worked;
END SetupReqs;

(* =============================================== *)
PROCEDURE CleanupReqs(VAR gadlist:GadgetPtr);
BEGIN
 IF gadlist<>NIL THEN
  FreeGadgets(gadlist);
 END; (* IF gadlist *)
END CleanupReqs;

(* =============================================== *)
PROCEDURE Palette(ScrnPtr:ScreenPtr;CustomScrn:BOOLEAN;
		  WinTitle:ARRAY OF CHAR;
	          VAR errmsg:ARRAY OF CHAR;VAR reg:CARDINAL;
		  left,top:INTEGER;VAR canceled,error:BOOLEAN);
VAR
 origMap:ColorMapPtr; 
 Cmap:ColorMapPtr;
 oldreg:CARDINAL;
 NewWin:NewWindow;
 
(* =============================================== *)
PROCEDURE Putback();
BEGIN
  SetMap(ScrnPtr,origMap^);
  reg:=oldreg;
END Putback;


BEGIN (* OF Palette *)

origMap:=GetColorMap(32);
Cmap:=GetColorMap(32);
IF (Cmap=NIL) OR (origMap=NIL) THEN 
  error:=TRUE;
  CopyString(errmsg,"not enough memory");
  RETURN;
END; (* IF *)

 oldreg:=reg;
 

 GetMap(ScrnPtr,Cmap^);
 GetMap(ScrnPtr,origMap^);
 
 mousegad:=0;
 start:=0D;
 wasstart:=0D;
 lastvpot:=0;
 Globalcanceled:=FALSE;
 canceled:=FALSE;
 error:=FALSE;
 Quit:=FALSE;
 errmsg[0]:=CHR(0);
 
 IF SetupReqs(gadlist) THEN
 
    WITH NewWin DO
      LeftEdge    := left;
      TopEdge     := top;
      Width       := x2+10;
      Height      := y1+10;
      DetailPen   := BYTE(0);
      BlockPen    := BYTE(1);
      Title       := ADR(WinTitle);
      Flags:=  WindowFlagsSet{Activate,NoCareRefresh,WindowClose,
	      			WindowDrag,WindowDepth};

      IDCMPFlags  := IDCMPFlagsSet{};
      
      IF CustomScrn THEN
       Type        := CustomScreen;
      ELSE
       Type	   := WBenchScreen;
      END; (* IF *)
      
      FirstGadget := gadlist;
      CheckMark   := NIL;
      Screen      := ScrnPtr;
      BitMap      := NIL;
      MinWidth    := 250;
      MinHeight   := 100; 
      MaxWidth    := 320;
      MaxHeight   := 200;

    END; (* WITH NewWin *)

    WinPtr :=  OpenWindow (NewWin);

    IF WinPtr <> NIL THEN 

  	     AddBox(ScrnPtr);

	     NormFlags:=IDCMPFlagsSet{Closewindow,MouseMove,
					MouseButtons,GadgetUp,GadgetDown};

	     FixGad(ScrnPtr,reg);

             MainLoop(ScrnPtr,Cmap^,reg);


	     canceled:=Globalcanceled;


             CleanMessages;

         IF WinPtr<>NIL THEN CloseWindow(WinPtr^); WinPtr:=NIL; END;
         ELSE
	     CopyString(errmsg,"could not open window");
	     error:=TRUE;
         END; (*IF WinPtr *)

	 IF Globalcanceled THEN 
	  Putback();
         END; (* IF Globalcanceled *)

         CleanupReqs(gadlist);

     ELSE
      CleanupReqs(gadlist); (* clean up partial allocation *)
      CopyString(errmsg,"could not allocate gadgets");
      error:=TRUE;
   END; (* IF SetupReqs *)


FreeColorMap(origMap^);
FreeColorMap(Cmap^);

END Palette;

(* =============================================== *)

BEGIN (* of MODULE *)

END Palette.

